/*
 * MarsCommunication.java
 *
 * Created on August 17, 2004, 12:21 PM
 */

package com.nikolay.ar.base.communication;

/**
 *
 * @author  nikolays
 */

import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.nikolay.ar.base.activities.NBaseActivity;
import com.nikolay.ar.base.constants.NParameterConstants;
import com.nikolay.ar.base.store.NStore;
import com.nikolay.ar.base.utils.NError;
import com.nikolay.ar.base.utils.NServerException;
import com.nikolay.ar.base.utils.NUtilities;

public class NCommunication implements NParameterConstants {
    
    private IDataParser parser = null;
    private ConnectionThread _connectionThread = null;
    private boolean started = false;
    
    private boolean isFileUpload = false;
    private String filename = "";
    private byte[] data;
    
    NBaseActivity activity;
    
    
    /** Creates a new instance of MarsCommunication */    
    public NCommunication(NBaseActivity activity) {
        this(activity, null, false, "", null);
    }
    
    public NCommunication(NBaseActivity activity, IDataParser parser) {
        this(activity, parser, false, "", null);
    }
    
    public NCommunication(NBaseActivity activity, IDataParser parser, boolean isFileUpload, String filename, byte[] data) {
        try {
        	this.activity = activity;
            this.parser = parser;             
            this.isFileUpload = isFileUpload;
            this.filename = filename;
            this.data = data; 
            
              
              
        }catch(Exception ex){
            this.started = false;
            parser.setError(new NError("Problem with creating new Communication thread, please leave some screens before continue", NError.EXCEPTION_TYPE, ex));
            NUtilities.logError(this, ex, true);
           /*
            try {
             net.rim.device.api.ui.component.Status.show("Problem with creating new Communication thread, please leave some screens before continue", 5000);
            } catch (Exception e) {                
            }*/
        }
    }
    
    private NError lastError = null;
    
    public NError getLastError(){
    	return lastError;
    }
    
    public IDataParser getParser() {
    	return this.parser;
    }
    
    
    public StringBuffer fetchDataSync() {
    	return this.fetchDataSync(parser.getUrl(), getPairs());
    }
    
    public NStore getStore() {
		return NStore.getStoreInstance();
	}
    
    public List<NameValuePair> getPairs() {
    	String emailId = activity.getNPreferences().getUserEmail();
    	if(emailId==null || emailId.length()==0) {
    		emailId = NUtilities.getUserEmailID(activity);
    	}
    	List<NameValuePair> pairs = this.parser.getNameValuePairs();
    	pairs.add(new BasicNameValuePair(USERNAME, activity.getNPreferences().getUserID()));
    	pairs.add(new BasicNameValuePair(PASSWORD, activity.getNPreferences().getPassword())); 
    	pairs.add(new BasicNameValuePair(USER_EMAIL, emailId));
    	pairs.add(new BasicNameValuePair(PIN, NUtilities.getPIN_DEVICEID(activity, true)));
    	pairs.add(new BasicNameValuePair(VERSION, NUtilities.getDeviceInfoAsCGIReady(activity)));
    	pairs.add(new BasicNameValuePair(TOKEN, NUtilities.getToken(activity)));
    	pairs.add(new BasicNameValuePair(UDEVICEID, NUtilities.getUniqueCode(activity)));
    	
    	
    	return pairs;
    }
    
    public StringBuffer fetchDataSync(String url, List<NameValuePair> nameValuePairs) {
    	StringBuffer result = null;  
    	try {  
              // _connectionThread.stopTheThread();            
               if(!NUtilities.isRadioOn()){
                   lastError = new NError("Radio is not ON. Please check your connection.", NError.CONNECTION_TYPE);
                   return null;    
               }
                    
               try {
            	   if(_connectionThread==null) _connectionThread = new ConnectionThread(parser); 
            	   
            	   result = _connectionThread.doTheCall(url, nameValuePairs);
            	   if(result==null){
            		   lastError = new NError("Server returned empty data", NError.SERVER_TYPE);
            		   return result;
            	   }            	   
               } catch (NServerException e){
            	   lastError = new NError(e.getMessage(), e.getType());
            	   return null;
               }
               
           } catch (Exception ex) {
        	   lastError = new NError("Fail to fetch server data", NError.EXCEPTION_TYPE, ex);
               NUtilities.logError(this, ex, true);
               return null;
           }
           
           return result;
    }
    
    public boolean fetchData() {
        return this.fetchData(false);
    }
    
    public boolean fetchData(boolean isPost) {
        return this.fetchData(isPost, true);
    }

    
    public boolean fetchData(boolean isPost, boolean force_out) {
        try {  
           // _connectionThread.stopTheThread();            
            if(!NUtilities.isRadioOn()){
                parser.setError(new NError("Radio is not ON. Please check your connection.", NError.CONNECTION_TYPE));
                return false;    
            }
            
            if(_connectionThread!=null) {
                _connectionThread.stopThread();
                _connectionThread = null;
            }
            
            _connectionThread = new ConnectionThread(parser);
            if(isFileUpload) {
                _connectionThread.setStream(true);
                _connectionThread.setRequestBytes(data);
                _connectionThread.setFilename(filename);
            }     
            _connectionThread._stop = false;
            
           
            
            String url1 = "";
            List<NameValuePair> nameValuePairs = null; 
            try{
            	url1 = parser.getUrl();
            	nameValuePairs = getPairs();
            	
            } catch (Exception ex) {
           
            }
            _connectionThread.setNameValuesPairsForPost(nameValuePairs);
                 
            NUtilities.log("MC::fetchData(url)", url1);
            
            _connectionThread.force_out = force_out;
               if(force_out)  this.started = false;
            if(!this.started) {
                try {                                   
                    _connectionThread.start();   
                } catch (Exception ex) {
                    parser.setError(new NError("Too many background tasks are currently running on your device. Please wait for a moment and try again. Thank you.", NError.CONNECTION_TYPE));
                    return false;    
                }         
            }
            
            this.started = true;  
        
            _connectionThread.setUrl(url1, nameValuePairs);
            _connectionThread.fetch(url1);        
        } catch (Exception ex) {
            NUtilities.logError(this, ex, true);
            return false;
        }
        return true;
    }    
    
    public boolean stopConnThread(){
        
        try {
            _connectionThread.stopThread();        
        } catch (Exception ex) {
        }
        
        this.started = false;
        return true;
    }

	public void setDataParser(IDataParser _parser) {
		this.parser = _parser;
	}
	
	public  NBaseActivity getActivity() {
		return this.activity;
	}
}
         
    
    
    
