/*
 * ConnectionThread.java
 *
 * Created on August 17, 2004, 12:31 PM
 */

package com.nikolay.ar.base.communication;

/**
 *
 * @author  nikolays
 */


/*import com.mars.bb.bin.*;*/
import java.io.*;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

//import com.mars.android.MarsApp;
//import com.nikolay.glu.utils.*;
import com.nikolay.ar.base.constants.*;
import com.nikolay.ar.base.utils.NError;
import com.nikolay.ar.base.utils.NServerException;
import com.nikolay.ar.base.utils.NUtilities;

public class ConnectionThread extends Thread //implements MarsResource
{
    private String _theUrl = "";
    private List<NameValuePair> _theNameValuePairs;
    //private String _theUrlPost = "";
    //private String _theUrlPostBody = "";
    
    public volatile boolean _start = false;
    public volatile boolean _stop = false; 
    private IDataParser parser;
    private boolean statusThread;
    //private MarsApp ap = null;
    public boolean force_out = true;
    
    
    StatusThread _statusThread;
    
    public ConnectionThread (IDataParser parser) {    
        //this.ap = app;
        this.parser = parser;
        
        //Force not to start the Status Thread Ever.
        statusThread = false; //status; 
        
        if(statusThread)
            _statusThread = new StatusThread(parser);
    }
    
      /**
     * Only relevant for Blackberry
     * @return
     */

    @Deprecated
    public static String getWapProviderOptions() {
    	return "";
    }
  
    /**
     * Only relevant for Blackberry
     * @return
     */
  @Deprecated
  public synchronized String getUrlOptions()
  {   
      String options = "";
      switch (NConstants.CONNECTION_METHOD) 
      {
          case NConstants.MDS:
              options = "";
              break;
          case NConstants.DIRECT_INTERNET:
              options = "";//";deviceside=true";
              break;
          case NConstants.WAP2:
              options = "";//com.mars.bb.utils.MarsUtilities.getWap2connectionStringExt();
              break;
          case NConstants.WAP_PROVIDER_INTERNET:             
              options = "";// getWapProviderOptions();
              break;
              
      }
      //To suppot direct connection: "deviceside=true"; possible timeout
      return options;
      //Force WAP2
      //return com.mars.bb.utils.MarsUtilities.getWap2connectionStringExt();
      //For WAP Provider
  }
    
    public synchronized String getUrl()
    {
        return _theUrl; // +getUrlOptions();
    }
    
    public synchronized void setUrl(String url,  List<NameValuePair> nameValuePairs ){
        _theUrl = url;
    }
    
    //fetch a page
    // synchronized so that i don't miss requests 
    public void fetch(String url)
    {
        if ( _start ) {
            //Dialog.alert(_resources.getString(HTTP_ALERT_REQUESTINPROGRESS));
        } else {
            synchronized(this)
            {
                if ( _start ) {                   
                    //Dialog.alert(_resources.getString(HTTP_ALERT_REQUESTINPROGRESS));
                } else {   
                    _start = true;
                    _theUrl = url;   
                    
                    if(_stop) {
                        _stop = false;
                        try {
                            if(!this.isAlive())
                                this.start();
                        } catch(Exception ex) {
                        }
                    }
                                     
                    if(statusThread)
                        _statusThread.go();
                }
            }
        }
    }

    //shutdown the thread    
    public void stopThread() {
        _stop = true;
    }

    public StringBuffer doTheCall(String url, List<NameValuePair> nameValuePairs) throws NServerException {
    	
    	 HttpClient httpclient = new DefaultHttpClient();  
    	 HttpPost httppost = new HttpPost(url);  
    	    
    	 try {  
    	    // Add your data  
    	    
    	    //Setting up the headers
    	    if(isStream()) {
    	    	httppost.setHeader("Content-Type",  "application/octet-stream");
    	    	httppost.setHeader("Content-Length", Integer.toString(getRequestAsBytes().length, 10));                            
    	    	httppost.setHeader("User-Agent", "Profile/MIDP-2.0 Configuration/CLDC-1.0");
    	    	httppost.setHeader("Content-Disposition", 
                        "form-data; name=\"Filedata\"; filename=\""+getFilename()+"\"");
            } else {   
            	httppost.setHeader("Content-Type", "application/x-www-form-urlencoded");
            	//httppost.setHeader("Content-Length", Integer.toString(a, 10));                            
            	httppost.setHeader("If-Modified-Since", "29 Oct 1999 19:43:31 GMT");
            	httppost.setHeader("User-Agent", "Profile/MIDP-2.0 Configuration/CLDC-1.0");
            	httppost.setHeader("Content-Language", "en-US");                     
            }            	    
    	    httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
    	    
    	    // Execute HTTP Post Request
    	    HttpResponse response = httpclient.execute(httppost);
    	    int status = response.getStatusLine().getStatusCode();
    	    if (status == HttpStatus.SC_OK)  {
                //is this html?
                //String ctype = response.getFirstHeader(MarsConstants.HEADER_CONTENTTYPE).getValue();
                boolean htmlContent = true;//= (ctype != null && ctype.equals(response.getFirstHeader(MarsConstants.CONTENTTYPE_TEXTHTML).getValue()));//ctype.equals(MarsConstants.CONTENTTYPE_TEXTHTML));
                //boolean textContent = (ctype != null && ctype.equals(MarsConstants.CONTENTTYPE_TEXT));
                
                InputStream input = response.getEntity().getContent();                     
                byte[] data = new byte[1024];
                int len = 0;
                int size = 0;
               
                StringBuffer raw = new StringBuffer();
                
                len = input.read(data);
                String statusStr = "E";
                if(len!=-1) {
                	statusStr = new String(data);
                	if(statusStr.startsWith("<")) {
                		raw.append(statusStr);
                		statusStr = "S";
                	} else {
	                	raw.append(statusStr.substring(1));
	                	statusStr = statusStr.substring(0, 1);
                	}
                	len = input.read(data);
                }
                String bb = null;
                while ( -1 != len ) {
                	bb = new String(data, 0, len); 
                	try {
                  	 raw.append(bb);
                	} catch (Exception ex) {
                		raw = new StringBuffer("Error: Device ex:"+ ex.getMessage());
                		break;
                	}
                    bb = null;
                    size += len;
                    try 
                    {
                    	len = input.read(data);
                    } catch (IOException ioException) {
                    	len = -1;
                    }
                }
                
                try {
                	input.close();
                	response.getEntity().consumeContent();
                } catch (IOException ioException) {
                	
                }
                
                if ( htmlContent ) {                             
                    if(/*raw.charAt(0)*/statusStr.charAt(0)=='E') {
                        NUtilities.logError("Connection Thread, run:", "Server Error" + raw.toString());                        
                        throw new NServerException(raw.toString()); 
                    } else {
                    	return raw;
                    }
                } else {
                    if(/*raw.charAt(0)*/statusStr.charAt(0)=='E') {
                        NUtilities.logError("Connection Thread, run:", "Server Error" + raw.toString());                        
                        throw new NServerException(raw.toString()); 
                   } else {
                	   return raw;
                   }
                }

            } else {
                NUtilities.logError("Connection Thread, run:", "Error Server status:" + Integer.toString(status) +";");                        
                throw new NServerException("Error Server status:" + Integer.toString(status) +";", NError.CONNECTION_TYPE);

            }
    	   
    	 } catch (ClientProtocolException e) {
    		 NUtilities.logError("Connection Thread, ClientProtocolException:", e);
    		 throw new NServerException("ClientProtocolException:" + e.toString(), NError.EXCEPTION_TYPE);
    	 } catch (IOException e) {
    		 NUtilities.logError("Connection Thread, IOException:", e);
    		 throw new NServerException("IOException:" + e.toString(), NError.EXCEPTION_TYPE);
    	 }  
    }
    
    public void run() {
        for(;;) {
            //Thread control
            while( !_start && !_stop) {
                //sleep for a bit so we don't spin
                try {
                    sleep(NConstants.TIMEOUT);
                } catch (InterruptedException e) {
                    System.err.println(e.toString());
                }
            }
            
            //exit condition
            if ( _stop ) { 
                return;
            }
            
            //This entire block is synchronized, this ensures I won't miss fetch requests made while i process a page
             //synchronized(this) {
            	 // Create a new HttpClient and Post Header  
            	try {
            		StringBuffer raw = doTheCall(this.getUrl(), _theNameValuePairs);
            		parser.parseData(raw, "html");            		
            			
            	} catch (NServerException ex){
            		parser.setError(new NError(ex.getMessage(), ex.getType()));
            	}
            	  
            	if(statusThread)
                       stopStatusThread();
                  
                 //we're done one connection so reset the start state
                 _start = false;
                 //Experimental
                 if(force_out)
                    _stop = true;
             //}
        }
    }

    private void stopStatusThread() {
        if(!statusThread)
            return;
        
        _statusThread.pause();
        try {
            synchronized(_statusThread)
            {
            //Check the paused condition, incase the notify fires prior to our wait, in which case we may never see that nofity
                while ( !_statusThread.isPaused() ) {
                    _statusThread.wait();
                }
            }
        } catch (InterruptedException e) {
            System.err.println(e.toString());
        }
    }
    
    public void setNameValuesPairsForPost(List<NameValuePair> pairs) {
    	this._theNameValuePairs = pairs;
    }
    
    //Do Post not Ready -- need to be Fixed
    //New Methods  
    /*
    boolean _isPost = false;
    
    public void setPost(boolean _isPost){
        this._isPost = _isPost;
    }
    
    public boolean isPost(){
        return _isPost;
    }    
    
  
    public String getUrlPost(){
        return _theUrlPost//+";trustAll" +getUrlOptions();
    }  
    
    public String getUrlPostBody() {
        return _theUrlPostBody;
    }
    
    public void setUrlPost(String urlPost){
        this._theUrlPost = urlPost;
    }
    
    public void setUrlPostBody(String urlPostBody) {
        this._theUrlPostBody = urlPostBody;
    }
    */
    
    boolean _isStream = false;
    byte[] _requestBytes;
    String _filename="test.wav";
    
    public boolean isStream() {
        return this._isStream;
    }
    
    public void setStream(boolean val) {
        this._isStream = val;
    }
    
    public void setRequestBytes(byte[] _bt) {
        this._requestBytes = _bt;
    }
    
    public byte[] getRequestAsBytes() {
        return this._requestBytes;
    }
    
    public String getFilename() {
        return this._filename;
    }
    
    public void setFilename(String filename) {
        this._filename = filename;
    }
}    
