/*
 * IScreenUpdate.java
 *
 * Created on August 17, 2004, 12:22 PM
 */

package com.nikolay.ar.base.communication;


/**
 * Base interface, should be implement from the screens in order
 * to receive net communication
 * @author nikolays
 * @version 1.0
 */
public interface IScreenUpdate {
    
    /**
     * Method that receive the data from the net
     * @param content content that should be update
     * @param cr param 1
     * @param rc param 2
     */    
    public void updateContent(java.lang.Object content, int cr, int rc);

    /**
     * Method that receive un error in the data from the net
     * @param err - error that occured
     */    
    public void setError(com.nikolay.ar.base.utils.NError err);  
}
