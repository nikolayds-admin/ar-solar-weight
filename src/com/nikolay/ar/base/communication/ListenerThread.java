package com.nikolay.ar.base.communication;

//import java.io.*;



//inner classes ------------------------------------------------------------
/** This Class is for a implementing a Listner for incomming calls. 
 * Its relevent for Blackberry, but currently should not be use for 
 * Android. Nikolay has been commented the code.
 * 
 */
@Deprecated
public class ListenerThread extends Thread
{
    private boolean _stop = false;
    //private StreamConnectionNotifier _notify;
    private String _theUrl = "";
    
    /*
    public synchronized void stop()
    {
        _stop = true;
         try {  
            //_notify.close(); //close the connection so the thread will return.
         } catch (Exception e) {
             //System.err.println(e.toString());
         } 
     }
     */

     public void run() {
         
         //StreamConnection stream = null;
         //InputStream input = null;
         while (!_stop) {
             try {
                 synchronized(this) { 
                  //synchronize here so that we don't end up creating a connection that is never closed
                  // open the connection once (or re-open after an IOException), 
                  // so we don't end up in a race condition, where a push is lost if 
                  // it comes in before the connection is open again.
                    //_notify = (StreamConnectionNotifier)Connector.open(this.getUrl());
                  }                    
                  while (!_stop) {
                     //NOTE: the following will block until data is received
                	  /*
                     stream = _notify.acceptAndOpen();
                     input = stream.openInputStream();
                     //Extract the data from the input stream
                     DataBuffer db = new DataBuffer();
                     byte[] data = new byte[1024];
                     int chunk = 0;
                     while ( -1 != (chunk = input.read(data)) ) {
                        db.write(data, 0, chunk);
                     }
    
                     input.close();
                     stream.close();
                        
                     data = db.getArray();

                     // updateBitmap(data);
                      * 
                      */
                  }
                    
                  //_notify.close();
                  //_notify = null;   
              } catch (Exception ioe) {
                    // likely the stream was closed
                    //System.err.println(ioe.toString());
                    /*
                    if ( _notify != null ) {
                        try {
                            _notify.close();
                            _notify = null;
                        } catch ( Exception e ) {
                        }
                        
                  }
                  */
              }
         }
    }
        
    //retrieve the URL
    public synchronized String getUrl(){
        return _theUrl;
    }
    
    public synchronized void setUrl(String url){
        _theUrl = url;
    }
        
}
