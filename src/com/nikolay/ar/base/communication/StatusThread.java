/*
 * StatusTread.java
 *
 * Created on August 17, 2004, 12:51 PM
 */

package com.nikolay.ar.base.communication;

/**
 *
 * @author  nikolays
 */

import com.nikolay.ar.base.constants.*;

/**
 * The StatusThread class manages display of the status message while lengthy HTTP/HTML operations are taking place
 */
public class StatusThread extends Thread 
{
    //private static ResourceBundle _resources = ResourceBundle.getBundle(BUNDLE_ID, BUNDLE_NAME);
    
    private IDataParser parser;
    
    public StatusThread(IDataParser parser){
        this.parser = parser;
    }
    
    private volatile boolean _stop = false;
    private volatile boolean _running = false;
    private volatile boolean _isPaused = false;

    //resume the thread
    public void go() {
        _running = true;
        _isPaused = false;
    }

    //pause the thread
    public void pause() {
        _running = false;
    }

    //query the paused status
    public boolean isPaused() {
        return _isPaused;
    }

    //shutdown the thread
    public void stopStatusTread() {
        _stop = true;
    }

    public void run() {
        int i = 0;
        //setup the status messages
        String[] statusMsg = new String[6];
        StringBuffer status = new StringBuffer("HTTP_STATUS_MESSAGE");
        statusMsg[0] = status.toString();
        //preincrement improves efficiency
        for ( int j = 1; j < 6; ++j) {
            statusMsg[j] = status.append("HTTP_STATUS_MESSAGE_APPEND").toString();
        }
        
        for (;;) {
            while (!_stop && !_running) {
                //sleep a bit so we don't spin
                try {
                    sleep(NConstants.THREAD_TIMEOUT);
                } catch ( InterruptedException e) {
                    System.err.println(e.toString());
                }
            }
            
            if ( _stop ) {
                return;
            }
            i = 0;
            status.delete(0, status.length()); //clear the status buffer
            for ( ;; ) {
                //we're not synchronizing on the boolean flag! value is declared volatile therefore
                if ( _stop ) {
                    return;
                }
                
                if ( !_running ) {
                    _isPaused = true;
                    synchronized(this) {
                        this.notify();
                    }
                    break;
                }

                parser.updateContent(statusMsg[++i%6]);
                try {
                    Thread.sleep(NConstants.TIMEOUT); //wait for a bit
                } catch (InterruptedException e) {
                    System.err.println(e.toString());
                }
            }
        }
    }
}    


