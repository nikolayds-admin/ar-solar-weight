package com.nikolay.ar.base.communication.graph;

import java.util.*;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.nikolay.ar.base.activities.NBaseActivity;
import com.nikolay.ar.base.communication.*;
import com.nikolay.ar.base.constants.*;
import com.nikolay.ar.base.trans.*;
import com.nikolay.ar.base.utils.NUtilities;

/**
 *
 * @author  nikolays
 */
public class GraphCommunicator implements IDataParser, NParameterConstants {
  
    IScreenUpdate isu;
    
    private NCommunication marsComm; 
    private ArUser user = null;        
    private Vector<String> data = new Vector<String>();
        
    private String sel = "";
    private String comm = "";       
    private String screenID = "";
    private Vector<String> choices = new Vector<String>();
    
    Vector<String> headers = null;
    Vector<String> values = null;
    String title = "MARS Chart"; 
    String chartType = CHART_TYPE_BAR;

    
    /** Creates a new instance of GraphCommunicator */
    public GraphCommunicator(NBaseActivity activity, ArUser user, IScreenUpdate isu, Vector<String> header, Vector<String> values, String title) {
       this.isu = isu;
       this.user = user;              
       this.title = title;
       this.headers = header;
       this.values = values;
       marsComm = new NCommunication(activity, this); 
    }
    
    public void setChoices(Vector<String> choices) {
    	this.choices = choices;
    }
    
    public void setChartTypeAs3DPie()  {
    	chartType = CHART_TYPE_3D_PIE;
    }

    public void setChartTypeAsPie()  {
    	chartType = CHART_TYPE_3D_PIE;
    }

    public void setChartTypeAsBar()  {
    	chartType = CHART_TYPE_BAR;
    }

    public void setChartType(String type)  {
    	chartType = type;
    }
    
    public void fetch() {                
        this.sel = FETCH_SELECTOR;
        this.comm = GRAPH_FETCH;     
        marsComm.fetchData(true);  
    }
    
    public Vector<String> getData() {
    	return data;    	
    }
    
    public String getToken() {
    	if(data == null || data.size()==0) return null;
    	else return (String) data.elementAt(0);
    }
   
    public synchronized String getUrl() {
        String url = user.getServer();
        url+= NSQLStrings.REPORTS_SERVICE;      
        return url;         
    }
    
    public synchronized boolean parseData(StringBuffer buffer, String type) {
        Vector<String> temp = new Vector<String>();
        if(buffer!=null) {
        	String strBuffer = buffer.toString();
        	strBuffer = strBuffer.substring(1); //Removing the S
        	temp.addElement(strBuffer);
        }
        
        /*
        try {;
            temp = MarsUtilities.convertDataToVector(buffer);
        } catch (Exception e){
            MarsUtilities.logError(this, e, true, false);
            return false;
        }
        */
        
        /* Dead Code, temp never can be null
        if(temp==null) {
            isu.updateContent(new Vector<String>(), 0, 0);
            return true;
        }*/
        
        Enumeration<String> e = temp.elements();
        
        while(e.hasMoreElements()) {
            data.addElement(e.nextElement()); 
        }                
        
        isu.updateContent(getData(), 0, 0);        
        
        return true;          
    }
    public synchronized void resetData() {
         data.removeAllElements();
    }   
    public synchronized void setError(com.nikolay.ar.base.utils.NError err) {
        isu.setError(err);   
    }    
    public synchronized void stopTheThreads() {
        marsComm.stopConnThread();   
    }    
    public synchronized void updateContent(java.lang.String content) {
    }    
    public synchronized boolean updateStatus(java.lang.String content, java.lang.String type) {
        return true;
    }
 

	@Override
	public List<NameValuePair> getNameValuePairs() {
		List<NameValuePair> ret = new ArrayList<NameValuePair>();
		   
		ret.add(new BasicNameValuePair(USERNAME, user.getUsername()));
    	ret.add(new BasicNameValuePair(PASSWORD, user.getPassword()));                

    	ret.add(new BasicNameValuePair(SELECTOR, sel));
    	ret.add(new BasicNameValuePair(COMMAND, comm));   
    	ret.add(new BasicNameValuePair(SCREEN_ID, screenID));
    	ret.add(new BasicNameValuePair(CHART_TITLE, com.nikolay.ar.base.utils.NUtilities.encodingString(chartType))); 
        
        if(choices!=null && choices.size()!=0) {
        	ret.add(new BasicNameValuePair(CHART_COLOMNS_TO_PLOT, com.nikolay.ar.base.utils.NUtilities.PrepareChartChoiceAsUrlEncoded(choices)));
        }
        
        ret.add(new BasicNameValuePair(CHART_HEADER, NUtilities.PrepareHeaderAsUrlEncoded(headers)));
        ret.add(new BasicNameValuePair(CHART_DATA, NUtilities.PrepareChartDataAsUrlEncoded(values)));
        
		return ret;
	}               
}
