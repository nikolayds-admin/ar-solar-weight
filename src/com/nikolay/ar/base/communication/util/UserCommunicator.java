/*
 * MarsUserCommunicator.java
 *
 * Created on November 2, 2004, 9:53 AM
 */

package com.nikolay.ar.base.communication.util;

import java.util.*;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.nikolay.ar.base.activities.NBaseActivity;
import com.nikolay.ar.base.communication.*;
import com.nikolay.ar.base.constants.*;
import com.nikolay.ar.base.trans.*;
import com.nikolay.ar.base.utils.NError;
import com.nikolay.ar.base.utils.NUtilities;

/**
 *
 * @author  nikolays
 */
public class UserCommunicator implements IDataParser, NParameterConstants {
        
	
    IScreenUpdate isu;
    
    private NCommunication marsComm;   
    
    private boolean login = false;
    private Vector<String> data = new Vector<String>();
    
    private static String marsUserSelector = "MarsUser";
    private static String infoCommand = "UserData";
    private static String loginCommand = "Login";
    private String baseUrl = "";
    private String username = "";
    private String domain = "";
    private String password = "";
    private String encode = "Y";
    
    
    //protected static ResourceBundle _resources = ResourceBundle.getBundle(BUNDLE_ID, BUNDLE_NAME);
    
    /** Creates a new instance of MarsUserCommunicator */
    public UserCommunicator(NBaseActivity activity, IScreenUpdate isu, boolean login) {       
       this.isu = isu;       
       this.login = login; 
       marsComm = new NCommunication(activity, this);           
    }
    
    public void setBaseUrl(String url){
        this.baseUrl = url;
    }

    public String getDomain() {
    	return this.domain;
    }
    
    public void setDomain(String domain){
        this.domain = domain;
        NConstants.DOMAIN_NAME = domain;
    }
    
    public void setUsername(String un){
        this.username = un;
    }
    
    public void setPassword(String pass){
        this.password = pass;
    }

    public void setEncode(String enc){
        this.encode = enc;
    }
    
    public void setLogin(){
        login = true;
        marsComm.fetchData();
    }
    
    public void setData(){
        login = false;
        marsComm.fetchData();
    }
    
    public Vector<ArUser> getUserData(){
    	StringBuffer buffer = marsComm.fetchDataSync(this.getUrl(), this.getNameValuePairs());
    	Vector<String> _data = NUtilities.convertDataToVector(buffer);
    	
    	return getData(_data);
    }
    
    public NError getLastError(){
    	return marsComm.getLastError();
    }

    
    public String getUrl() {
        String url = baseUrl;        
        url += NSQLStrings.MARS_USER_FETCH;        
       
        return url;
    }
    
    
   public Vector<ArUser> getData(Vector<String> data) {
 
	   if(data == null) return null;
	   
	   while(data.size()< ArUser.ELEMENTS_PER_ROW) {
		   data.addElement("");
	   }
	   
	   Enumeration<String> el = data.elements();
        
        int c =0;
        Vector<String> vec = new Vector<String>();
        Vector<ArUser> rez = new Vector<ArUser>();
        
        int elPerRow = ArUser.ELEMENTS_PER_ROW;        
        
        while(el.hasMoreElements()){
            String str = el.nextElement().toString();                        
            vec.addElement(str);
            c++;
            if(c==elPerRow) {
                ArUser act = new ArUser(vec);
                rez.addElement(act);                                                    
                vec = new Vector<String>();
                c=0;
            }    
        }
        
        return rez;
    }
    
    public synchronized boolean parseData(StringBuffer buffer, String type) {
        //net.rim.device.api.ui.component.Status.show(buffer.toString());
        Vector<String> temp = NUtilities.convertDataToVector(buffer);
        
        //temp
        //isu.updateContent(temp.elementAt(1), 5, 0);
        //return true;
        //
    
        if(temp==null) {
            //net.rim.device.api.ui.component.Status.show("NULL Vector");
            if(login)
                isu.updateContent(null, 0, 0);
            else
                isu.updateContent(null, 1, 0);     
            
            return false;
        }
        
        Enumeration<String> e = temp.elements();
        
        while(e.hasMoreElements()) {
            data.addElement(e.nextElement()); 
        }        
        
        if(login)
            isu.updateContent(getData(temp), 
            		NCommunicationConstants.USER_COMMUNICATOR_LOGIN, NCommunicationConstants.USER_COMMUNICATOR);
        else
            isu.updateContent(getData(temp), 
            		NCommunicationConstants.USER_COMMUNICATOR_USER_DATA, NCommunicationConstants.USER_COMMUNICATOR);
        
       
        return true;
        
    }
        
    public synchronized void updateContent(java.lang.String content) {
       // MarsUtilities.logError(this, content, true, true);
       setError(new com.nikolay.ar.base.utils.NError(content, com.nikolay.ar.base.utils.NError.SERVER_TYPE)); 
    }
    
    public synchronized boolean updateStatus(java.lang.String content, java.lang.String type) {
        return true;
    }
    
    public void resetData() {
        data.removeAllElements();
    }  
    
    public synchronized void stopTheThreads(){
       marsComm.stopConnThread();
    }  
    
    public void setError(com.nikolay.ar.base.utils.NError err) {
        isu.setError(err);
    }

	@Override
	public List<NameValuePair> getNameValuePairs() {		
		List<NameValuePair> ret = new ArrayList<NameValuePair>();
    	ret.add(new BasicNameValuePair(USERNAME, username));
    	ret.add(new BasicNameValuePair(PASSWORD, password));
    	ret.add(new BasicNameValuePair(PASSWORD_ENC, encode));
	    //ret.add(new BasicNameValuePair(+ "=" + domain;
    	ret.add(new BasicNameValuePair(SELECTOR, marsUserSelector));        
	    if(login) {
	    	ret.add(new BasicNameValuePair(COMMAND, loginCommand));
        } else {
        	ret.add(new BasicNameValuePair(COMMAND, infoCommand));        
        }     
	    ret.add(new BasicNameValuePair(APP_VERSION, "AndoidTest"));
	    ret.add(new BasicNameValuePair(DEVICE_INFO, NUtilities.getDeviceInfoAsCGIReady(marsComm.getActivity())));
	        
	        
		return ret;
	}
    
}
