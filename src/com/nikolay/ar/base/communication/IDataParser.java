/*
 * IDataParser.java
 *
 * Created on August 17, 2004, 12:22 PM
 */

package com.nikolay.ar.base.communication;

import java.util.List;

import org.apache.http.NameValuePair;

/**
 * The Data Parser interface.
 * It's called be Conenction thread to parse data or specific events.
 * Once data is received should notify and passed to the screen or other
 * @see IScreenUpdate
 * @author nikolays
 * @version 1.0 
 */
public interface IDataParser {
    
    /**
     * Parsing the Data (from String) and pushed to the IScreenUpdate
     * @param buffer the data
     * @param type type of the data
     * @return true if the parsing is successful
     */    
    public boolean parseData(StringBuffer buffer, String type);    
    
    /**
     * Status of receiving - used by Status Thread
     * @param content content of status
     * @param type type of content
     * @return true if handle is successeful
     */    
    public boolean updateStatus(java.lang.String content, java.lang.String type);
    
    /**
     * Used for error or exception notify
     * the exception/error body
     * @param content body of the exception/error or other event
     */    
    public void updateContent(java.lang.String content);
    
    /**
     * used by Connection Thread to receive url of the request
     * it used CGI encoding.
     * Later should be code to use doPost method
     * @return the url of the request
     */    
    public String getUrl();
    
    /**
     * Method that receive un error in the data from the net
     * @param err - error that occured
     */    
    public void setError(com.nikolay.ar.base.utils.NError err);  
    
    /**
     * 
     * @return
     */
    public List<NameValuePair> getNameValuePairs();
    
    public void resetData(); 
    
    public void stopTheThreads();
    
    //public String getUrlPost();
    
    //public String getUrlPostBody();
    
    
    
}
