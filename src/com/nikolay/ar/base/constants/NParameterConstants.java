package com.nikolay.ar.base.constants;

public interface NParameterConstants {
	  //Http Parameters names
    public static final String SELECTOR = "selector";
    public static final String COMMAND = "command";
    public static final String REP_ID = "repID";
    public static final String ACT_ID = "actID";    
    public static final String FIRM_ID = "firmID";
    public static final String OFFICE_ID = "officeID";
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String PASSWORD_ENC = "passwordEncode";
    public static final String USER_EMAIL  = "userEmail";
    public static final String PIN  = "pin";
    public static final String DOMAIN  = "domain";
    public static final String IS_TODAY = "today";
    public static final String VERSION ="version";
    public static final String UDEVICEID = "udeviceid";
    public static final String TOKEN = "token";
    
    public static final String START_ITEM = "startItem";
    public static final String END_ITEM =  "endItem";
    public static final String SORT_TYPE = "sortType";
    public static final String SORT_TYPE_ASC = "sortTypeAsc";
    public static final String REPORT_SEL_1 = "reportSel1";
    public static final String REPORT_SEL_2 = "reportSel2";
    public static final String VALUE =  "value";
    public static final String VALUE_1 =  "value1";
    public static final String VALUE_2 =  "value2";
    
    //Common
    public static final String ENTRY_DT = "entryDate";
    public static final String UPDATE_DT = "updateDate";
    public static final String ENTRY_USR = "entryUser";
    public static final String UPDATE_USR = "updateUser";
    
    
    //Tickler
    public static final String UID = "uid";
    public static final String ACT_TYPE = "actType"; 
    public static final String CALENDAR_ITEM = "calendarItem";  
    public static final String ONLY_CHANGED_TILCKER_ITEMS = "onlyChangedItems"; 
    
    //Profile
    public static final String PROFILE_ID = "profileID";
    public static final String PROFILE_TYPE = "profileType";
    public static final String PROFILE_STATUS_CODE = "profileStatusCode";
    
    
    
    //Task
    public static final String FOR_USERID = "forUserID";
    public static final String TASK_SUBJECT = "taskSubject";
    public static final String TASK_DETAILS = "taskDetails";
    public static final String TASK_PRIORITY = "taskPriority";
    public static final String TASK_DUE_DATE = "taskDueDate";
    public static final String TASK_PERC = "taskPerc";
    public static final String TASK_COMPAIGNID = "taskCompID";
    public static final String TASK_ALARM_DATE = "taskAlarmDate";
    
    //Email
    public static final String FETCH_EMAIL_DETAILS_SELECTOR = "fetchEmailDetails";    
    public static final String EMAIL_SELECTOR = "Email";
    public static final String EMAIL_SEND_COMMAND = "EmailSend";
    public static final String EMAIL_SAVE_COMMAND = "EmailSave";    
    public static final String EMAIL_BODY = "EmailBody";
    public static final String EMAIL_SUBJECT = "EmailSubject";
    public static final String EMAIL_TOS = "EmailTOS";
    public static final String EMAIL_CCS = "EmailCCS";
    public static final String EMAIL_FROM = "EmailFrom";
    
    //RepNote
    public static final String NOTE =  "note";
    public static final String REP_NOTE =  "repNote";
    public static final String NOTE_ID =  "noteID";
    
    //Log Call
    public static final String LOGCALLTYPE = "logCallType";
    public static final String ISLOGGING = "isLogging";
    public static final String LOG_SCH_CALL_ID = "schCallID";
    public static final String LOG_REP_ID = "repID";
    
    public static final String LOG_CALL_TYPES = "logCallTypes";
    public static final String INCOMMINGCALL = "I";
    public static final String OUTGOINGCALL = "O";
    
    //Meeting
    public static final String TYPE_OF_MEETING = "meetingType";
    public static final String MEETING_DESC = "meetingDesc";
    public static final String CONTACT_ID = "contactID";
    public static final String ORG_USER = "orgUser";
    public static final String EXTERNAL_ATTENDEES = "extAtt";
    public static final String DIRECTIONS = "directions";
    public static final String ATTENDEES = "attendees";
    public static final String EXPENCES = "exps";   
    public static final String STATUS_CD = "statCD"; 
    public static final String PLACE = "place";
    
    
    public static String IS_HISTORY = "isHistory";
    
    //end meeting    
    
    
    //Schedular Call
    public static final String CALL_PRIORITY = "callPriority";
    public static final String ALARM_ON = "alarmOn";
    public static final String ALARM_NOTICE = "alarmNotice";
    public static final String CALL_TYPE = "callType";
    public static final String BEGIN_DATE = "beginDate";
    public static final String END_DATE = "endDate";
    public static final String SCHEDULAR = "schedular";    
    public static final String CALL_USER = "callUser";
    //??
    public static final String EVENT_ID = "eventID";
    public static final String RECURE_ID = "recureID";
    
    public static final String NEW_NOTE = "newNote";
    
    
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String FIRM_NAME = "firmName";
        
    //address
    public static final String CITY = "city";
    public static final String ADDRESS1 = "address1";
    public static final String ADDRESS2 = "address2";
    public static final String ADDRESS3 = "address3";
    public static final String ADDRESS4 = "address4";    
    public static final String ZIP_CODE = "zipCode";
    public static final String PHONE = "phone";
    public static final String FAX = "fax";
    public static final String EXT = "ext";
    public static final String STATE_CD = "stateCD";
    public static final String COUNTRY_CD = "countryCD";
    
    //repAddress addon
    public static String ADDRESS_ID = "addressID";
    public static String ADDRESS_TYPE = "addressType";
    public static String ADDRESS_DESC = "addressDesc";
    
    //repPhone addon
    public static String PHONE_ID = "phoneID";
    public static String PHONE_TYPE = "phoneType";
    
    
    //order 
    public static final String SHIP_NOTE = "shipNote";
    public static final String SHIP_INSTR = "shipInstr";
    public static final String SHIP_DATE = "shipDate";
    public static final String FF_HOUSE = "ffHouse";
    public static final String SHIP_METHOD = "shipMethod";
    public static final String ORDER_STATUS = "orderStatus";
    public static final String INVENTORIES = "invts";       
    //end order    
    
    //Reports
    public static String REPORT_ID = "reportID";
    public static String DELETE_SELECTOR = "delete";
    public static String DELETE_REPORT = "deleteReport";
    
    //favorites
    public static String LIST_ID = "listID";
    public static String IS_FAVORITE = "isFavorite";
    public static String IS_OFFICE_SEARCH = "isOfficeSearch";
    

    //Rep Profile
    public static String TITLE = "title";
    public static String EMAIL = "email"; 
    public static String EMAIL2 = "email2";  
    public static String NICK_NAME="nickName";  
    //end RepProfile
    
    //Rep Category Params
    public static String ENTITY_ID = "entityID";
    public static String CAT_CD = "catCD";
    public static String CAT_TYPE = "catType";
    public static String CAT_COMMENT = "catComment";
    //END rep Category Params
    
    //Chart
    public static String  CHART_TYPE = "chartType";
    public static String  CHART_TITLE = "chartTitle";
    public static String  CHART_HEADER = "chartHeader";
    public static String  CHART_DATA = "chartData";
    public static String  CHART_COLOMNS_TO_PLOT = "chartCToPlot";

    
    //Actions
    public static final String SAVE =  "save";
    public static final String UPDATE =  "update";
    public static final String DELETE =  "delete";
    public static final String INSERT =  "insert";
    
    
    //Selectors 
    public static String fetchSelector = "fetch";
    
    //Comamnds
    public static String fetchCommand = "fetch";
    public static String CHECK_COMMAND = "check";
    //public static String saveCommand = "save";
    
    
    //Rep Profile Selectors
    public static String REP_PROFILE_UPDATE = "repUpdateProfile";
    public static String REP_ADDRESS = "repAddress";
    public static String REP_PHONE = "repPhone";
    public static String REP_CATEGORY = "repCategory";
    public static String PROFILE = "profile";
    public static String PROFILE_GRID = "profileGrid";
    //End Rep Profile Selectors;
    
    //Firm Selectors
    public static String FIRM_CATEGORY = "firmCategory";
    
    //Office Profile Selectors
    public static String OFFICE_CATEGORY = "officeCategory";


    //Tickler Selectors and Commans
    public static String LOCATIONS = "Locations";
    public static String TASK_SELECTOR = "Task";
    public static String ticklerCallSelector = "Call";
    public static String ticklerMeetingSelector = "Meeting";
    public static String ticklerOrderSelector = "Order";
    public static String ticklerLogCallSelector = "LogCall";    
    public static String ticklerNoteSelector = "Note";    
    public static String ticklerTaskSelector = "Task";    
    public static String ticklerIntMeetingSelector = "IntMeeting";    
    public static String ticklerListSelector = "List";
    public static String ticklerItemBBUIDSelector = "saveUID";    
    public static String FETCH_INVENTORY_SELECTOR ="fetchInventory";
    
    //Reports and favorites commands and selectors
    public static String fetchAllFavorites = "fetchAllFavorites";    
    public static String fetchAllReports = "fetchAllReports";
    public static String fetchReport = "fetchReport";    
    //end favorites
    
    //Office
    //public static String OFFICE_ID = "officeID";
    
    //Graph
    public static String GRAPH_FETCH = "graph";    
    
    //Dashborad
    public static final String FETCH_SELECTOR = "fetch";
    public static final String DASHBOARD_FETCH = "fetchDashboard";
    public static String UPDATE_SELECTOR = "update";
    public static String UPDATE_DASHBOOARD_COMMAND = "updateDashboard";
    public static String GADGET_IDS = "gadgetIDs";
    public static String SCREEN_IDS = "screenIDs";
    
        
    public static String DASHBOARD_GADGET_LIST_FETCH = "fetchDashboardList";
    
    //Maps
    public static String MAP_SELECTOR = "Maps";
    public static String MAP_FETCH_CORDINATES_COMMAND = "FetchCordinates";
    
    public static String MAP_ADDRESS1 ="+address1";
    public static String MAP_ADDRESS2 ="&address2";
    public static String MAP_CITY="city";
    public static String MAP_STATE = "state";
    public static String MAP_ZIP = "zip";
    public static String MAP_DESC = "desc";
    public static String MAP_NAME = "name";   
    
    
    //Command
    public static String ASSETS_BY_PROD = "AsstesByProd";
    
    //Order By
    public static String ORDER_BY = "orderBy";
    public static String IS_ASC = "isAsc";
    
    
    public static String CHART_TYPE_PIE = "pie";
    public static String CHART_TYPE_3D_PIE = "3dpie";
    public static String CHART_TYPE_BAR = "bar";
    public static String CHART_TYPE_HORIZONTAL_BAR = "hotizontalbar";
    public static String CHART_TYPE_HORIZONTAL_BAR_STACK = "hotizontalbarstack";
    
    //UserDeviceInfo
    public static String APP_VERSION = "1";
    public static String DEVICE_INFO = "info";
    
    //Save userPref
    public static String SAVE_USER_PREF = "saveUserPref";
    public static String USER_PREF_VALUES = "UPVALUES";
    public static String USER_PREF_KEYS = "UPKEYS";
    
    public static String EIS_DISPLAY_FIELDS = "eisDF";
    public static String EIS_PARENT_PRIMARY_COL_IDS = "eisPPCs";
    public static String EIS_PARENT_SCREEN_IDS = "eisPSs";
    public static String EIS_CRITERIA_CDS = "eisCCs";
    public static String EIS_CRITERIA_VALUES = "eisCVs";
        
    public static String SCREEN_ID = "screenID";
    public static String START_DATE = "startDate";

    public static String GADGET_ID = "gadgetID";
    public static String DASHBOARD_ID = "dashboardID";
    
    public static String DATA_SERVICE = "DATA_SERVICE";
    
    public static String QUERY = "query";
    public static String QUERIES = "queries";
    
    //GENERIC COMMANDS
	public static final String EXEC_QUERY = "execQuery";
	public static final String EXEC_QUERY_TRANS = "execQueryTrans";
	public static final String EXEC_STORE = "execStore";
	public static final String EXEC_STORE_TRANS = "execStoreTrans";
	public static final String EXEC_NON_QUERY = "execNonQuery";
	public static final String EXEC_NON_QUERY_TRANS = "execNonQueryTrans";
    
    //ARFA
    public static final String FETCH_SYNC_DB = "SyncDB";
    public static final String ARFT_SERVICE = "ARFT_SERVICE";
    
    
    public static final String LAST_UPDATE_DATE = "lastUpdateDate";
}
