package com.nikolay.ar.base.constants;

public class NCodeConstants
{
    //colomns
    public static final int CD_TYPE = 0;
    public static final int CD_CD = 1;
    public static final int CODE_DESC = 2;
    public static final int SORT_ID = 3;
    public static final int ACCESS_FLG = 4;    
    public static final int ACTIVE_FLG = 5;    
    
    //elements of call
    static public int elementsPerRow = 6;    
    
    //types
    public static final String LOG_TYPE = "CT";
    public static final String SCHEDULE_CALL_TYPE = "SCT";
    public static final String FULLFILMENT_HOUSE_TYPE = "FU";
    public static final String SHIP_METHOD_TYPE = "SHP";
    public static final String ORDER_STATUS = "OS";
    public static final String CATEGORY_TYPE = "CTR";
    public static final String MEETING_TYPE = "MT";
    public static final String MEETING_STATUS = "MS";
    public static final String MEETING_EXP = "EXP";
    public static final String PHONE_TYPE = "PTY";
    public static final String EIS_SCREEN = "EIS";
    
    public static final String CATEGORY_TYPE_FIRM = "CTF";
    public static final String CATEGORY_TYPE_OFFICE = "CTO";
    public static final String DASHBOARD_CODES = "BDS";
    public static final String DASHBOARD_GADGETS_AVAILABLE = "BAS";
    
    private NCodeConstants() {
    }
}
