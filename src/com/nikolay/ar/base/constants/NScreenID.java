/*
 * MarsScreenID.java
 *
 * � <your company here>, 2003-2005
 * Confidential and proprietary.
 */

package com.nikolay.ar.base.constants;




/**
 * 
 */
public class NScreenID {
    private NScreenID() {    }
    
    public String FIRM_SCREEN_ID = "110";
    public String REP_SCREEN_ID = "112";
    
} 
