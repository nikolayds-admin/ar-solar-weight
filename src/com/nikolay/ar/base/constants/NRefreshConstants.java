/*
 * MarsRefreshConstants.java
 *
 * � <your company here>, 2003-2005
 * Confidential and proprietary.
 */

package com.nikolay.ar.base.constants;




/**
 * 
 */
public class NRefreshConstants {
    
    public static final int REP_SCREEN_CONSTANT = 1001;
    public static final int MEETING_REFRESH_CONSTANT = 1002;
    public static final int FIRM_SCREEN_REFRESH = 1006;
    public static final int EVENT_LOG_CONSTANT = 1100;
    public static final int CALL_SCREEN_REFRESH_CONSTANT = 1004;
    public static final int REP_MAINTENANCE_SCREEN = 1101;
    public static final int PROFILE_SCREEN_REFRESH_CONSTANT = 1102;
    public static final int COLUMN_CHANGE_REFRESH_CONSTANT = 1103;
    public static final int DASHBORAD_GADGETS_REFRESH_CONSTANT = 1104;
    
    private NRefreshConstants() {    }
} 
