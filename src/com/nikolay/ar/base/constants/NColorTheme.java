/*
 * MarsColorTheme.java
 *
 * � <your company here>, 2003-2005
 * Confidential and proprietary.
 */

package com.nikolay.ar.base.constants;




/**
 * 
 */
public class NColorTheme {
    public int LIST_BACKGROUND_COLOR = 0xFFFFFF;
    public int LIST_HIGHLIGHT_BACKGROUND_COLOR = 0x00CCFF;
    public int ALT_LIST_BACKGROUND_COLOR = 0xEBDDE2;
    
    public String THEME_NAME = "Lavender blush";
    NColorTheme(String name, int listBC, int altListBC, int listHBC) {
        this.THEME_NAME = name;
        this.LIST_BACKGROUND_COLOR = listBC;
        this.LIST_HIGHLIGHT_BACKGROUND_COLOR = listHBC;
        this.ALT_LIST_BACKGROUND_COLOR = altListBC;
    }
} 
