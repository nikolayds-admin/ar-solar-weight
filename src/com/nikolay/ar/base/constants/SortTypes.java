/*
 * SortTypes.java
 *
 * Created on �����, 2004, ������ 4, 11:40
 */

package com.nikolay.ar.base.constants;

/** Sort Constants Class
 *
 * @author  nikolays
 */
public class SortTypes {
    
    /**
     * Sort Elements for Rep
     */
    public static String [] elRep = 
        new String[] {"Name", "Firm", "State", "Office", "Purchases", "Redemptions", "Assets", "ZIP" };
        //, "City", "Zip"
     /**
     * Sort Elements for Firm
     */       
    public static String [] elFirm = 
        new String[] {"State", "Purchases", "Redemptions", "Assets", "Firm"};
     
     /**
     * Sort Elements for Tickler
     */           
    public static String [] elTickler = 
                new String[] {"Date", "Type", "Name"/*, "Priority" */};
                
     /**
     * Sort Elements for Report
     */           
    public static String [] elReport = 
                new String[] {"Amount", "Rep Name", "Product"};       
                
     /**
     * Sort Elements for Office
     */           
    public static String [] elOffice = 
            new String[] {"State", "Purchase", "Redemptions", "Firm", "Name"}; 
                
    public static String [] elHOList =
            new String[] {"Type", "Date"}; 
            
    public static String [] elInv =
            new String[] {"Inv No", "Description", "Unit Cost", "Quantity"};             
                                         
    /** Creates a new instance of SortTypes */
    private SortTypes() {
    }
 
 //Rep values
 /**Rep Sort by Last name*/    
    public final static int ByLastName = 0;    
 /**Rep Sort by Firm name*/        
    public final static int ByFirm = 1;
/**Rep Sort by State*/        
    public final static int ByState = 2;
/**Rep Sort by Office*/        
    public final static int ByOffice = 3;            
/**Rep Sort by Purchases*/        
    public final static int ByPurch = 4;
/**Rep Sort by Redemptions*/        
    public final static int ByRed = 5;
/**Rep Sort by Asstes*/        
    public final static int ByAssets = 6;
/**Rep Sort by Zip*/        
    public final static int ByZip = 7;
/**Rep Sort by City*/        
    public final static int ByCity = 8;
    
    
    
    
//Firm values

/**Firm Sort by State*/    
    public final static int FirmByState = 0;
/**Firm Sort by Purchases*/    
    public final static int FirmByPurch = 1;
/**Firm Sort by Redemptions*/    
    public final static int FirmByRed = 2;
/**Firm Sort by Asstes*/    
    public final static int FirmByAssets = 3;
/**Firm Sort by Firm name*/
    public final static int FirmByName = 4;

//Office values

/**Office Sort by State*/    
    public final static int OfficeByState = 0;
/**Office Sort by Purchases*/    
    public final static int OfficeByPurch = 1;
/**Office Sort by Redemptions*/    
    public final static int OfficeByRed = 2;
/**Office Sort by Asstes*/    
    public final static int OfficeByAssets = 3;
/**Office Sort by Firm name*/
    public final static int OfficeByFirmName = 4;
/**Office Sort by name*/
    public final static int OfficeByName = 5;

//Tickler values

/**Tickler Sort by Date*/    
    public final static int TicklerByDate = 0;
/**Tickler Sort by Type*/    
    public final static int TicklerByType = 1;    
/**Tickler Sort by Name*/    
    public final static int TicklerByName = 2;

//Report values

/**Report Sort by Amount*/    
    public final static int ReportByAmount = 0;
/**Report Sort by RepName*/    
    public final static int ReportByRepname = 1;    
/**Report Sort by Product*/    
    public final static int ReportByProduct = 2;

/**
 * <description>
 */
    public final static int EISDefault = 0;
}
