package com.nikolay.ar.base.constants;

public class NCommunicationConstants {

	public static final int USER_COMMUNICATOR = 760205;
	public static final int STATIC_DATA_COMMUNICATOR = 760206;
	
	
	public static final int USER_COMMUNICATOR_LOGIN = 0;
	public static final int USER_COMMUNICATOR_USER_DATA = 1;
	
	public static final int DEFAULT_PARAM = 0;
	
	
}
