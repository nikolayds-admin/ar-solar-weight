/*
 * MarsConstants.java
 *
 * Created on �����, 2004, ������ 4, 11:14
 */

package com.nikolay.ar.base.constants;

import android.graphics.Bitmap;
import android.os.Build;



/**
 *
 * @author  nikolays
 */
public class NConstants {
	
	public static boolean IS_AMAZON_MARKET = false;
    public static java.lang.String Author = "Nikolay Sarmadzhiev";
	public static final int TWO_MINUTES = 1000 * 60 * 2;
	public static final char BEL = 0x0007;
	
	//Accuracy constant
    public static final float DEFAULT_ACCURACY_RANGE = 50f;
    
    public static final String SERVER_TIME_ZONE = "America/Los_Angeles";
    
    //Size Constants
    public static final int MAX_NOTE_SZ = 1000;     
    
    //Others
    public static final int SLOW_DEVICE_FIX_INTERVAL = 12000; //in milisec
    
    //App Constants
    public static boolean isExtensiveLogOn = true;
    public static boolean isDebugLogging = true;
    public static final int LOG_EVENTS_TO_SEND_MAX = 200;
    
    
    public static final int FROM_LOCAL = 1000;
    public static final int FROM_NET = 1001;

    //public  static String SAMPLE_HTTPS_PAGE = "https://www.blackberry.com/go/mobile/samplehttps.shtml";
    public  static final String[] HTTP_PROTOCOL = {"http://", "http:\\", "https://", "https:\\"};
    public  static final char HTML_TAG_OPEN = '<';
    public  static final char HTML_TAG_CLOSE = '>';
    public  static String HEADER_CONTENTTYPE = "content-type";
    public  static String CONTENTTYPE_TEXTHTML = "text/html";
    public  static String CONTENTTYPE_TEXT = "text";    
    
    public static final int TIMEOUT = 500; //ms
    public static final int INTERVAL = 6;
    public static final int THREAD_TIMEOUT = 1000;
    
    public static final int SAVE_THREAD_SLEEP = 5000; //ms
    
    //next Object Constant
    public static final String nextObjectString = "Sledvasht";   
    
    //URLStringSeparator
    public static final String URLSEPARATOR = "|sldvsht|";
    
      
    //BooleanDev
    public static final String TRUE = "True";
    public static final String FALSE = "False";
    public static final String YES = "Yes";
    public static final String NO = "No";
    
    
    
    //Size of Colomns
    public static final int MAX_COLOMN_SIZE = 2001;
    public static final char ERROR_SYMBOL = 'E';
    public static final char SUCCESS_SYMBOL = 'S';
    public static final int BEG_OFFSET = 1;
    
    //wait constants
    public static final int REP_LIST_WAIT_TIME = 2000; //2 sec
    
    //Connection Constants
    public static final int MDS = 0;
    public static final int DIRECT_INTERNET = 1;
    public static final int WAP2 = 2;
    public static final int WAP_PROVIDER_INTERNET = 3;
    
    //Set at login time
    public static int CONNECTION_METHOD = MDS;
    
    
    public static final String DEFAULT_DOMAIN = "a";
    public static String DEFAULT_SCREEN_ID = "110";
    
    //wap Constants
    public static String WAP_PROVIDER_NONE = "0_NONE_WAP";
    public static String WAP_PROVIDER_TMOBILE = "T-MOBILE";
    public static String WAP_PROVIDER_CINGULAR = "CINGULAR";
    public static String WAP_PROVIDER_ATT = "ATT";
    public static String WAP_PROVIDER_VERIZON = "VERIZON";
    public static String WAP_PROVIDER_SPRINT = "SPRINT";
    public static String WAP_PROVIDER_OTHER = "DIFFERENT";
    
    //set by user
    //default
    public static String WAP_PROVIDER = WAP_PROVIDER_NONE;
    //endwap
    public static String HOST = "http://10.10.5.66";  
    public static String DOMAIN_NAME = "MARS_TEST"; 
     
    //Firms, Reps, Offices per list
    public static int FIRMS_PER_LIST = 25;
    public static int REPS_PER_LIST = 25;   
    public static int OFFICES_PER_LIST =25; 
    public static int TICKLER_PER_LIST = 25;
    public static int HISTORY_PER_LIST = 25;
    
    //Params for Query
    public static int NUMBER_OF_PARAMS = 1;
    public static int NUMBER_OF_CHARS = 1;
    //end usersettings
    
  
    
    //EIS
    public static int EIS_PER_LIST = 25;

    
    public static int MAX_EIS_COLOMNS = 50;
    

        
    //Width
    public static int MARS_LABEL_WIDTH_MIN = 95; 
    public static int MARS_LABEL_WIDTH_DEV = 30; 
    
    private final static int HIGH_REZ_WIDTH_CONST = 321;
    
    private final static int DEFAULT_SCREEN_SIZE = 320;
    
    public final static int HIGH_RES_MODAL_LIST_TEXT_COLOR = 0x000000;
    
    public static boolean isHighResolution() {
        if(getScreenWidth()>HIGH_REZ_WIDTH_CONST) return true;
        else return false;
    }
    
    @Deprecated
    public static int getScreenWidth() {
        return 0;//net.rim.device.api.system.Display.getWidth();
    }
    @Deprecated
    public static int getScreenHeight() {
        return 0;//net.rim.device.api.system.Display.getHeight();    
    }
    @Deprecated
    public static int getColumnSeparatorWidth() {
        int sz = 0;//net.rim.device.api.system.Display.getWidth()/100;
        return sz;
    }
    
    @Deprecated
    public static int getMarsLabelWidth() {        
        int sw = 0;//net.rim.device.api.system.Display.getWidth();         
        int mid = sw * MARS_LABEL_WIDTH_DEV;
        int ret = mid / 100;        
        if(ret<MARS_LABEL_WIDTH_MIN) ret = MARS_LABEL_WIDTH_MIN;
        return ret;
    }
    
    @Deprecated
    public static int getNormalizedSize(int size) {
    	  int sw = 0;//net.rim.device.api.system.Display.getWidth();         
          int percPerScreen = ( size * 100) / DEFAULT_SCREEN_SIZE;
          int mid = sw * percPerScreen;
          int ret = mid / 100;        
          if(ret<size) ret = size;
          return ret;
    }
    
    @Deprecated
    public static int getDefaultSpaceHight() {
        return 4;                
    }
    
    public static String getRepNotFoundMessage(String repID) {
        String ret = "Rep not found. You may not have access to this rep territory. RepID = "+repID + ".";
        return ret;
    }
      
    //Start Screen Choices
    public static String[] START_SCREENS = new String []{"Home", "Dashboard", "Today", "Tickler"};
    
    //MARS ColorThemes Preset
    public static String[] THEME_NAMES = new String []{"Light Slate Gray", "Lady Turquoise", "Cornflower Blue", "Dark Sea Green", "Khaki1", "Light golden rod", "Light Blue", "Cyan", "Lavendar Blush"};
    
    public static NColorTheme[] THEMES = {   new NColorTheme("Light Slate Gray", 0xFFFFFF, 0xCCCCCC, 0x00CCFF), 
                                                new NColorTheme("Lady Turquoise", 0xFFFFFF, 0xCCFFFF, 0xFFCCFF),
                                                new NColorTheme("Cornflower Blue", 0xFFFFFF, 0xCCCCFF, 0xCCFF66),
                                                new NColorTheme("Dark Sea Green", 0xFFFFFF, 0xC3FDB8, 0x00CCFF),
                                                new NColorTheme("Khaki1", 0xFFFFFF, 0xFFF380, 0xCC9900),
                                                new NColorTheme("Light golden rod", 0xFFFFFF, 0xFAF8CC, 0xCC9900), 
                                                new NColorTheme("Light Blue", 0xFFFFFF, 0x95B9C7, 0x00CCFF),
                                                new NColorTheme("Cyan", 0xFFFFFF, 0xE0FFFF, 0x00CCFF),
                                                new NColorTheme("Lavendar Blush", 0xFFFFFF, 0xEBDDE2, 0x00CCFF)                                                                                              
                                                };
    
    //Preset Fonts
    public static String[] FONT_NAMES = new String[]{"BBAlpha Sans", "BBAlpha Serif", "BBSerif", "BBMillbank"};

    //Default font name
     public static String DEFAULT_FONT_NAME = "BBAlpha Serif";
     
     //Refresh Constants
     public static final int REPORTS_REFRESH = 11001;
    
     //High Res
     @Deprecated
     public static Bitmap getSalesFocusBitmap() {
         if(isHighResolution()){
            return null;//Bitmap.getBitmapResource("SalesFocus2High.png");
         } else {
            return null;//Bitmap.getBitmapResource("SalesFocus2.png");
         }
     } 
     /*
     public static Bitmap getBitmapResource( String name)
     {     	
    	 Bitmap bt = Bitmap.getBitmapResource(name);

    	 if(isHighResolution()) {    		 
    		 int nw = (bt.getWidth() * getScreenWidth())/(HIGH_REZ_WIDTH_CONST-1);
    		 int nh = (bt.getHeight() * getScreenWidth())/(HIGH_REZ_WIDTH_CONST-1);
    	 
    		 bt = resampleImage(bt, nw, nh);
    	 }
    	 
     	 return bt; 
     }*/
     
     /*
     public static EncodedImage scaleImageToWidth(EncodedImage encoded, int newWidth) {
 		return scaleToFactor(encoded, encoded.getWidth(), newWidth);
 	}
 	
 	public static EncodedImage scaleImageToHeight(EncodedImage encoded, int newHeight) {
 		return scaleToFactor(encoded, encoded.getHeight(), newHeight);
 	}
 	
 	
 	public static EncodedImage scaleToFactor(EncodedImage encoded, int curSize, int newSize) {
 		int numerator = Fixed32.toFP(curSize);
 		int denominator = Fixed32.toFP(newSize);
 		int scale = Fixed32.div(numerator, denominator);

 		return encoded.scaleImage32(scale, scale);
 	}
 	*/
     @Deprecated 
 	public static Bitmap resampleImage(Bitmap orgImage, int newWidth, int newHeight)
    {

    	 return null;
    	 /*
        int orgWidth = orgImage.getWidth();
        int orgHeight = orgImage.getHeight();
        int orgLength = orgWidth * orgHeight;
        int orgMax = orgLength - 1;

        int[] rawInput = new int[orgLength];
        orgImage.getARGB(rawInput, 0, orgWidth, 0, 0, orgWidth, orgHeight);

        int newLength = newWidth * newHeight;

        int[] rawOutput = new int[newLength];

        int yd = (orgHeight / newHeight - 1) * orgWidth;
        int yr = orgHeight % newHeight;
        int xd = orgWidth / newWidth;
        int xr = orgWidth % newWidth;
        int outOffset = 0;
        int inOffset = 0;

        // Whole pile of non array variables for the loop.
        int pixelA, pixelB, pixelC, pixelD;
        int xo, yo;
        int weightA, weightB, weightC, weightD;
        int redA, redB, redC, redD;
        int greenA, greenB, greenC, greenD;
        int blueA, blueB, blueC, blueD;
        int red, green, blue;

        for (int y = newHeight, ye = 0; y > 0; y--)
        {
            for (int x = newWidth, xe = 0; x > 0; x--)
            {

                // Set source pixels.
                pixelA = inOffset;
                pixelB = pixelA + 1;
                pixelC = pixelA + orgWidth;
                pixelD = pixelC + 1;

                // Get pixel values from array for speed, avoiding overflow.
                pixelA = rawInput[pixelA];
                pixelB = pixelB > orgMax ? pixelA : rawInput[pixelB];
                pixelC = pixelC > orgMax ? pixelA : rawInput[pixelC];
                pixelD = pixelD > orgMax ? pixelB : rawInput[pixelD];

                // Calculate pixel weights from error values xe & ye.
                xo = (xe << 8) / newWidth;
                yo = (ye << 8) / newHeight;
                weightD = xo * yo;
                weightC = (yo << 8) - weightD;
                weightB = (xo << 8) - weightD;
                weightA = 0x10000 - weightB - weightC - weightD;

                // Isolate colour channels.
                redA = pixelA >> 16;
                redB = pixelB >> 16;
                redC = pixelC >> 16;
                redD = pixelD >> 16;
                greenA = pixelA & 0x00FF00;
                greenB = pixelB & 0x00FF00;
                greenC = pixelC & 0x00FF00;
                greenD = pixelD & 0x00FF00;
                blueA = pixelA & 0x0000FF;
                blueB = pixelB & 0x0000FF;
                blueC = pixelC & 0x0000FF;
                blueD = pixelD & 0x0000FF;

                // Calculate new pixels colour and mask.
                red = 0x00FF0000 & (redA * weightA + redB * weightB + redC * weightC + redD * weightD);
                green = 0xFF000000 & (greenA * weightA + greenB * weightB + greenC * weightC + greenD * weightD);
                blue = 0x00FF0000 & (blueA * weightA + blueB * weightB + blueC * weightC + blueD * weightD);

                // Store pixel in output buffer and increment offset.
                rawOutput[outOffset++] = red + ((green | blue) >> 16);

                // Increment input by x delta.
                inOffset += xd;

                // Correct if we have a roll over error.
                xe += xr;
                if (xe >= newWidth)
                {
                    xe -= newWidth;
                    inOffset++;
                }
            }

            // Increment input by y delta.
            inOffset += yd;

            // Correct if we have a roll over error.
            ye += yr;
            if (ye >= newHeight)
            {
                ye -= newHeight;
                inOffset += orgWidth;
            }
        }
        Bitmap bitmap = new Bitmap(newWidth, newHeight);
        bitmap.setARGB(rawOutput, 0, newWidth, 0, 0, newWidth, newHeight);
        return bitmap;
        */
    }


     
     
     //Temp 
     public static boolean isFastDevice() {
         return NConstants.isHighResolution();
     }
     
     @Deprecated
     public static boolean isBlackberryMapPresent() {
    	 return false;
    	 /*
    	 //USING THE APPLICATION MANAGER
         //(RUNNING APPS)
         //get the ApplicationManager
         ApplicationManager appMan
           = ApplicationManager.getApplicationManager();

         //grab the running applications
         ApplicationDescriptor[] appDes
           = appMan.getVisibleApplications();

         //check for the version of a standard
         //RIM app. I like to use the ribbon
         //app but you can check the version
         //of any RIM module as they will all
         //be the same.
         int size = appDes.length;

         for (int i = size-1; i>=0; --i){
             if ((appDes[i].getModuleName()).
               equals("net_rim_bb_maps_app")){
                 return true;
             }
         }
         
         return false;
         */
     }
     
     public static String getDeviceOSVersion() {
    	 return Build.VERSION.CODENAME + "|" +Build.VERSION.RELEASE + "|" + Build.VERSION.SDK;
    	 /*
    	 //USING THE APPLICATION MANAGER
         //(RUNNING APPS)
         //get the ApplicationManager
         ApplicationManager appMan
           = ApplicationManager.getApplicationManager();

         //grab the running applications
         ApplicationDescriptor[] appDes
           = appMan.getVisibleApplications();

         //check for the version of a standard
         //RIM app. I like to use the ribbon
         //app but you can check the version
         //of any RIM module as they will all
         //be the same.
         int size = appDes.length;
         
         String ret = "";

         for (int i = size-1; i>=0; --i){
             if ((appDes[i].getModuleName()).
               equals("net_rim_bb_ribbon_app")){
                 ret =appDes[i].getVersion();
             }
         }
         
         return ret;*/
     }
     
     public static boolean isHighResDeviceBasedOnOS() {
    	 String ver = getDeviceOSVersion();
    	 if(ver.startsWith("4.6") || ver.startsWith("4.7.1")){
    		 return true;    	 
    	 } else {
    		 return false;
    	 }    	 
     }
     
     
     public static int getDefaultFontSize() {
    	 return 12;    	
     }
     
     public static boolean isDebugLoggingOn() {
    	 return isDebugLogging;     
     }
     
     public static boolean useDefaultDrawForObjectChoiceFields() {    	 
    	 String ver = getDeviceOSVersion();
    	 if(ver.compareTo("4.6")>0){
    		 return true;    	 
    	 } else {
    		 return false;
    	 }    	   
     }
}
