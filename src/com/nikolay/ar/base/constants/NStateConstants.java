package com.nikolay.ar.base.constants;

public class NStateConstants
{
     //colomns
    public static final int COUNTRY_CD = 0;
    public static final int STATE_ID = 1;
    public static final int STATE_NM = 2;
    public static final int ACTIVE_FLG = 3;
    public static final int UPD_DT = 4;   
    
    //elements of call
    static public int elementsPerRow = 5;
    
    public static final String US = "US";
       
    private NStateConstants(){
    }
}
