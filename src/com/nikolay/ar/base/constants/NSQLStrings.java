/*
 * MarsSQLStrings.java
 *
 * Created on August 17, 2004, 2:36 PM
 */

package com.nikolay.ar.base.constants;

/**
 *
 * @author  nikolays
 */

public class NSQLStrings {
    
    /** Creates a new instance of MarsSQLStrings */
    public NSQLStrings() {
    }
    
    public static final String ext = "/MBS";
    //public static final String ext = "/BB";
        
    public static String REP_LIST_FETCH = ext + "/GetRepList.aspx";    
    public static String MARS_USER_FETCH = ext + "/MarsUserService.aspx";    
    public static String FIRM_SERVICE_FETCH = ext + "/FirmService.aspx";
    public static String OFFICE_SERVICE_FETCH = ext + "/OfficeService.aspx";
    public static String TICKLER_SERVICE_FETCH = ext + "/TicklerService.aspx";
    public static String MARS_STATIC_SERVICE = ext + "/MarsStaticData.aspx";    
    // EIS_SERVICE_FETCH is using Reports Service
    public static String REPORTS_SERVICE = ext + "/MarsReportService.aspx"; 
    public static String MAPPING_SERVICE = ext + "/showMap.aspx";
    public static String VOICE_SERVICE = ext + "/VoiceService.aspx";
    public static String VOICE_PLAYER_SERVICE = ext + "/VoicePlayerService.aspx";
     
}
