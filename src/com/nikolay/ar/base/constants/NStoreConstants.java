/*
 * MarsStoreConstants.java
 *
 * Created on September 15, 2004, 4:29 PM
 */

package com.nikolay.ar.base.constants;

/**
 *
 * @author  nikolays
 */


public class NStoreConstants {
    
    /** Creates a new instance of MarsStoreConstants */
    public NStoreConstants() {
    }
    
    public static String MARSUSER = "MarsUser";
    public static String MARSSTATICDATA = "StaticData";
    public static String REPSORT = "RepSort";
    public static String REPLIST = "RepList";
    public static String FIRMSORT = "FirmSort";
    public static String FIRMLIST = "FirmList";    
    public static String TICKLERSORT = "TicklerSort";
    public static String TICKLERLIST = "TicklerList";  
    public static String BCALENDERLIST = "BlackberryCaledarInserts";
    public static String UPDATEQUERY = "UpdateQuery";
    
    public static String OFFICESORT = "OfficeSort";
    public static String OFFICELIST = "OfficeList";  
    
    public static String EISSORT = "EISSort";
    public static String EISLIST = "EISList";  
    
    public static String DRILLDOWNS = "DRILLDOWNS";
    
    public static String THEME = "MARSTHEME";
    
    public static String EVENTS = "EventLog";
}
