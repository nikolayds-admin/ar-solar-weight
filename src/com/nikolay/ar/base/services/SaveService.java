/*
 * MarsUpdate.java
 *
 * Created on December 29, 2004, 8:37 AM
 */

package com.nikolay.ar.base.services;

import java.util.*;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.nikolay.ar.base.activities.NBaseActivity;
import com.nikolay.ar.base.communication.*;
import com.nikolay.ar.base.constants.*;
import com.nikolay.ar.base.store.*;
import com.nikolay.ar.base.trans.*;
import com.nikolay.ar.base.utils.NUtilities;

/**
 *
 * @author  nikolays
 */
public class SaveService extends Thread  implements IDataParser, NParameterConstants {
    
    private boolean start = false;
    private boolean stop = false;
    
    private String theUrl = "";
    private NStore ma = null;
    private ArUser mu = null;
    private NCommunication marsComm; 
    
    protected String getTheUrl() {
    	return this.theUrl;
    }
    
    /** Creates a new instance of MarsUpdate */
    public SaveService(NBaseActivity activity, ArUser mu) {
        this.ma = NStore.getStoreInstance();
        this.mu = mu;
        marsComm = new NCommunication(activity, this);                
    }
        
    public void startService(){
    	ma.logEvent("startService", "start", false);
        if ( start ) {      
        } else {
            synchronized(this)
            {
                if ( start ) {                   
                    //Dialog.alert(_resources.getString(HTTP_ALERT_REQUESTINPROGRESS));
                } else {   
                    start = true;                                                            
                    if(stop) {
                       stop = false;
                       this.start();
                    }                                     
                }
            }
        }
    }

    //public static int INTERVALS =  10;
    private int _ict = 0;
    private boolean inRefresh = false;
    private boolean inStartReceive = false;
    private int nbr = 1;
    
    public void run() {
        for(;;) {
            //Thread control
            while( !start && !stop) {
                //sleep for a bit so we don't spin
                try {
                    sleep(NConstants.TIMEOUT);
                } catch (InterruptedException e) {
                    System.err.println(e.toString());
                }
            }
            
            //exit condition
            if ( stop ) { 
                return;
            }
            
            //This entire block is synchronized, this ensures I won't miss fetch requests made while i process a page
             synchronized(this) {
                 start = false;
                 
                 boolean a = true;
                 if(!inRefresh) {
                    ACTION = 0;
                    a = servContainer();
                    if(a) nbr = 1;
                    else nbr=5;
                } else {
                    if(ACTION==2) {
                        if(!inStartReceive){
                            try {
                                sleep(NConstants.SAVE_THREAD_SLEEP*2);
                            } catch (InterruptedException e) {
                                System.err.println(e.toString());
                            }             
                            syncTickler();
                        }
                    }
                }
                 
                 if(!a) {                    
                    if(_ict == ma.getMarsTheme().getBBCalendarIdleTime()){
                        ACTION = 1;
                        //Start Tickler update
                        refreshTickler();
                        _ict=0;
                    } else {
                        _ict++;
                    }                 
                }
                 
                 start = true;
             }
             
             //Sleep before next serv (to reduce battery usage
             try {
                 sleep(NConstants.SAVE_THREAD_SLEEP*nbr);
             } catch (InterruptedException e) {
                    System.err.println(e.toString());
             }             
        }             
    }
    
    
    //Tickler 
    private int ACTION = 0;
    //0 - default (serv container), 
    //1 - check if updates
    //2 - update Tickler
    
    
    synchronized private void refreshTickler() {
        inRefresh = true;
        checkTickler();
    }
    
    synchronized private void checkTickler() {            
        ACTION = 1;
        marsComm.fetchData();
    }
    
    synchronized public void syncTickler() {        
        ACTION = 2;
        inStartReceive = true;
        marsComm.fetchData();
    }
    
    
    synchronized public void syncDoJob(StringBuffer buffer){        
        //do job
        Vector<String> data = null;
        
        try {
            data = NUtilities.convertDataToVector(buffer);
        } catch (Exception e){
            NUtilities.logError(null, this, e, true, false);
            return;
        }
        
        if((data==null)||data.size()==0) {
            return;
        }
        
        //Convert Data to Tickler Items
        /*
        Enumeration<String> el = data.elements();
        
        int c =0;
      
        Vector<String> vec = new Vector<String>();
        Vector<MarsTicklerItem> rez1 = new Vector<MarsTicklerItem>();   //update, insert             
        Vector<MarsTicklerItem> rez2 = new Vector<MarsTicklerItem>();   //delete
        ma.logEvent("SS::syncDoJob", "Start", false);
        while(el.hasMoreElements()){
            String str = el.nextElement().toString();                        
            vec.addElement(str);
            c++;
            if(c==MarsTicklerItem.elementsPerRow) {
                MarsTicklerItem act = new MarsTicklerItem(vec);
                if(act.getStatCD().compareTo("D")==0) {
                    rez2.addElement(act);                                                    
                } else {
                    rez1.addElement(act);                                                    
                }
                vec.removeAllElements();
                c=0;
            }  
        }    
        //Sync
        if(rez1.size()!=0 || rez2.size()!=0){
            ma.logEvent("SS::syncDoJob", "Begins", false);
            NCalendarHelper.updateBBPIMEvents(rez1, rez2);
        }
        
        //end
        ACTION = 0;
        inRefresh = false;
        inStartReceive = false;
        ma.logEvent("SS::syncDoJob", "Done.", false);
        */
    }
    
    public int inx = 0;   
    boolean lastRequestFinish = true;
    boolean lastRequestFail = false;
    SaveElement curEl = null;
         
    synchronized private boolean servContainer(){        
        if(!lastRequestFinish) return true;
        
        Vector<SaveElement> container = ma.getUpdateContainer();
        if((container==null)||(container.size()==0)){
            return false;        
        }
        ma.logEvent("servContainer", "Get Container...", false);
        inx = 0;
        
        SaveElement el = (SaveElement) container.elementAt(inx);
        if(curEl!=null) {
            //if(el.hashCode()==curEl.hashCode()){
            if(lastRequestFail){
                if(container.size()>1){                
                    inx = 1; 
                    el = (SaveElement) container.elementAt(inx);
                }
            }
        }        
        curEl = el;        
        lastRequestFinish = false; 
        marsComm.fetchData(true);
        ma.logEvent("servContainer", "Done.", false);
        return true;
    }    
    
    public String getUrl() {
        String url = mu.getServer();  
        switch(ACTION) {
            case 0 :
                //please use MarsSaveElement;
                url+= curEl.getIUString();              
                break;
                //not ready
            case 1: //Check if Updates
                inRefresh = true;
                url+= NSQLStrings.TICKLER_SERVICE_FETCH;
               break;
            case 2: //Updates
                inRefresh = true;
                inStartReceive = true;
                url+= NSQLStrings.TICKLER_SERVICE_FETCH;
              break;
        }
        //MarsUtilities.log("SS::getUrl", url); 
        return url;         
    }
    
    public synchronized boolean parseData(StringBuffer buffer, String type) {
        switch (ACTION) {
            case 0:
                Vector<SaveElement> container = ma.getUpdateContainer();
                ma.logEvent("SS::parseData(0)", "Saving finish of "+buffer.toString(), false);
                if((container!=null)&&(container.size()>inx)) {        
                    //container.removeElementAt(inx);
                    ma.removeUpdateElement(inx);
                    inx = 0;
                    lastRequestFinish = true;
                    lastRequestFail = false;
                    NUtilities.log("SS::parseData(0)", "U/I Complete:" + buffer.toString()); 
                }
                break;
            case 1:
                //check if 
                NUtilities.log("SS::parseData(1)", "Count:"+buffer.toString()); 
                String aa = buffer.toString();
                if(aa.startsWith("S0")){                    
                    inRefresh = false;
                } else {
                    ACTION = 2;
                }
                break;
            case 2: 
                NUtilities.log("SS::parseData(2)", "Start sync"); 
                syncDoJob(buffer);
                NUtilities.log("SS::parseData(2)", "End sync"); 
                inStartReceive = false;
                inRefresh = false;
                ACTION = 0;
                break;    
        }
        return true;
    }
    
    public synchronized void resetData() {
    }
    
    public synchronized void setError(com.nikolay.ar.base.utils.NError err) {
        
        NUtilities.logError(null, null, "U/I Failed:" + err.toString(), true, false);
        
        //Error refresh
        if(ACTION!=0) {
            ACTION = 0;
            inRefresh = false;
            inStartReceive = false;
            return;
        }
        
        lastRequestFinish = true;
        if((err.getType()==com.nikolay.ar.base.utils.NError.CONNECTION_TYPE)
            ||(err.getType()==com.nikolay.ar.base.utils.NError.EXCEPTION_TYPE)) {
                
            lastRequestFail = true;
        } else {
            Vector<SaveElement> container = ma.getUpdateContainer();
            if((container!=null)&&(container.size()>inx)) {        
                ma.removeUpdateElement(inx);
                //container.removeElementAt(inx);
                inx = 0;            
            }
            lastRequestFail = false;
        }
    }
    
    public synchronized void stopTheThreads() {
        NUtilities.log("SS::stopTheThreads", "Stopping threads"); 
        stop = true;
        marsComm.stopConnThread();        
        NUtilities.log("SS::stopTheThreads", "Stopping threads done."); 
    }
    
    public synchronized void updateContent(java.lang.String content) {
    }
    
    
    public synchronized boolean updateStatus(java.lang.String content, java.lang.String type) {
        return true;
    } 
     
   
	@Override	
	public List<NameValuePair> getNameValuePairs() {		
		//Please implement save serving of the container
		/*
		 MarsSaveElement element = curEl;       
	        if(element==null) return "";
	        String  url = MarsConstants.USERNAME + "=" + mu.getUsername();
	        url+="&" + MarsConstants.PASSWORD + "=" + mu.getPassword();
	        //please use MarsSaveElement;
	        url+=element.getParam();    
	        */
		List<NameValuePair> ret = new ArrayList<NameValuePair>();
    	ret.add(new BasicNameValuePair(USERNAME, mu.getUsername()));
    	ret.add(new BasicNameValuePair(PASSWORD, mu.getPassword()));                

    	switch(ACTION) {
            case 0 :
                //please use MarsSaveElement;
                //url+=curEl.getParam();        
                break;
                //not ready
            case 1: //Check if Updates
                inRefresh = true;               
                ret.add(new BasicNameValuePair( SELECTOR, ticklerListSelector));
                ret.add(new BasicNameValuePair(COMMAND, CHECK_COMMAND));                                
                break;
            case 2: //Updates
                inRefresh = true;
                inStartReceive = true;                
                ret.add(new BasicNameValuePair(SELECTOR, ticklerListSelector));
                ret.add(new BasicNameValuePair(COMMAND, fetchCommand));                                
                ret.add(new BasicNameValuePair(ONLY_CHANGED_TILCKER_ITEMS, NConstants.YES));
                break;
        }	            
	        
		return ret;
	}         

}
