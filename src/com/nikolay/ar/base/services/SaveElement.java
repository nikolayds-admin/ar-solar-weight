/*
 * MarsSaveElement.java
 *
 * Created on December 29, 2004, 9:07 AM
 */

package com.nikolay.ar.base.services;

import java.util.*;

import com.nikolay.ar.base.trans.ArBaseTrans;

/**
 *
 * @author  nikolays
 */
public class SaveElement extends ArBaseTrans  {
    
    private String updateStringService = "";
    private String urlString = "";   
    
    /** Creates a new instance of MarsSaveElement */
    public SaveElement(String uiString, Hashtable<String, String> ht, boolean isEnc) {
    	super(null);
        this.updateStringService = uiString;        
        Enumeration<String> ks = ht.keys();
        while(ks.hasMoreElements()){
            String el = ks.nextElement();
            String p = (String) ht.get(el);
            if(isEnc){
                p = com.nikolay.ar.base.utils.NUtilities.encodingString(p);
            }
            urlString+="&" + (String) el + "=" + p;
        }
    }
    
    public String getIUString(){
        return updateStringService;
    }    
    
    public String getParam(){
        return urlString;
    }
}
