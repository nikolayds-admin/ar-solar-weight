/*
 * MarsUserPrefInfo.java
 *
 * � <your company here>, 2003-2005
 * Confidential and proprietary.
 */

package com.nikolay.ar.base.trans;

/**
 *
 * @author  nikolays
 */


import java.util.*;

import com.nikolay.ar.base.constants.NConstants;

public class ArUserPrefInfo extends ArBaseTrans {
    public static final int USERID = 0;
    public static final int KEY = 1;
    public static final int VALUE = 2;
    public static final int ENTRY_DATE = 3;
    public static final int ENTRY_USR = 4;
    
    public static final int elementsPerRow = 5;
    
    /** Creates a new instance of MarsUser */
    public ArUserPrefInfo(Vector<String> par) {
    	super(par);
        try{
            key = ((String) par.elementAt(KEY)).trim();        
            value = ((String) par.elementAt(VALUE)).trim();  
        }catch(Exception ex) {
        }
    }        
    private String key = "";
    private String value = "";
    
    public String getKey() {
        return key;
    }
    
    public String getValue() {
        return value;
    }
    
    public boolean getValueAsBoolean() {
        if(value.startsWith("T")) return true;
        else return false;
    }
    
    public int getValueAsInt() {
        try{
            return Integer.parseInt(value, 10);
        } catch (Exception ex) {
        }
        return -1;
    }    
    
    
    public static ArUserPrefInfo getValuePerKey(Vector<ArUserPrefInfo> allPref, String key) {
        Enumeration<ArUserPrefInfo> en = allPref.elements();
        while(en.hasMoreElements()){
            ArUserPrefInfo el = (ArUserPrefInfo) en.nextElement();
            if(el.getKey().compareTo(key)==0) {
                return el;              
            }
        }
        return null;
    }   
    
    private static String lastVals = "";
    
    public static String getLastValues() {
        return lastVals;
    }
    
    public static String getKeysForInsert(Hashtable<String, String> elements, String valuesForInsert) {
        String ret = "";
        Enumeration<String> en = elements.keys();
        while(en.hasMoreElements()) {
            String k = (String) en.nextElement();
            String v = (String) elements.get(k);
            ret+=k;
            ret+=NConstants.URLSEPARATOR;
            valuesForInsert+=v;
            valuesForInsert+=NConstants.URLSEPARATOR;
        }
        
        lastVals = valuesForInsert;
        
        return ret;
    }
    
    //KEYS TO SAVE
    public static String EIS_START_SCREEN = "ESS";
    public static String IDLE_SYNC = "ISY";
    public static String MAX_THREADS = "MTH";
    public static String SAVE_LOG_ON = "SLO";
    public static String MARS_START_SCREEN = "MSS";
    public static String MV_REP = "MVR";
    public static String MV_OFFICE = "MVO";
    public static String MV_FIRM = "MVF";
    public static String DEFAULT_MEETING_TYPE = "DMT";
    public static String DEFAULT_MEETING_DESC = "DMD";
    
    public static String COLOR_SCHEME = "DCS";
    public static String FONT_NAME = "DFN";
    public static String FONT_SIZE = "DFZ";

    public static String LIST_FONT_NAME = "LFN";
    public static String LIST_FONT_SIZE = "LFZ";
}
