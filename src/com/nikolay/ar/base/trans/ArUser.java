/*
 * MarsUser.java
 *
 * Created on August 18, 2004, 11:15 AM
 */

package com.nikolay.ar.base.trans;

/**
 *
 * @author  nikolays
 */

import com.nikolay.ar.base.constants.NConstants;

import java.util.*;



public class ArUser {
    
    /** Creates a new instance of MarsUser */
    public static int USERNAME = 0;
    public static int PASSWORD = 1;    
    public static int  FIRST_NM = 2;
    public static int  LAST_NM = 3;
    public static int URL = 4;
    public static int DOMAIN = 5;
    public static int NO_OF_REPS = 6;
    public static int NO_OF_OFFICES = 7;
    public static int NO_OF_FIRMS = 8;
    public static int NO_OF_TICKLER_ITEMS = 9;
    public static int NO_OF_HISTORY_ITEMS = 10;
    public static int START_REPL_SEQ = 11;
    public static int END_REPL_SEQ = 12;
    public static int RNOP = 13;
    public static int RNCP = 14;        
    public static int CLIENT_NAME = 15;
    public static int CLIENT_NUMBER = 16;
    public static int CLIENT_NUMBER_EXT = 17;
    public static int CLIENT_EMAIL = 18;
    public static int CUSTOM_LABEL_1 = 19;
    public static int CUSTOM_FIELD_1 = 20;    
    public static int CURR_ASSETS_YEAR = 21;    
    public static int INT_WHOLESALER_USERID = 22;
    public static int MARS_SERVER_VERSION = 23;
    public static int MARS_SERVER_UPGRADE_URL = 24;
    public static int IS_PROFILING_ON = 25;
    public static int REMOTE_DOWNLOAD_FLAG = 26;
        
    public static final int ELEMENTS_PER_ROW = 27;

    
    
    public ArUser(Vector<String> par) {        
       this((String)par.elementAt(USERNAME), 
            (String)par.elementAt(PASSWORD), 
            (String)par.elementAt(FIRST_NM), 
            (String)par.elementAt(LAST_NM), 
            (String)par.elementAt(URL), 
            (String)par.elementAt(DOMAIN), 
            (String)par.elementAt(NO_OF_REPS), 
            (String)par.elementAt(NO_OF_OFFICES), 
            (String)par.elementAt(NO_OF_FIRMS), 
            (String)par.elementAt(NO_OF_HISTORY_ITEMS), 
            (String)par.elementAt(NO_OF_TICKLER_ITEMS), 
            (String)par.elementAt(START_REPL_SEQ), 
            (String)par.elementAt(END_REPL_SEQ), 
            (String)par.elementAt(RNOP), 
            (String)par.elementAt(RNCP),
            (String)par.elementAt(CLIENT_NAME),
            (String)par.elementAt(CLIENT_NUMBER), 
            (String)par.elementAt(CLIENT_NUMBER_EXT), 
            (String)par.elementAt(CLIENT_EMAIL), 
            (String)par.elementAt(CUSTOM_LABEL_1), 
            (String)par.elementAt(CUSTOM_FIELD_1),
            (String)par.elementAt(CURR_ASSETS_YEAR),
            (String)par.elementAt(INT_WHOLESALER_USERID),
            (String)par.elementAt(MARS_SERVER_VERSION),
            (String)par.elementAt(MARS_SERVER_UPGRADE_URL),                
            (String)par.elementAt(IS_PROFILING_ON),
            (String)par.elementAt(REMOTE_DOWNLOAD_FLAG)
            );
    }
    
    public ArUser(String username, String password, String firstName, String lastName, 
                    String url, String domain, String _reps, String _offices, String _firms, 
                    String _history, String _tickler, String startSeq, String endSeq,
                    String _numOfParam, String _numOfChar, String clName, String clNumber, 
                    String clExt, String clEmail, String ctLabel_1, String ctField_1, String currAssetsYear, 
                    String intWholesalerUserID, String marsServerVersion, String upgradeUrl, String isProfileOn, String remoteDownloadFlag) {        
        this.clientName = clName.trim();
        this.clientNumber = clNumber.trim();
        this.clientExt = clExt.trim();
        this.clientEmail = clEmail.trim();
        this.customLabel_1 = ctLabel_1;
        this.customField_1 = ctField_1;
        this.username = username.trim();
        this.password = password.trim();
        this.firstName = firstName;
        this.lastName = lastName;
        this.url = url;
        this.domain = domain;        
        this.startSeq = startSeq;
        this.endSeq = endSeq;
        this.eisScreenID = NConstants.DEFAULT_SCREEN_ID;
        int reps = NConstants.REPS_PER_LIST;
        int firms = NConstants.FIRMS_PER_LIST;
        int offices = NConstants.OFFICES_PER_LIST;
        int history = NConstants.HISTORY_PER_LIST;
        int ticklers = NConstants.TICKLER_PER_LIST;
        int nop = NConstants.NUMBER_OF_PARAMS;
        int noc = NConstants.NUMBER_OF_CHARS;
        this.currAssetsYear = currAssetsYear;
        this.intWholesalerUserID = intWholesalerUserID;
        this.marsServerVersion = marsServerVersion;
        this.upgradeUrl = upgradeUrl;
        this.isProfileOn = isProfileOn;        
        if(this.isProfileOn!=null) isProfileOn = isProfileOn.trim();        
        
        if(remoteDownloadFlag.trim().compareTo("1")==0) isRemoteDownload = "Y";
        else isRemoteDownload = "N";
        
        try {
            reps = Integer.parseInt(_reps);
            firms = Integer.parseInt(_firms);
            offices = Integer.parseInt(_offices);
            ticklers = Integer.parseInt(_tickler);
            history = Integer.parseInt(_history);
            nop = Integer.parseInt(_numOfParam);
            noc = Integer.parseInt(_numOfChar);
        }catch(Exception ex) {
        }        
        
        this.firmPerList = firms;
        this.repPerList = reps;
        this.officesPerList = offices;
        this.numOfChars = noc;
        this.numOfParams = nop;
        
        NConstants.HOST = url;
        NConstants.FIRMS_PER_LIST = firms;
        NConstants.REPS_PER_LIST = reps;
        NConstants.OFFICES_PER_LIST = offices;
        NConstants.TICKLER_PER_LIST = ticklers;
        NConstants.HISTORY_PER_LIST = history;
        NConstants.DOMAIN_NAME = this.domain;
        
        
        this.wapProvider = NConstants.WAP_PROVIDER_NONE;        
        this.wapPort = "9201";
        this.wapIP = "127.0.0.1";
        this.wapAPN = "Blackberry.net";
        this.wapUsername = "";
        this.wapPassword = "";
        this.wapEnableWTLS = "FALSE"; //if true default port is 9203
        
    }
    
    String username;
    String firstName;
    String lastName;
    String password;
    String url;
    String domain;
    String startSeq;
    String endSeq; 
    int numOfParams;
    int numOfChars;
    
    //list Parameters
    int firmPerList;
    int officesPerList;
    int repPerList;    
    
    //Wap Parameters
    String wapProvider;
    String wapUsername;
    String wapPassword;
    String wapAPN;
    String wapIP;
    String wapPort;
    String wapEnableWTLS;     
    
    //About custom Params
    String clientNumber;
    String clientName;
    String clientExt;
    String clientEmail;
    String customLabel_1;
    String customField_1;   
    
    //EIS Default Parametes
    String eisScreenID;   
    
    //Current Assets
    String currAssetsYear = "";
    String intWholesalerUserID = "";
      
    String marsServerVersion = "";
    String upgradeUrl = "";
    
    //Profile
    String isProfileOn = "";
    
    String isRemoteDownload = "Y";
    
    //Connection Type    
    private int connectionType = NConstants.MDS;
    
    public int getConnectionType() {
        return connectionType;
    }
    
    public void setConnectionType(int ct) {
        connectionType = ct;
    }
    
    public boolean isRemoteDownloadOnForUser() {
    	if(this.isRemoteDownload.compareTo("Y")==0) return true;
    	else return false;    	
    }
    
    public boolean isProfileOnForUser() {
    	boolean def = true;
    	if(isProfileOn==null) return def;
    	else {
    		if(isProfileOn.compareTo("0")==0) return false;
    		else return true;
    	}    	
    }
    
    public String getNextSeq(){
        String ret = startSeq;
        String udep = "";
        String dig = "";
        String digE = "";
        
        try {
            udep = startSeq.substring(0, 4);
            dig = startSeq.substring(4);
            digE = endSeq.substring(4);            
            
            long d = Long.parseLong(dig, 10);
            long de = Long.parseLong(digE, 10);
            //check the end Element//Temp is out
            if(d==de) return "-1";
            //end      
            d+=1;
            dig = Long.toString(d);  
            while(dig.length()<6){
                dig = "0"+dig;
            }
            
            startSeq = udep+dig;
            ret = startSeq;
        } catch (Exception ex) {
        }
        return ret;        
    }
    
    public String getUpgradeUrl() {
        return this.upgradeUrl;
    }
    
    public String getMarsServerVersion() {
        return this.marsServerVersion;
    }
    
    public int getRepsPerList(){
        return repPerList;
    }
    
    public void setRepPerList(int repPerList){
        NConstants.REPS_PER_LIST = repPerList;
        this.repPerList = repPerList;        
    }
    
    public int getFirmsPerList(){
        return firmPerList;
    }
    
    public void setFirmPerList(int firmPerList){
        NConstants.FIRMS_PER_LIST = firmPerList;
        this.firmPerList = firmPerList;        
    }
    
    public int getOfficesPerList(){
        return officesPerList;
    }
    
    public void setOfficesPerList(int officesPerList){
        NConstants.FIRMS_PER_LIST = officesPerList;
        this.officesPerList = officesPerList;        
    }
    
    public String getClientName() {
        return clientName;
    }
    
    public String getClientNumber() {
        return clientNumber;
    }
    
    public String getClientExt() {
        return clientExt;
    }
    
    public String getClientEmail() {
        return clientEmail;
    }    
    
    public String getCustomLabel_1(){
        return customLabel_1;
    }
    
    public String getCustomField_1(){
        return customField_1;
    }
    
    public String getEISScreenID(){
        return eisScreenID;
    }
    
    public void setEISScreenID(String eisScreenID){
        this.eisScreenID = eisScreenID;
        NConstants.DEFAULT_SCREEN_ID = this.eisScreenID;
    }
    
    public String getDomain(){
        return domain;
    }
    
    public void setDomain(String domain){
        this.domain = domain;
        NConstants.DOMAIN_NAME = this.domain;
    }
    
    public void setUserName(String username){
        this.username = username;
    }
    
    public String getUsername() {
        return username;    
    }
    
    public String getFirstName(){
        return firstName;
    }
    
    public String getLastName(){
        return lastName;    
    }
    
    public String getPassword(){
        return password;
    }     
    
    public String getServer(){
        return url;
    }    
    
    public void setServer(String url){
        if(url.toLowerCase().startsWith("https")) 
        {
            if(url.indexOf(":443") == -1) 
            {
               url+=":443"; 
            }
        }
        NConstants.HOST = url;
        this.url = url;
    } 
    
    public void setWapProdiver(String prov) {
        NConstants.WAP_PROVIDER = prov;
        this.wapProvider = prov;
    }   
    
    public String getWapProdiver() {        
        return this.wapProvider;
    }   
    
    public void setWapIP(String ip) {        
        this.wapIP = ip;
    }   
    
    public String getWapIP() {        
        return this.wapIP;
    }    
    
    public void setWapAPN(String apn) {        
        this.wapAPN = apn;
    }   
    
    public String getWapAPN() {        
        return this.wapAPN;
    }        
    
    public void setWapPort(String port) {        
        this.wapPort = port;
    }   
    
    public String getWapPort() {        
        return this.wapPort;
    }        
    
    public void setWapEnableWTLS(boolean tr) {        
        if(tr)
            this.wapEnableWTLS = "TRUE";
        else
            this.wapEnableWTLS = "FALSE";
    }   
    
    public boolean isWapEnableWTLS() {        
        if(this.wapEnableWTLS.charAt(0)=='T') return true;
        else return false;
    }        
    
    public void setWapUsername(String username) {        
        this.wapUsername = username;
    }   
    
    public String getWapUsername() {        
        return this.wapUsername;
    }        
    
    public void setWapPasswrod(String password) {        
        this.wapPassword = password;
    }   
    
    public String getWapPassword() {        
        return this.wapPassword;
    }   
    
    public int getNumberOfChars(){
        return this.numOfChars;
    } 
    
    public int getNumberOfParams(){
        return this.numOfParams;
    }
    
    public void setCurrAssetsYear(String val) {
        this.currAssetsYear = val;
    }
 
     public String getCurrAssetsYear(){
        if(this.currAssetsYear==null || this.currAssetsYear.length()==0) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            return Integer.toString((cal.get(Calendar.YEAR)));
        } else {
            return this.currAssetsYear;
        }
     }      
     
     public String getCurrAssetsYearShort(){
          String curYearShort = getCurrAssetsYear().substring(2);
          return "("+curYearShort+")";
     }
     
     public String getLastAssetsYear(){
        
        String lastYear = this.getCurrAssetsYear();
        
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());     
        String ret = Integer.toString((cal.get(Calendar.YEAR)-1));
        
        try {
            int y = Integer.parseInt(lastYear)-1;
            ret = Integer.toString(y);
        } catch(Exception ex) {
        }
        
        return ret;
     }   
     
     public String getLastAssetsYearShort(){
        String lastYearShort = getLastAssetsYear().substring(2);
        return "("+lastYearShort+")";              
     }
     
     public String getIntWholeSalesUserID() {
         return this.intWholesalerUserID;
     }
     
     public void setIntWholeSalesUserID(String val) {
         this.intWholesalerUserID = val.trim();
     }
     
     
     
     /*
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        String curYear = Integer.toString((cal.get(Calendar.YEAR)));
        String lastYear = Integer.toString((cal.get(Calendar.YEAR)-1));
        
        currentH.setName(curYear);
        lastH.setName(lastYear);
        
        String curYearShort = curYear.substring(2);
        curYearShort = "("+curYearShort+")";
        
        String lastYearShort = lastYear.substring(2);
        lastYearShort = "("+lastYearShort+")";     
     */
        
}
