package com.nikolay.ar.base.trans;

import java.io.Serializable;
import java.util.List;


public class NDicatePragraph implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2849720573050359728L;
	
	public static int STOP = 0;
	public static int STARTED = 1;
	public static int DONE = 2;
	
	int time = 0;
	String line = null;
	String paragraphID = "";
	
	int status = NDicatePragraph.STOP;
	
	public NDicatePragraph(String line, String paragraphID, int timeInSec) {
		this.line = line;
		this.time = timeInSec * 1000;
		this.paragraphID = paragraphID;
	}
	
	public String getLine() {
		return line;
	}
	
	public int getDelayAfterMilliseconds() {
		return this.time;
	}
	
	public String getParagraphID() {
		return this.paragraphID.trim();
	}
	
	public int getStatus() {
		return status;
	}
	
	public void setStarted() {
		status = STARTED;
	}
	
	public void setStopped() {
		status = STOP;
	}

	public void setDone() {
		status = DONE;
	}
	
	public static NDicatePragraph getParagraphByParagraphID(List<NDicatePragraph> list, String pID) {
		for(NDicatePragraph prg : list) {
			if(prg.getParagraphID().compareToIgnoreCase(pID)==0) {
				return prg;
			}
		}
		return null;
	}
	
}