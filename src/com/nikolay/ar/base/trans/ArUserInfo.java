package com.nikolay.ar.base.trans;

/**
 *
 * @author  nikolays
 */

import java.util.*;



public class ArUserInfo extends ArBaseTrans {
    public static final int USR_ID = 0;
    public static final int FIRST_NM = 1;
    public static final int LAST_NM = 2;
    public static final int USR_ENCRYPT_PSWD = 3;
    public static final int USR_EMAIL = 4;
    
    public static final int USR_CUR_ASSETS_YEAR = 5;
    
    public static final int elementsPerRow = 5;
    
    /** Creates a new instance of MarsUser */
    public ArUserInfo(Vector<String> par) {
    	super(par);
        try{
            username = (String) par.elementAt(USR_ID);        
            firstName = (String) par.elementAt(FIRST_NM);        
            lastName = (String) par.elementAt(LAST_NM);        
            password = (String) par.elementAt(USR_ENCRYPT_PSWD); 
            email = (String) par.elementAt(USR_EMAIL); 
            
            currAssetsYear = (String) par.elementAt(USR_CUR_ASSETS_YEAR); 
        }catch(Exception ex) {
        }
    }
    
    String username;
    String firstName;
    String lastName;
    String password;
    String email;
    String currAssetsYear = "";
    

    
    public void setUserName(String username){
        this.username = username;
    }
    
    public String getUsername() {
        String ret = username;
        if(ret==null) return null;
        else return ret.trim();    
    }
    
    public String getFirstName(){
        return firstName;
    }
    
    public String getLastName(){
        return lastName;    
    }
    
    public String getEmail(){
        return email;
    } 
    
    public String getPassword(){
        return password;
    }   
    
     public String getCurrAssetsYear(){
        if(this.currAssetsYear==null || this.currAssetsYear.length()==0) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            return Integer.toString((cal.get(Calendar.YEAR)));
        } else {
            return this.currAssetsYear;
        }
     }      
     
     public String getLastAssetsYear(){
        
        String lastYear = this.getCurrAssetsYear();
        
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());     
        String ret = Integer.toString((cal.get(Calendar.YEAR)-1));
        
        try {
            int y = Integer.parseInt(lastYear)-1;
            ret = Integer.toString(y);
        } catch(Exception ex) {
        }
        
        return ret;
     }   
    
    public static ArUserInfo getUserInfoByID(Vector<ArUserInfo> users, String userID){
        userID = userID.trim();
        Enumeration<ArUserInfo> el = users.elements();
        while(el.hasMoreElements()){
            ArUserInfo usr = (ArUserInfo) el.nextElement();
            if(usr.getUsername().compareTo(userID)==0)
                return usr;
        }                
        return null;                
    }
    
    public static String getNameByID(Vector<ArUserInfo> users, String userID){
        ArUserInfo usr = getUserInfoByID(users, userID);
        if(usr==null) return userID;
        return usr.getFirstName() + " " + usr.getLastName();
    }
}
