/*
 * MarsState.java
 *
 * Created on November 16, 2004, 3:36 PM
 */

package com.nikolay.ar.base.trans;

import java.util.*;

import com.nikolay.ar.base.constants.NStateConstants;


/**
 *
 * @author  nikolays
 */
public class NState extends ArBaseTrans {
    
    private String country;
    private String stateID;
    private String name;
    //private String activeFlag;
    //private String lastUpdate;
    
    
    /** Creates a new instance of MarsState */
    public NState(Vector<String> info) {
    	super(info);
        country = (String) info.elementAt(NStateConstants.COUNTRY_CD);     
        stateID = (String) info.elementAt(NStateConstants.STATE_ID);     
        name = (String) info.elementAt(NStateConstants.STATE_NM);     
        //activeFlag = (String) info.elementAt(MarsStateConstants.ACTIVE_FLG); 
        //lastUpdate = (String) info.elementAt(MarsStateConstants.UPD_DT); 
    }
    
    public String getCountry(){
        return country.trim();
    }
    
    public String getStateID(){
        return stateID.trim();
    }

    public String getName(){
        return name.trim();
    }
    
    /*
    public String getActiveFlag(){
        return activeFlag;
    }   */
    

//Helpers    
    public static String[] getStateIDs(Vector<NState> st, String country){
        Vector<NState> sts = getStates(st, country);
        
        if(sts == null) return null;
        
        String ret[] = new String[sts.size()];
        for(int i = 0; i<sts.size(); i++){
            NState state = (NState) sts.elementAt(i);
            ret[i] = state.getStateID();
        }
        return ret;
    }

    public static String[] getStateNames(Vector<NState> st, String country){
        Vector<NState> sts = getStates(st, country);
        
        if(sts == null) return null;
        
        String ret[] = new String[sts.size()];
        for(int i = 0; i<sts.size(); i++){
            NState state = (NState) sts.elementAt(i);
            ret[i] = state.getName();
        }
        return ret;
    }
    
    
    public static Vector<NState> getStates(Vector<NState> st, String country) {
        Vector<NState> ret = new Vector<NState>();
        Enumeration<NState> el = st.elements();
        while(el.hasMoreElements()){
            NState state = (NState) el.nextElement();
            if(state.getCountry().compareTo(country)==0)
                ret.addElement(state);
        }                
        return ret;
    }   
    
    public static Vector<String> getCountries(Vector<NState> st) {
        Vector<String> ret = new Vector<String>();
        Enumeration<NState> el = st.elements();
        while(el.hasMoreElements()){
            NState state = (NState) el.nextElement();
            Enumeration<String> cel = ret.elements();
            boolean exist = false;
            
            while(cel.hasMoreElements()){
                String ct = (String)cel.nextElement();
                if(ct.trim().compareTo(state.getCountry().trim())==0){
                    exist = true;
                    break;
                }
            }
            
            if(!exist) ret.addElement(state.getCountry().trim());
        }                
        
        return ret;
    }    
    
    public static String[] getCountryIDs(Vector<String> countries){
        if(countries == null) return null;
        
        String ret[] = new String[countries.size()];
        for(int i = 0; i<countries.size(); i++){            
            ret[i] = ((String) countries.elementAt(i)).trim();
        }
        return ret;        
    }
}
