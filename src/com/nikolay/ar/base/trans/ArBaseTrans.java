/*
 * AMarsTrans.java
 *
 * Created on November 10, 2004, 9:54 AM
 */

package com.nikolay.ar.base.trans;

import java.util.*;

/**
 *
 * @author  nikolays
 */
public class ArBaseTrans  {
    
    protected Vector<String> info;    
    
    /** Creates a new instance of AMarsTrans */
    public ArBaseTrans(Vector<String> info) {
        this.info = info;
    }
    
    public Vector<String> getInfo(){
        return info;
    }
    
    public void setInfo(Vector<String> information){
        info = information;
    }    
}
