/*
 * MarsCode.java
 *
 * Created on November 16, 2004, 3:36 PM
 */

package com.nikolay.ar.base.trans;

import java.util.*;

import com.nikolay.ar.base.constants.NCodeConstants;


/**
 *
 * @author  nikolays
 */
public class NCode  {
    
    private String type;
    private String code;
    private String desc;
    private String accessFlag;
    private String sortID;    
    private String active;
    
    /** Creates a new instance of MarsCode */
    public NCode(String type, String code, String desc, String accessFlag, String sortID, String active) {
        Vector<String> a = new Vector<String>();
        a.addElement(type);
        a.addElement(code);
        a.addElement(desc);
        a.addElement(accessFlag);
        a.addElement(sortID);
        a.addElement(active); 
        setElements(a);
    }
    
    private void setElements(Vector<String> info){    	
        type = (String) info.elementAt(NCodeConstants.CD_TYPE);
        type = type.trim();
        code = (String) info.elementAt(NCodeConstants.CD_CD);
        desc = (String) info.elementAt(NCodeConstants.CODE_DESC);
        accessFlag = (String) info.elementAt(NCodeConstants.ACCESS_FLG);
        sortID = (String) info.elementAt(NCodeConstants.SORT_ID); 
        active = (String) info.elementAt(NCodeConstants.ACTIVE_FLG);
    }
    
     public NCode(Vector<String> info) {
        setElements(info);
    }
    
    public boolean isActive(){
        if(active.trim().equalsIgnoreCase("N"))
            return false;
        else 
            return true;
    }
    
    public String getActive() {
        return active.trim();
    }
    
    public String getType() {
        return type.trim();
    }
    
    public String getCode() {
        return code.trim();
    }
    
    public String getDesc() {
        return desc.trim();
    }    
    
    public String getAccessFlag() {
        return accessFlag.trim();
    }  
    
    public String getSortID(){
        return sortID.trim();
    }
    
    //Helpers for Code
    public static Vector<NCode> getCodes(Vector<NCode> st, String type) {
        return getCodes(st, type, true);
    }
    
    public static Vector<NCode> getCodes(Vector<NCode> st, String type, boolean includeInactive) {        
        Vector<NCode> ret = new Vector<NCode>();
        
        if(type.equalsIgnoreCase(NCodeConstants.PHONE_TYPE))
        {
            ret.addElement(new NCode(NCodeConstants.PHONE_TYPE,"Home", "Home" , "Y", "0", "Y"));
            ret.addElement(new NCode(NCodeConstants.PHONE_TYPE,"Work", "Work" , "Y", "0", "Y"));
            ret.addElement(new NCode(NCodeConstants.PHONE_TYPE,"Mobile", "Mobile" , "Y", "0", "Y"));
            ret.addElement(new NCode(NCodeConstants.PHONE_TYPE,"Pager", "Pager" , "Y", "0", "Y"));
            ret.addElement(new NCode(NCodeConstants.PHONE_TYPE,"Fax", "Fax" , "Y", "0", "Y"));
            ret.addElement(new NCode(NCodeConstants.PHONE_TYPE,"Other", "Other" , "Y", "0", "Y"));
            
            return ret;
        }
        
        Enumeration<NCode> el = st.elements();
        type = type.trim();
        while(el.hasMoreElements()){
            NCode code = (NCode) el.nextElement();
            if(code.getType().compareTo(type)==0) {
                if(includeInactive || code.isActive())
                    ret.addElement(code);
            }
        }                
        return ret;
    }   
    
    public static NCode getCodeByID(Vector<NCode> st, String codeID) {        
        Enumeration<NCode> el = st.elements();
        codeID = codeID.trim();
        while(el.hasMoreElements()){
            NCode code = (NCode) el.nextElement();
            if(code.getCode().compareTo(codeID)==0)
                return code;
        }                
        return null;
    }         
    
    public static int getCodeInxByID(Vector<NCode> st, String codeID) {        
        Enumeration<NCode> el = st.elements();
        int ret = 0;
        codeID = codeID.trim();
        while(el.hasMoreElements()){
            NCode code = (NCode) el.nextElement();            
            if(code.getCode().compareTo(codeID)==0)
                return ret;
            ret++;  
        }                
        return -1;
    }         
    
    public static NCode getCodeByCodeID(Vector<NCode> st, String codeID) {        
        Enumeration<NCode> el = st.elements();        
        codeID = codeID.trim();
        while(el.hasMoreElements()){
            NCode code = (NCode) el.nextElement();            
            if(code.getCode().compareTo(codeID)==0)
                return code;            
        }                
        return null;
    }         
    
}
