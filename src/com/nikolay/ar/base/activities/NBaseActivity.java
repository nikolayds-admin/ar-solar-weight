package com.nikolay.ar.base.activities;
import java.util.ArrayList;
import java.util.Vector;

//import com.nikolay.ar.base.R;
//import com.bugsense.trace.BugSenseHandler;
import com.nikolay.ar.base.communication.NCommunication;
import com.nikolay.ar.base.constants.NConstants;
import com.nikolay.ar.base.store.NStore;
import com.nikolay.ar.base.trans.ArUser;
import com.nikolay.ar.base.trans.ArUserPrefInfo;
import com.nikolay.ar.base.trans.NCode;
import com.nikolay.ar.base.utils.NPreferences;
import com.nikolay.ar.base.utils.NTheme;
import com.nikolay.ar.base.utils.RetrieveThread;
import com.nikolay.ar.base.utils.ThreadCallback;
import com.nikolay.solar.R;
//import com.nikolay.arfa.R;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public abstract class NBaseActivity extends Activity implements ThreadCallback, com.nikolay.ar.base.utils.BundleConstants {
    /** Called when the activity is first created. */
	
	public static final int PROGRESS_DIALOG_ID = 0;
	NotificationManager mNM = null;
	
	private NPreferences nPreferences = null; 
	
	private static final String TAG = NBaseActivity.class.getName();
	protected static ArrayList<NBaseActivity> activities = new ArrayList<NBaseActivity>();


	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);     
        //Save main/home/nikolay/workplace/ARFirstAid
		//Setup Preferences
		this.getNPreferences().initPreferences();
		//Custom Init Preferences
		onNSetupPrefereces();
		//end setup Preferences
        setContentView(getLayoutID());
        //Add Global Exception handler
        //BugSenseHandler.setup(this, "32642c79");
    	
        onNCreate(savedInstanceState);    
        activities.add(this);
    }
    
    public void showNotification(int id, int icon, String text) {
    	// Set the icon, scrolling text and timestamp
        Notification notification = new Notification(icon, text,
                System.currentTimeMillis());

        // The PendingIntent to launch our activity if the user selects this notification
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, NReturnFromMessage.class), 0);

        // Set the info for the views that show in the notification panel.
        notification.setLatestEventInfo(this, "", text, contentIntent);

        // Send the notification.
        // We use a layout id because it is a unique number.  We use it later to cancel.
        getNotificationManager().notify(id, notification);
    }	
    
    public NotificationManager getNotificationManager() {
    	if(mNM==null) {
    		mNM = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
    	}
    	
    	return mNM;
    }
    
    /**
     * Override this method in Order to setup custom Preferences.
     */
    protected void onNSetupPrefereces() {
    	
    }
    
    @Override
    protected void onStart(){
    	super.onStart();
    	onNStart();
    }
    
    @Override
    protected void onRestart() {
    	super.onRestart();
    }

    @Override
    protected void onResume(){
    	super.onResume();
    	onNResume();
    }

    protected void onNResume() {
		
	}

	@Override
    protected void onPause(){
    	super.onPause();
    	onNPause();
    }

    protected void onNPause() {
		
	}

	@Override
    protected void onStop(){
    	super.onStop();
    }

    @Override
    protected void onDestroy(){
    	onNDestroy();
    	super.onDestroy();
    	activities.remove(this);   	
    }
    
    @Override
    protected void onNewIntent (Intent intent) {
    	onNNewIntent(intent);
    	super.onNewIntent(intent);
    }
    
    protected void onNNewIntent(Intent intent) {
		
	}

	protected void onNStart(){    	
    }
    
    public abstract void onNCreate(Bundle savedInstanceState);
   
    protected int getLayoutID() {
		return R.layout.underconstruction;
	}
    
    protected int getMenuID(){
    	return R.menu.main_menu;
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(getMenuID(), menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
    	if(onNOptionsItemSelected(item)) {
    		return true;
    	}
    	
        switch (item.getItemId()) {
        //case R.id.preferences:
        //	return callPreferences();
               
        case R.id.about:
        	return showAbout();
        default:
            return super.onOptionsItemSelected(item);
        }
    }
    
    /* Implement this method to handle menu items. Please do not override onOptionItemSelected,
     * unless you know what are u doing.
     * 
     */
    protected boolean onNOptionsItemSelected(MenuItem item) {
    	return false;
    }
    
    public abstract boolean showAbout();

	protected abstract boolean callPreferences();
    
    
    public void onStartActivity(Intent intent) {
    	super.startActivity(intent);
    }


    public synchronized void logEvent(String method, String text) {
		getStore().logEvent(method, text, false);
	}
    
	
	public NPreferences getNPreferences() {
		if(nPreferences == null)
			nPreferences =  new NPreferences(NBaseActivity.this);
		
		return nPreferences;
	}

	public String getURL() {
		//TODO Implement it correctly
		//return getNPreferences().getPrefURL();
		
		//TODO FIX THE LOAD BALANCING IN Lower Level Code
		String server0 = "http://callyourdoctor.eu:8880";
		String server1 = "http://callyourdoctor.eu:8881";
		String server2 = "http://callyourdoctor.eu:8883";
		String server3 = "http://callyourdoctor.eu:8884";
		
		String server_old0 = "http://callyourdoctor.eu:8882";
		String server_old1 = "http://callyourdoctor.eu:8885";
		
		//TODO Switch 3 servers based on the last 
		//digit of the Number
		String server = server0;
		String server_old = server_old0;
		
		//TODO Check if server is fail before call!!!
		// and switch to another one!!!
		
		//TODO Debug 
		//server = "http://192.168.2.54:8080";
		//server_old = "http://192.168.2.54:8080";
		
		//TODO Debug on VM1
		server = server_old;//"http://192.168.1.170:8080";
		//server_old = "http://192.168.2.77:8882";
		
		if(!getNPreferences().isEvaluationCompleted() 
				|| getNPreferences().isCertifitedPhone()) {
			//Primary 
			return server+"/GluServer/BaseServlet";
		} else {
			//Third Server
			return server_old + "/GluServer/BaseServlet";
		}
	}
	
	public ArUser getUser(){
	      return getStore().getUser();
	}
	
	public NStore getStore() {
		return NStore.getStoreInstance();
	}
	
	public NCode getCodeByCodeCD(String codeCD) {
		NCode rez = NCode.getCodeByCodeID(getStore().getCodes(false), codeCD);
		return rez;
	}
	
	 @SuppressWarnings({ "unchecked", "deprecation", "rawtypes" })
	 public void setStaticData(Vector data) {
		 Vector calList = getStore().getCalendarActivities();
		 getStore().cleanStore();
		 getStore().setStaticData(data);
		 getStore().setCalendarActivities(calList);
		 
		 Vector up = (Vector) data.elementAt(4);
		 if(up==null || up.size()==0) {
			 return;
		 }
	        
	     ArUserPrefInfo upi = ArUserPrefInfo.getValuePerKey(up, ArUserPrefInfo.COLOR_SCHEME);
	     
	     //Theme
	     NTheme tm =  getStore().getMarsTheme();
	     
	     tm.setColorTheme(NConstants.THEMES[upi.getValueAsInt()], upi.getValueAsInt());
	     upi = ArUserPrefInfo.getValuePerKey(up, ArUserPrefInfo.FONT_NAME);
	     tm.setFontFamilyStr(upi.getValue());
	     upi = ArUserPrefInfo.getValuePerKey(up, ArUserPrefInfo.LIST_FONT_NAME);
	     tm.setListFontFamilyStr(upi.getValue());
	     upi = ArUserPrefInfo.getValuePerKey(up, ArUserPrefInfo.FONT_SIZE);
	     tm.setAppFontSize(upi.getValue());
	     upi = ArUserPrefInfo.getValuePerKey(up, ArUserPrefInfo.LIST_FONT_SIZE);                
	     tm.setListFontSize(upi.getValue());
	        
	     upi = ArUserPrefInfo.getValuePerKey(up, ArUserPrefInfo.MARS_START_SCREEN);                
	     tm.setDefaultStartScreen(upi.getValueAsInt());
	     upi = ArUserPrefInfo.getValuePerKey(up, ArUserPrefInfo.IDLE_SYNC);                        
	     tm.setBBCalendarIdleTime(upi.getValueAsInt());
	     upi = ArUserPrefInfo.getValuePerKey(up, ArUserPrefInfo.MAX_THREADS);                        
	     tm.setMaxThreads(upi.getValueAsInt());
	        
	     upi = ArUserPrefInfo.getValuePerKey(up, ArUserPrefInfo.MV_FIRM);                        
	     tm.setMultiFirmView(upi.getValueAsBoolean());
	     upi = ArUserPrefInfo.getValuePerKey(up, ArUserPrefInfo.MV_REP);                        
	     tm.setMultiRepView(upi.getValueAsBoolean());
	     upi = ArUserPrefInfo.getValuePerKey(up, ArUserPrefInfo.MV_OFFICE);                        
	     tm.setMultiOfficeView(upi.getValueAsBoolean());
	        
	     //tm.setForwardDefaultOn(this.forwardDefaultCF.getChecked());        
	        
	     upi = ArUserPrefInfo.getValuePerKey(up, ArUserPrefInfo.SAVE_LOG_ON);                        
	     tm.setLocalEvents(upi.getValueAsBoolean());
	            

         upi = ArUserPrefInfo.getValuePerKey(up, ArUserPrefInfo.DEFAULT_MEETING_TYPE);                        
	     tm.setDefaultInstantMeetingCode(upi.getValue());        
	     upi = ArUserPrefInfo.getValuePerKey(up, ArUserPrefInfo.DEFAULT_MEETING_DESC);                        
	     tm.setInstantMeetingDesc(upi.getValue());
	        
	     getStore().setMarsTheme(tm);        
         //Font.setDefaultFont(this.getMarsTheme().getAppFont());
    }
	 
	@SuppressWarnings({ "rawtypes" })
	public Vector getStaticData(){
		return getStore().getStaticData();
	}
	
	RetrieveThread retrieveThread = null;
	
	protected void startRetrieve() {
		startRetrieve(0,0);
	}
	
	protected void startRetrieve(int id, int secondID) {
		showDialog(PROGRESS_DIALOG_ID);
		
		try {
			if(retrieveThread!=null) {
				//retrieveThread.destroy();
				retrieveThread = null;
			}
		} catch (Exception ex) {
			Log.e("NBaseActivity::startRetrieve", ex.getMessage());
		}
		
		retrieveThread = new RetrieveThread(this);
		retrieveThread.setID(id);
		this.retrieveThread.setSecondID(secondID);
		this.retrieveThread.start();
	}
	
	@Override	
	public void runMethod(int id, int secondID) {	
		boolean isOK = false;
		String eMessage = "Common. contact support.";
		if(getCommunicator()!=null) {
			StringBuffer buff = getCommunicator().fetchDataSync();
			if(buff==null) {
				isOK = false;
				Log.e("ARFA::NBaseActivity::runMethod", "Server returned empty blog. May be your server is down?");
				eMessage = "Server returned empty block";
			} else {				
				//buff.trimToSize();
				//buff = new StringBuffer(buff.substring(1);
				isOK = this.processData(buff);	
				buff.delete(0, buff.capacity());
				buff=null;
			}
			
			System.gc();
		}
		
		Message myMessage=new Message();
    	if(!isOK) {
    		myMessage.obj="FAIL:"+eMessage;    		
    	} else {
			myMessage.obj=SUCCESS_RESULT;			
    	}
    	myMessage.arg1 = id;
    	myMessage.arg2 = secondID;
    	//getRetrieveHandler().sendMessage(myMessage);
    	NBaseActivity.this.retrieveHandler.sendMessage(myMessage);//.handleMessage(myMessage);
	}
	
	public void setDialogProgressMessage(String message) {
		Message myMessage=new Message();
    	myMessage.obj = "DM:"+message;
		NBaseActivity.this.retrieveHandler.sendMessage(myMessage);
	}
	
	protected boolean processData(StringBuffer buff) {
		return false;
	}

	ProgressDialog progressDialog = null;
	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		  case PROGRESS_DIALOG_ID: { //Retrieve Data
			progressDialog = new ProgressDialog(this);
			progressDialog.setMessage(getDataRetrieveMessage());
			progressDialog.setIndeterminate(true);
			progressDialog.setCancelable(false);
			return progressDialog;
		  }
		}
		return null;
    }
	
	/**
	 * Override to put custom message
	 * @return
	 */
	protected CharSequence getDataRetrieveMessage() {
		return getString(R.string.data_retrieve);
	}

	public static String SUCCESS_RESULT = "SUCCESS";
	
	protected Handler retrieveHandler = new Handler() {
		  @Override
		  public void handleMessage(Message msg) {
			  String dm = (String) msg.obj;
			  if(dm !=null && dm.startsWith("DM:")) {
				  dm = dm.substring(3);
				  //TODO set message
				  setDialogMessage(dm);
				  return;
			  }
			  
			  removeDialog(PROGRESS_DIALOG_ID);
			  if(msg.obj == SUCCESS_RESULT){
				  onFinishRetrieveSuccess();
				  onFinishRetrieveSuccess(msg.arg1, msg.arg2);
			  } else {
				  onFinishRetrieveFailed();
				  onFinishRetrieveFailed(msg.arg1, msg.arg2, msg.obj);
			  }			 
		  }

		private void setDialogMessage(String dm) {
			// TODO Auto-generated method stub
			progressDialog.setMessage(dm);
		}
	};
	
	protected Handler getRetrieveHandler() {
		return NBaseActivity.this.retrieveHandler;
	}
	
	protected void onFinishRetrieveSuccess() {	
	}
	
	protected void onFinishRetrieveSuccess(int id, int secondID) {
		
	}
	
	protected void onFinishRetrieveFailed() {		  
	}

	protected void onFinishRetrieveFailed(int id, int secondID, Object msg) {		
		//Toast.makeText(this, msg.toString(), Toast.LENGTH_LONG);
		 new AlertDialog.Builder(this)
	        .setIcon(android.R.drawable.ic_dialog_alert)
	        .setTitle("Failed")
	        .setMessage(msg.toString())
	        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
	            @Override
	            public void onClick(DialogInterface dialog, int which) {
	            	
	 
	        	}
	        })
	        //.setNegativeButton(R.string.cancel, null)
	        .show();
	}
	
	private NCommunication communicator = null;
	
	protected void setCommunicator(NCommunication com) {
		this.communicator = com;
	}
	
	protected NCommunication getCommunicator() {
		return this.communicator;
	}

	protected boolean onNDestroy() {
		// TODO Auto-generated method stub
		return false;
	}

	protected void onNQuit() {
		//TODO Figure out when and how to call this one.
	}

	public static void finishAll()
	{
		for(Activity activity:activities)
	           activity.finish();
	}

	public String getEmergencyCallNumber() {
		return "";
	}

	public String getCountry() {
		return "";
	}
}
