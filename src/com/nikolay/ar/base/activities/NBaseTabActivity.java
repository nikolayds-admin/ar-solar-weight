package com.nikolay.ar.base.activities;

//import com.nikolay.ar.base.R;
//import com.bugsense.trace.BugSenseHandler;
import com.nikolay.ar.base.utils.BundleConstants;
import com.nikolay.solar.R;

import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.TabHost;

public abstract class  NBaseTabActivity extends TabActivity  implements BundleConstants {
	protected int getLayoutID() {
		return R.layout.underconstruction;
	}
	
	// Resource object to get Drawables
	protected Resources resourceObj;
	// The activity TabHost
	protected TabHost tabHost;
	// Resusable TabSpec for each tab
	protected TabHost.TabSpec spec;
	// Reusable Intent for each tab
	protected Intent intent;
	 
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(getLayoutID());
	    
	    //Add Global Exception handler
        //BugSenseHandler.setup(this, "32642c79");
        //
	    
	    resourceObj = getResources(); 
	    tabHost = getTabHost();  	    
	    onNCreate(savedInstanceState);
	    
	}

	protected abstract void onNCreate(Bundle savedInstanceState);
	
	@Override
	protected void onDestroy() {
		onNDestroy();
		super.onDestroy();
	}

	protected void onNDestroy() {
		
	}
	
	//Typical Implementation
	/*
	 	    // Create an Intent to launch an Activity for the tab (to be reused)
	    intent = new Intent().setClass(this, ArtistsActivity.class);

	    // Initialize a TabSpec for each tab and add it to the TabHost
	    spec = tabHost.newTabSpec("artists").setIndicator("Artists",
	                      res.getDrawable(R.drawable.ic_tab_artists))
	                  .setContent(intent);
	    tabHost.addTab(spec);

	    // Do the same for the other tabs
	    intent = new Intent().setClass(this, AlbumsActivity.class);
	    spec = tabHost.newTabSpec("albums").setIndicator("Albums",
	                      res.getDrawable(R.drawable.ic_tab_albums))
	                  .setContent(intent);
	    tabHost.addTab(spec);

	    intent = new Intent().setClass(this, SongsActivity.class);
	    spec = tabHost.newTabNBaseTabActivitySpec("songs").setIndicator("Songs",
	                      res.getDrawable(R.drawable.ic_tab_songs))
	                  .setContent(intent);
	    tabHost.addTab(spec);

	    tabHost.setCurrentTab(2);
	    
	    */
	 
	 
}
