package com.nikolay.ar.base.web.services;

import java.io.IOException;
import java.io.InputStream;


import java.net.URL;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import android.util.Log;


import com.nikolay.ar.weatherforecast.weather.GoogleWeatherHandler;
import com.nikolay.ar.weatherforecast.weather.WeatherSet;


public class GeoService {

	
	public static WeatherSet getWeatherForAddress(String city, double lat, double lon) {
		URL url;
		WeatherSet ws = null;
		try {
		
			String cityParamString = city;
			
			String queryString = "http://www.google.com/ig/api?weather="
					+ cityParamString;
			// Replace blanks with HTML-Equivalent. 
			url = new URL(queryString.replace(" ", "%20"));

			// Get a SAXParser from the SAXPArserFactory. 
			SAXParserFactory spf = SAXParserFactory.newInstance();
			SAXParser sp = spf.newSAXParser();

			// Get the XMLReader of the SAXParser we created. 
			XMLReader xr = sp.getXMLReader();

			
			GoogleWeatherHandler gwh = new GoogleWeatherHandler();
			xr.setContentHandler(gwh);

			// Parse the xml-data our URL-call returned.
			xr.parse(new InputSource(url.openStream()));

		
			ws = gwh.getWeatherSet();
		

		} catch (Exception e) {
			Log.e("getWeatherTagsForAddress", "WeatherQueryError", e);
		}
	
    	return ws;
	}
	
	public static String getAddressByGEOCoordinated(double lat, double lon) {
		//http://maps.googleapis.com/maps/api/geocode/json?latlng=40.714224,-73.961452&sensor=true
		
		//String latS = Double.toString(lat);
		//String lonS = Double.toString(lon);
		
		String url = "http://maps.googleapis.com/maps/api/geocode/json?latlng="+lat+","+lon+"&sensor=true";
		HttpClient httpclient = new DefaultHttpClient();  
    	HttpGet httppost = new HttpGet(url);  
    	    
    	 try {  
    	    // Add your data  
    	    
    	    //Setting up the headers
    	   
    	    // Execute HTTP Post Request
    	    HttpResponse response = httpclient.execute(httppost);
    	    int status = response.getStatusLine().getStatusCode();
    	    if (status == HttpStatus.SC_OK)  {
                //is this html?
                //String ctype = response.getFirstHeader(MarsConstants.HEADER_CONTENTTYPE).getValue();
                //boolean htmlContent = true;//= (ctype != null && ctype.equals(response.getFirstHeader(MarsConstants.CONTENTTYPE_TEXTHTML).getValue()));//ctype.equals(MarsConstants.CONTENTTYPE_TEXTHTML));
                //boolean textContent = (ctype != null && ctype.equals(MarsConstants.CONTENTTYPE_TEXT));
                
                InputStream input = response.getEntity().getContent();                     
                byte[] data = new byte[1024];
                int len = 0;
                int size = 0;
                StringBuffer raw = new StringBuffer();
                len = input.read(data);
                while ( -1 != len ) {                             
                    raw.append(new String(data, 0, len));
                    size += len;
                    try 
                    {
                    	len = input.read(data);
                    } catch (IOException ioException) {
                    	len = -1;
                    }
                }
                
                try {
                	input.close();
                	response.getEntity().consumeContent();
                } catch (IOException ioException) {
                	
                }
                
              return raw.toString();
    	    }
    	   
    	 } catch (ClientProtocolException e) {
    	 } catch (IOException e) {
    	 }
    	 
    	 return "";
	}
}
