/*
 * MarsGPSHelper.java
 *
 * � <your company here>, 2003-2005
 * Confidential and proprietary.
 */


package com.nikolay.ar.base.utils;

//import java.util.List;

import android.content.Context;
import android.location.*;
import android.os.Bundle;

import com.nikolay.ar.base.constants.*;


/**
 * 
 */
public class NGPSThread extends Thread implements LocationListener {
    
	//private static final String TAG = "LocationDemo";
	//private static final String[] S = { "Out of Service", "Temporarily Unavailable", "Available" };
	
	//private LocationManager locationManager;
	private String bestProvider;
	private Context mContext;
	
	int minTime = 60000;
	int distance = 10;
	int accuracy = 5;
	
	/*
	 * 
	 */
    private NGPSThread(Context mContext, int minTime, int distance, int accuracy) {
    	this.mContext = mContext;
    	this.distance = distance;
    	this.accuracy = accuracy;
    	this.minTime = minTime;
    }
    
    /* 0 to 10 - Accuracy 0 is best. 
     * minTime - min time to rest the gps chip. consider 60000 ms as good
     * distance - meters from the last point
     */
    public static NGPSThread createMGT(Context mContext, int minTime, int distance, int accuracy) {
        return new NGPSThread(mContext, minTime, distance, accuracy);
    }
    
    //double lon = 0;
    //double lat = 0;
    
    ChangeListener _listener = null;
    
    //Coordinates c = null;  
    Location loc;
      
    boolean doneInit = false;
    boolean failed = false;
    String errorMessage = "";
    
    boolean canceled = false;
    boolean _start = false;
    boolean _stop = false;
    
    public void setChangeListener(ChangeListener cl) {
        this._listener = cl;
    }

    private void initLocation(Context mContext, long minTime, long distance) {
        NUtilities.log("MGPST::initLocation", "Start");      
        // Get the location manager
        LocationManager locationManager = (LocationManager)mContext.getSystemService(Context.LOCATION_SERVICE);
      
      
        // List all providers:
        //List<String> providers = locationManager.getAllProviders();
        //for (String provider : providers) {
        	//printProvider(provider);
        //}

        Criteria criteria = new Criteria();
        bestProvider = locationManager.getBestProvider(criteria, true);
        //output.append("\n\nBEST Provider:\n");
        //printProvider(bestProvider);

        //output.append("\n\nLocations (starting with last known):");
        loc = locationManager.getLastKnownLocation(bestProvider);
        //printLocation(location);
        if(locationManager.isProviderEnabled(bestProvider)) {
        	try {
        		locationManager.requestLocationUpdates(bestProvider, minTime, distance, this);
        	} catch (Exception ex){
        		NUtilities.logError("MGT:initLocation:requestLocationUpdates registration", ex);
        	}
        }
        
        NUtilities.log("MGPST::initLocation", "Done");
        callFieldChanged(205);
    }

    public void onLocationChanged(Location location) {
    	this.loc  = location;
    }

    public void onProviderDisabled(String provider) {
    	// let okProvider be bestProvider
    	//re-register for updates
    	//output.append("\n\nProvider Disabled: " + provider);
    }

    public void onProviderEnabled(String provider) {
    	// is provider better than bestProvider?
    	//is yes, bestProvider = provider
    	//output.append("\n\nProvider Enabled: " + provider);
    }

    public void onStatusChanged(String provider, int status, Bundle extras) {
    		//output.append("\n\nProvider Status Changed: " + provider + ", Status="
			//+ S[status] + ", Extras=" + extras);
    }

     public void callFieldChanged(int parm) {
        try {
            
            if(this.isCanceled()) return; 
            
            if(_listener!=null)
                _listener.changed(this, parm);
            
            _listener = null;
        } catch (Exception ex) {
            NUtilities.logError("MGPST::callFieldChanged", ex, false, false);
        }
    }
       
    public void run(){
        for(;;) {
            while( !_start ) {
                //sleep for a bit so we don't spin
                if(_stop) return;
                try {
                    sleep(NConstants.TIMEOUT);
                } catch (InterruptedException e) {                    
                    NUtilities.logError("MGPST:run", e, true, false);
                }
            }                     
            initLocation(mContext, minTime, distance);
            _start = false;
        }
    }
    
    public void startThread() {
        _start = true;    
        this.canceled= false;    
    }
    
      //shutdown the thread  
    /*
    public void stopThread() {
        _start = false; 
        _stop = true;       
    }*/
   
    //Protocol Methods
    /* --Commented as per Compiler not Invoke
    public double getLatitudeAsDouble() {
        return lat;
    }*/
    
    /* --Commented as per Compiler not Invoke
    public double getLongditudeAsDouble() {
        return lon;
    }*/
    
     public String getLatitudeAsString(boolean formatForDevice) {
    	if(loc ==null ) return "0";
    	double lat = loc.getLatitude();
        String str = Double.toString(lat);
        if(!formatForDevice) return str;
        
        if(str.indexOf(".")!=-1) {
                String str1 = str.substring(0, str.indexOf("."));
                String str2 = str.substring(str.indexOf(".")+1);
                while(str2.length()<5){
                    str2+="0";
                }
                 while(str2.length()>5) {
                    str2 = str2.substring(0, str2.length()-1);
                }
                str=str1+str2;
            }            
            
            return str;
    }
    
    public String getLongditudeAsString(boolean formatForDevice) {
    	if(loc ==null ) return "0";
    	double lon = loc.getLongitude();
        String str = Double.toString(lon);
        if(!formatForDevice) return str;
        
           if(str.indexOf(".")!=-1) {
                String str1 = str.substring(0, str.indexOf("."));
                String str2 = str.substring(str.indexOf(".")+1);
                while(str2.length()<5){
                    str2+="0";
                }
                 while(str2.length()>5) {
                    str2 = str2.substring(0, str2.length()-1);
                }
                str=str1+str2;
            }            
            
            return str;
    }
    
    /* --Commented as per Compiler not Invoke
    public Coordinates getCordinates() {
        return c;
    } */

    /* --Commented as per Compiler not Invoke    
    public Location getLocation() {
        return loc;
    }*/
    
    public boolean isDoneRetrieving() {
        return doneInit;
    }
    
    public boolean isFailedRetrieving() {
        return failed;
    }
    
    public boolean isCanceled() {
        return this.canceled;
    }
    
    public boolean isStarted() {
        return this._start;
    }
    
    public void setCancel(boolean val) {
        this.canceled = val;
    }    
    
    public String getLastErrorMessage() {
        return this.errorMessage;
    }   

    /* --Commented as per Compiler not Invoke    
    public void killThread() {
        this.interrupt();
    } */
} 
