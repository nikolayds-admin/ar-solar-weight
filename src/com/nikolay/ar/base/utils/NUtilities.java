/*
 * MarsUtilities.java
 *
 * Created on August 17, 2004, 12:28 PM
 */

package com.nikolay.ar.base.utils;

/**
 *
 * @author  nikolays
 */


import java.util.*;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings.Secure;
import android.speech.RecognizerIntent;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.nikolay.ar.base.activities.NBaseActivity;
import com.nikolay.ar.base.constants.*;
import com.nikolay.ar.base.store.NStore;
import com.nikolay.ar.base.trans.NDicatePragraph;
import com.nikolay.solar.R;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

/*
import com.mars.bb.screens.dialogs.*;
*/

public class NUtilities {
    
    public static void goToUpgrade(String upgradeUrl, Activity activity) {
        log("NU::goToUpgrade", "Go to "+upgradeUrl);        
        
        Uri uri = Uri.parse(upgradeUrl);
    	Intent intent = new Intent(Intent.ACTION_VIEW, uri);
    	activity.startActivity(intent);                       
    }
    

    public static void callTo(String phone, Activity activity) {
        log("NU::callTo", "Call to "+phone);
        Uri uri = Uri.parse("tel:"+phone);
    	Intent intent = new Intent(Intent.ACTION_CALL, uri);
    	activity.startActivity(intent);
    }
    
    public static double round(double dig, int to) {
        try {
            double d1 = dig;
            for(int i=0; i< to; i++) {
                d1 = d1*10;
            }       
                     
                     
            double a = Math.floor(d1);
            
            if((d1-a)<0.5) {                
            } else {
                a++;            
            }
            
            d1 = a;
            
            for(int i=0; i<to; i++)
                d1/=10;
            
            return d1;
            
        } catch (Exception ex) {
            return dig;
        }
    }
    
    public static String getJustDate(String date) {
        String rez = date;        
        int inx = rez.indexOf(' ');
        rez = rez.substring(0, inx);
        return rez;
    }
    
    public static String fixedDBDate(String date) {
        try {
            String rez = date;
            String am = "AM";
            int inx=rez.lastIndexOf('A');
            if(inx==-1) am="PM";                
            inx = rez.indexOf(':');
            rez = rez.substring(0, inx+3);        
            return rez + " " +am;
        } catch (Exception ex){
            return "";
        }
    }
    
    /* --Commented as per Compiler not Invoke
    public static boolean specialChar(char c)
    {
        return c == LF || c == CR;
    }
    */
    
    /* --Commented as per Compiler not Invoke
    public static String prepareData(String text)
    {
        //do some simple operations on the html data

        //a simple state machine for removing tags, comments, whitespace and inserting newlines
        // for the <p> tag.

        final int text_length = text.length();
        StringBuffer data = new StringBuffer(text_length);
        int state = STATE_0;
        int count = 0;
        int writeIndex = -1;
        char c = (char)0;
        //cache the text length and preincrement the counter for
        for ( int i = 0; i < text_length; ++i)
        {
            c = text.charAt(i);
            switch ( state )
            {
                case STATE_0:
                    if ( c == HTML_TAG_OPEN )
                    {
                        ++count;
                        state = STATE_1;
                    }
                    else if ( c == ' ' )
                    {
                        data.insert(++writeIndex, c);
                        state = STATE_5;
                    }
                    else if ( !specialChar(c) )
                    {
                        data.insert(++writeIndex, c);
                    }
                    break;
                case STATE_1:
                    if ( c == '!' && text.charAt(i + 1) == '-' && text.charAt(i + 2) == '-' )
                    {
                        //System.out.println("Entering Comment state");
                        i += 2;
                        state = STATE_3;
                    }
                    else if ( Character.toLowerCase(c) == 'p' )
                    {
                        state = STATE_4;
                    }
                    else if ( c == HTML_TAG_CLOSE )
                    {
                        --count;
                        state = STATE_0;
                    }
                    else
                    {
                        state = STATE_2;
                    }
                    break;
                case STATE_2:
                    if ( c == HTML_TAG_OPEN )
                    {
                        ++count;
                    }
                    else if ( c == HTML_TAG_CLOSE )
                    {
                        if( --count == 0 )
                        {
                            state = STATE_0;
                        }
                    }
                    break;
                case STATE_3:
                    if ( c == '-' && text.charAt(i+1) == '-' && text.charAt(i + 2) == HTML_TAG_CLOSE )
                    {
                        --count;
                        i += 2;
                        state = STATE_0;
                        //System.out.println("Exiting comment state");
                    }
                    break;
                case STATE_4:
                    if ( c == HTML_TAG_CLOSE )
                    {
                        --count;
                        data.insert(++writeIndex, '\n');
                        state = STATE_0;
                    }
                    else
                    {
                        state = STATE_1;
                    }
                    break;
                case STATE_5:
                    if ( c == HTML_TAG_OPEN )
                    {
                        ++count;
                        state = STATE_1;
                    }
                    else if ( c != ' ' )
                    {
                        state = STATE_0;
                        if ( !specialChar(c) )
                        {
                            data.insert(++writeIndex, c);
                        }
                    }
                    break;
            }
        }
        return data.toString().substring(0, writeIndex + 1);
    } */

    public static Vector<String> convertDataToVector(StringBuffer info){        
        //Converting the string to strings array        
        if(info==null) return null; 
        
    	String text = info.toString();
        
        Vector<String> rez = new Vector<String>(5000, 1000);        
        try {
            if (text.charAt(0)==NConstants.ERROR_SYMBOL) {
                logError(null, null, text.substring(1), true, true);
                return null;
            }
        } catch (Exception ex) {
             logError(null, ex, true, true);
             return null;
        }
      
        String del = "b1D";//new String(tt);
        int le = del.length();
        text = text.substring( NConstants.BEG_OFFSET);
        int inx = text.indexOf(del);
        if(inx==-1) {
        	rez.addElement(text);
        } else {
        	while(inx!=-1){
        		String el = text.substring(0, inx);
        		text = text.substring(inx+le);
        		inx = text.indexOf(del);
        		rez.addElement(el);
        	} 
        }
        return rez;
    }
    
    public static String encodingString(String src) {
        
        //Unsave
        //Percent character ("%")         25
        src = replaceStrOnce(src, '%', "%25");
        
        //Requried
        src = replaceStr(src, '\n', "%0A");   
        // Dollar ("$") 24
        src = replaceStr(src, '$', "%24");        
        // Euro
        //src = replaceStr(src, '�', "%80");
        // Pound
        //src = replaceStr(src, '�', "%A3");
        // Yen
        //src = replaceStr(src, '�', "%A5");
        // Cent
        //src = replaceStr(src, '�', "%A2");        
        // Dollar ("$") 24
        src = replaceStr(src, '$', "%24");
        //Ampersand ("&") 26
        src = replaceStr(src, '&', "%26");
        // Plus ("+") 2B
        src = replaceStr(src, '+', "%2B");        
        //Comma (",") 2C
        src = replaceStr(src, ',', "%2C");
        //Forward slash/Virgule ("/") 2F
        src = replaceStr(src, '/', "%2F");
        //Colon (":") 3A
        src = replaceStr(src, ':', "%3A");
        //Semi-colon (";") 3B
        src = replaceStr(src, ';', "%3B");
        //Equals ("=") 3D
        src = replaceStr(src, '+', "%3D");
        //Question mark ("?") 3F
        src = replaceStr(src, '?', "%3F");
        //'At' symbol ("@") 40
        src = replaceStr(src, '@', "%40");
        
        //Unsaved
        src = replaceStr(src, ' ', "%20");
        
        //Quotation marks 22
        src = replaceStr(src, '"', "%22");
        //'Less Than' symbol ("<") 3C
        src = replaceStr(src, '<', "%3C");
        //'Greater Than' symbol (">") 3E
        src = replaceStr(src, '>', "%3E");
        
        //'Pound' character ("#")         23
        src = replaceStr(src, '#', "%23");

        
        //Left Curly Brace ("{") 7B
        src = replaceStr(src, '{', "%7B");
        //Right Curly Brace ("}") 7D
        src = replaceStr(src, '}', "%7D");
        //Vertical Bar/Pipe ("|") 7C
        src = replaceStr(src, '|', "%7C");
        //Backslash ("\") 5C
        src = replaceStr(src, '\\', "%5C");
        //Caret ("^")  5E
        src = replaceStr(src, '^', "%5E");
        //Tilde ("~") 7E
        src = replaceStr(src, '~', "%7E");
        //Left Square Bracket ("[") 5B
        src = replaceStr(src, '[', "%5B");
        //Right Square Bracket ("]") 5D
        src = replaceStr(src, ']', "%5D");
        //Grave Accent ("`")      60        
        src = replaceStr(src, '`', "%60");       
    
        
        return src;
    }
    
    public static String replaceStrOnce(String src, char oldChar, String rep){
        String ret = src;
        if(ret.indexOf(oldChar)!=-1){
            int inx = ret.indexOf(oldChar);
            String t = ret.substring(0, inx);
            String n = ret.substring(inx+1);
            ret=t + rep + n;            
        }
        
        return ret;
        
    }
    
   public static String replaceStr(String src, String oldChar, String rep){
        String ret = src;
        while(ret.indexOf(oldChar)!=-1){
            int inx = ret.indexOf(oldChar);
            String t = ret.substring(0, inx);
            String n = ret.substring(inx+oldChar.length());
            ret=t + rep + n;            
        }
        
        return ret;
    }    
    
    public static String replaceStr(String src, char oldChar, String rep){
        String ret = src;
        while(ret.indexOf(oldChar)!=-1){
            int inx = ret.indexOf(oldChar);
            String t = ret.substring(0, inx);
            String n = ret.substring(inx+1);
            ret=t + rep + n;            
        }
        
        return ret;
    }
    
    public static String convertStringToPhone(String str) {
        if(str.length() !=10) return str;
        
        String ret = "(";
        String ac = str.substring(0, 3);        
        ret+=ac+")";
        ac = str.substring(3, 6);
        ret+=ac + "-";
        ac = str.substring(6);
        ret+=ac;
        return ret;        
    } 
    
    public static String convertPhoneToString(String str) {
        String ret = "";
        for(int i=0; i< str.length();i++){
            char a = str.charAt(i);
            if(a >='0' && a<='9'){
                ret+=new String(new char[] {a});
            }
        }
        return ret;
    
    }   
    
    public static String convertPhoneToString2(String str) {
        if(str.length() !=13) return str;
        if(!str.startsWith("(")) return str;
        
        String ret = "";
        String ac = str.substring(1, 4);        
        ret+=ac;
        ac = str.substring(5, 8);
        ret+=ac;
        ac = str.substring(9);
        ret+=ac;
        return ret;        
    }      
    
    //Use Log.d
    @Deprecated
    public static void logDebug(String method, String text) {
        if(NConstants.isDebugLoggingOn())
            NStore.getStoreInstance().logEvent(method, text, false);               
    }
    
    //Use Log.l
    @Deprecated
    public static void log(String method, String text) {
        if(NConstants.isExtensiveLogOn)
        	NStore.getStoreInstance().logEvent(method, text, false);               
    }
    
    //Use Log.e
    @Deprecated    
    public static void logError(String method, Exception ex) {
        logError(method, ex.toString());
    }
    
    public static void logError(String method, String text){
        text = text.replace('\n', '>');
        NStore.getStoreInstance().logEvent(method, text, true); 
    }
    
    public static void logError(Object obj, Exception ex, boolean isError){
        logError(null, obj, ex, isError, false);
    }
    
    public static void logError(String obj, Exception ex, boolean isError){
        logError(obj, ex, isError, false);
    }
    
    public static void logError(String obj, Exception ex, boolean isError, boolean show){
        if(show)
            showError(null, ex.toString()); 
        
        if(obj==null) obj = "ShowError";    
        NStore.getStoreInstance().logEvent(obj, ex.toString(), isError);               
    }
    
    
    public static void logError(Activity activity, Object obj, Exception ex, boolean isError, boolean show){
        if(show)
            showError(activity, ex.toString()); 
            
        if(obj==null) obj = "ShowError";    
        NStore.getStoreInstance().logEvent(obj.toString(), ex.toString(), isError);               
    }

    public static void logError(Activity activity, Object obj, String str, boolean isError, boolean show){
        if(show)
            showError(activity, str);
         
         if(obj==null) obj = "ShowError";
         NStore.getStoreInstance().logEvent(obj.toString(), str, isError);   
    }
    
    
    public static void showError(Activity activity, String text){
    	Toast.makeText(activity, text, Toast.LENGTH_LONG).show();
    } 
    
    public static Vector<Vector<String>> splitObjects(Vector<String> data) {
        Enumeration<String> el = data.elements();
        Vector<String> t = new Vector<String>();
        Vector<Vector<String>> rez = new Vector<Vector<String>>();
        while(el.hasMoreElements()) {
            String str = "";
            try {
                str = (String) el.nextElement();
            }catch(Exception ex) {                
            }
            
            if(str.compareTo(NConstants.nextObjectString)==0){
                rez.addElement(t);
                t = new Vector<String>();
            } else {
                t.addElement(str);
            }            
        }
        rez.addElement(t);
        return rez;        
    }    
    
   
    public static List<String> splitObjectsAsStrings(StringBuffer buffer) {
    	List<String> ret = new ArrayList<String>();
    	
    	while(buffer.indexOf(NConstants.nextObjectString)!=-1) {
    		int inx = buffer.indexOf(NConstants.nextObjectString);
    		String t = buffer.substring(0, inx);
    		ret.add(t);
    		buffer.delete(0, inx+NConstants.nextObjectString.length());
    	}
    	ret.add(buffer.toString());
    	return ret;
    }
    
    public static boolean isValidEmail(String email, boolean isNullOK){
        if(email==null || email.length()==0)
            return isNullOK;
        //Rules
        if(email.startsWith("@")) {
            return false;
        } else if(email.indexOf('@')==-1){
            return false;
        } else if(email.indexOf('.')==-1){
            return false;
        } else if(email.indexOf('@')!=email.lastIndexOf('@')){
            return false;
        } else if(email.indexOf('@')>email.lastIndexOf('.')){
            return false;
        } else if(email.lastIndexOf('.')==email.length()-1){
            return false;
        }
        return true;
    }
    
    public static boolean isValidPhone(String phone, boolean isNullOK){
        if(phone==null || phone.length()==0)
            return isNullOK;
        
        String b = convertPhoneToString2(phone);
        for(int i=0; i< b.length();i++){
            char a = b.charAt(i);
            if(a >='0' && a<='9'){
                continue;
            } else {
                return false;
            }
        }
        
        return true;
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
	public static void copyVector(Vector src, Vector dest){
        if(src==null || dest==null) return;
        Enumeration el = src.elements();
        dest.removeAllElements();
        while(el.hasMoreElements()){
            dest.addElement(el.nextElement());
        }
    }
    
    public static boolean isRadioOn(){
        return isRadioOn(false);
    }
    
    @Deprecated
    public static boolean isRadioOn(boolean lowSignal){    
    	return true;
        /*
        int sig = net.rim.device.api.system.RadioInfo.getSignalLevel();
        
        if(lowSignal && sig < -90) 
        {
            return false;
        }
        
         if((net.rim.device.api.system.RadioInfo.getState()!=net.rim.device.api.system.RadioInfo.STATE_ON)               
            ||(net.rim.device.api.system.RadioInfo.getSignalLevel()==net.rim.device.api.system.RadioInfo.LEVEL_NO_COVERAGE))
         {
            return false;    
         } else {
            return true;
        }*/
    }
    
    public static String getUserEmailID(Context context) {
    	//TODO Implement getEmailID properly, currently force account to be google and 0
    	String myEmailid = "";
    	
    	try {
	    	Account[] accounts=AccountManager.get(context).getAccountsByType("com.google"); 
	    	if(accounts!=null && accounts.length!=0){
	    		if(accounts[0]!=null)
	    			myEmailid=accounts[0].name; 
	    	}
	    	
	    	Log.d("My email id that i want", "");
    	} catch (Exception ex) {
    		Log.e("getDeviceInfoAsCGIReady", "getting email");
    	}
    	
    	return myEmailid;
    	
    	/* The Correct way 
    	 * 
    	    Account[] accounts = AccountManager.get(this).getAccounts();
			for (Account account : accounts) {
  				// TODO: Check possibleEmail against an email regex or treat
  				// account.name as an email address only for certain account.type values.
  				String possibleEmail = account.name;
  				
			}
    	 */
    }
    
    public static String getDeviceInfoAsCGIReady(Context context) {
        
    	
    	 
    	String ret = "";
        String emailID = getUserEmailID(context);
        
        ret+="V:"+context.getResources().getString(R.string.version);
        ret+="|DOS:"+Build.VERSION.RELEASE;
        ret+="|E:"+emailID;
        //ret+="|DPL:"+Build.VERSION.CODENAME;//DeviceInfo.getPlatformVersion();
        ret+="|C:"+getCountryISO(context);
        ret+="|DBuildID:"+Build.ID;
        ret+="|DM:"+Build.MANUFACTURER;
        ret+="|DMODEL:"+Build.MODEL;
        return ret; //NUtilities.encodingString(ret);
    } 
    
    public static String getLatitude() {  
        NGPSThread mgt = NStore.getStoreInstance().getGPSServiceThread();        
        return mgt.getLatitudeAsString(false);
    }
    
    
    public static String getLongditute() {
    	NGPSThread mgt = NStore.getStoreInstance().getGPSServiceThread();       
        return mgt.getLongditudeAsString(false);
    }
    
    public static String PrepareHeaderAsUrlEncoded(Vector<String> headers) {
    	String urlString = "";
    	Enumeration<String> elements = headers.elements();
    	while(elements.hasMoreElements()){
    		urlString+=com.nikolay.ar.base.utils.NUtilities.encodingString((String) elements.nextElement() + NConstants.URLSEPARATOR);
    	}    	
        return urlString;
    }
    
    public static String PrepareChartDataAsUrlEncoded(Vector<String> values) {
    	String urlString = "";
    	Enumeration<String> elements = values.elements();
    	while(elements.hasMoreElements()){
    		urlString+=com.nikolay.ar.base.utils.NUtilities.encodingString((String) elements.nextElement() + NConstants.URLSEPARATOR);
    	}    	
         return urlString;
    }
  
    public static String PrepareChartChoiceAsUrlEncoded(Vector<String> values) {
    	String urlString = "";
    	Enumeration<String> elements = values.elements();
    	while(elements.hasMoreElements()){
    		urlString+=com.nikolay.ar.base.utils.NUtilities.encodingString((String) elements.nextElement() + NConstants.URLSEPARATOR);
    	}    	
         return urlString;
    }    

    public static String getCurrentDateAsStringForServerTimeZone() {
    	Calendar cl = Calendar.getInstance(TimeZone.getTimeZone(NConstants.SERVER_TIME_ZONE));
    	int m = cl.get(Calendar.MONTH);
    	m++;
    	String m_str = Integer.toString(m);
    	if(m<10) {
    		m_str="0"+m_str;
    	}
    	
    	int d = cl.get(Calendar.DATE);
    	String d_str = Integer.toString(d);
    	if(d<10) {
    		d_str = "0"+d_str;
    	}
    	
    	String ret = cl.get(Calendar.YEAR) + "/"+m_str + "/" + d_str;
    	return ret;
    }

    public static String getCountryISO(Context context) {
    	String ret = "UN";
    	try {
    		ret = ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE)).getNetworkCountryIso();
    	} catch (Exception ex) {
    		ret = "UN";
    	}
    	
    	return ret;
    }
    
    public static String getPIN_DEVICEID(Context context, boolean enc) {
    	String android_id = Secure.getString(context.getContentResolver(),
                Secure.ANDROID_ID);
		if(!enc) {
			return 	android_id;//((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
	
		} else {
			try {
				String ret =  byteArrayToHexString(computeHash(android_id));
				return ret;
			} catch (Exception e) {
				Log.e("getPIN_DEVICEID", e.toString());
				return android_id;
			}
		}
    }
    /*
    public static String encryptString(String toEnc) {
    	byte[] inputBytes = toEnc.getBytes();
    	byte[] outputBytes;
    	byte[] cypheredBytes = cipher.doFinal(inputString, 0, inputBytes, outputBytes, 0);
    	return new String(outputBytes, 0, cypheredBytes);
    }*/
	
	public static String getUniqueCode(Context context) {
		String uID = "1";
		try {
			uID = ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
			if(uID == null || uID.length()==0) {
				uID = getPIN_DEVICEID(context, false);
			}
		} catch (Exception ex) {
			uID = "1";
		}
		try {
			return byteArrayToHexString(computeHash(uID));
		} catch (Exception e) {
			Log.e("getUniqueCode", e.toString());
			return "1";
		}
	}
	
	public static String getToken(Context context) {
		String pas = "nikolayds";
		pas+=getCurrentDateAsStringForServerTimeZone();
		pas+=getUniqueCode(context);
		try {
			pas = byteArrayToHexString(computeHash(pas));
		} catch (Exception ex) {
			return "";
		}
		return pas;
	}
	
	public static byte[] computeHash(String x)   
	  throws Exception  
	  {
	     java.security.MessageDigest d =null;
	     d = java.security.MessageDigest.getInstance("SHA-1");
	     d.reset();
	     d.update(x.getBytes());
	     return  d.digest();
	  }
	  
	 public static String byteArrayToHexString(byte[] b){
	     StringBuffer sb = new StringBuffer(b.length * 2);
	     for (int i = 0; i < b.length; i++){
	       int v = b[i] & 0xff;
	       if (v < 16) {
	         sb.append('0');
	       }
	       sb.append(Integer.toHexString(v));
	     }
	     return sb.toString().toUpperCase();
	  }
	 
	 /* Returns if Voice recognition is allowed on the 
	  * device. 
	  */
	 public static boolean isVoiceRecognitionPresent(NBaseActivity activity) {
		 PackageManager pm = activity.getPackageManager();
	     List<ResolveInfo> activities = pm.queryIntentActivities(
	                new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH), 0);
	     if (activities.size() != 0) {
	    	 return true;
	     } else {
	    	 return false;
	     }
	}
	 
	 /**
	  * Returns true for amazon and false 
	  * (when Google is default)
	  * @return
	  */
	 public static boolean isAmazonMarket() {
		 //TODO Get the store from Package manager
		 //You can get from where teh package has been installed by 
		 //By the command 
		 //ctx.getPackageManager().getInstallerPackageName ("com.nikolay.arfa");
			
		 return NConstants.IS_AMAZON_MARKET;
	 }
	 
	 /**
	  * This method returns the url to the market from 
	  * where the app has been installed. 
	  * It must be used for differentiation between Google and Amazon for example
	  * @return
	  */
	 public static String getMarketUrl() {
		 //TODO Add more markets
		 if(isAmazonMarket()){
			 return "http://www.amazon.com/gp/mas/dl/android?p=";
		 } else {
			 return "market://details?id=";
		 }
	 
	 }


	public static boolean isValidEmail(String email) {
		Pattern p = Pattern.compile(".+@.+\\.[a-z]+");
		Matcher m = p.matcher(email);
		boolean matchFound = m.matches();
		if(matchFound){
			return true;
		} else {
			return false;
		}
	}
	
	public static String getEmergencyPhoneByCountry(String country) {
		//TODO Add more countries
		if(country==null 
				|| country.length()==0 
				|| country.toUpperCase().startsWith("US")) {
			return "911";
		} else if(country.equalsIgnoreCase("AU")
				|| country.equalsIgnoreCase("AUSTRALIA")) {
			return "000";
		} else if(country.equalsIgnoreCase("IN")
				|| country.equalsIgnoreCase("INDIA")) {
			return "108";
		} else if(country.equalsIgnoreCase("KSA")
				|| country.equalsIgnoreCase("Saudu Arabia")) {
			return "997";
		} else if(country.equalsIgnoreCase("NZ")
				|| country.equalsIgnoreCase("New Zealand")) {
			return "111";
		} else if(country.equalsIgnoreCase("CA")
				|| country.equalsIgnoreCase("CANADA")) {
			return "911";
		} else if(country.equalsIgnoreCase("MY")
				|| country.equalsIgnoreCase("Malaysia")) {
			return "999";
		} else if(country.equalsIgnoreCase("SG")
				|| country.equalsIgnoreCase("Singapore")) {
			return "995";
		} else if(country.equalsIgnoreCase("ID")
				|| country.equalsIgnoreCase("Indonesia")) {
			return "112";
		} else if(country.equalsIgnoreCase("PH")
				|| country.toUpperCase().startsWith("PHILIPPINE")) {
			return "117";
		} else {
			//Universal
			return "112";		
		}
	}
	
	public static boolean isArfaInstalled(Context ctx){
		//TODO Implement is properly
	    //ComponentName comp = new ComponentName("com.nikolay.arfa", "com.nikolay.arfa.activity.ArfaBaseActivity");
	    //Intent intentName = new Intent().setComponent(comp);
	    //List <ResolveInfo> list = ctx.getPackageManager().queryIntentActivities(intentName, PackageManager.MATCH_DEFAULT_ONLY);
	    //return list.size() > 0;
		String p = "";
		try {
			p = ctx.getPackageManager().getInstallerPackageName ("com.nikolay.arfa");
			if(p!=null) {
				p += "google";
			}
		} catch (Exception ex) {
			return false;
		}
		return true;
	}
	
	
	/** Determines whether one Location reading is better than the current Location fix
	  * @param location  The new Location that you want to evaluate
	  * @param currentBestLocation  The current Location fix, to which you want to compare the new one
	  */
	public static boolean isBetterLocation(Location location, Location currentBestLocation) {
	    	
		if(location==null) return false;
			
		if (currentBestLocation == null) {
	        // A new location is always better than no location
			return true;
	    }

	    // Check whether the new location fix is newer or older
	    long timeDelta = location.getTime() - currentBestLocation.getTime();
	    boolean isSignificantlyNewer = timeDelta > NConstants.TWO_MINUTES;
	    boolean isSignificantlyOlder = timeDelta < NConstants.TWO_MINUTES;
	    boolean isNewer = timeDelta > 0;

	    // If it's been more than two minutes since the current location, use the new location
	    // because the user has likely moved
	    if (isSignificantlyNewer) {
	        //return true;
	    // If the new location is more than two minutes older, it must be worse
	    } else if (isSignificantlyOlder) {
	        //return false;
	    }

	    // Check whether the new location fix is more or less accurate
	    int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
	    boolean isLessAccurate = accuracyDelta > 0;
	    boolean isMoreAccurate = accuracyDelta < 0;
	    boolean isSignificantlyLessAccurate = accuracyDelta > 200;

	    // Check if the old and new location are from the same provider
	    boolean isFromSameProvider = isSameProvider(location.getProvider(),
	            currentBestLocation.getProvider());

	    // Determine location quality using a combination of timeliness and accuracy
	    if (isMoreAccurate) {
	        return true;
	    } else if (isNewer && !isLessAccurate) {
	        return true;
	    } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
	        return true;
	    }
	    return false;
	}

	/** Checks whether two providers are the same */
	public static boolean isSameProvider(String provider1, String provider2) {
	    if (provider1 == null) {
	      return provider2 == null;
	    }
	    return provider1.equals(provider2);
	}
	
	public static boolean isLastLocationAccurate(Location location) {
	    	if(location!=null 
	    			&& location.hasAccuracy()) {
	    		
	    		float ac = location.getAccuracy();
	    		return ac <= NConstants.DEFAULT_ACCURACY_RANGE;
	    	} else {
	    		return false;
	    	}
		}

	
	public static String START_DICTATE = "<!-- DICTATE -->";
	public static String END_DICTATE = "<!-- END -->";
	public static String WAIT_DICTATE_START = "<!-- W:";
	//<!-- W:30 U:234_2 -->
	
	public static List<NDicatePragraph> parseDictate(String html_temp) {
		if(html_temp.indexOf(START_DICTATE)==-1) return null;
		ArrayList<NDicatePragraph> ret = new ArrayList<NDicatePragraph>();
		String rest = html_temp;
		int lS = START_DICTATE.length();
		int lE = END_DICTATE.length();
		while(rest.indexOf(START_DICTATE)!=-1) {
			rest = rest.substring(rest.indexOf(START_DICTATE)+lS);
			int end = rest.indexOf(END_DICTATE);
			if(end==-1) break;
			String line = rest.substring(0, end);
			//TODO Get delay from <W> flaga
			String delay = "2";
			String pID = "";
			int iwds = line.indexOf(WAIT_DICTATE_START);
			if(iwds!=-1) {
				delay = line.substring(iwds+WAIT_DICTATE_START.length());
				int dE = delay.indexOf(" ");
				
				if(dE!=-1) {
					pID = delay.substring(dE);
					delay = delay.substring(0, dE);
					if(pID.indexOf("-->")!=-1) {
						pID = pID.substring(0, pID.indexOf("-->"));
						pID = pID.trim();
					} else {
						pID = "";
					}
					
				} else {
					delay = "2";
				}
			}
			int d = 2;
			try {
				d = Integer.parseInt(delay.trim());
			} catch (Exception ex) {
				
			}
			ret.add(new NDicatePragraph(clearTags(line), pID, d));
			rest = rest.substring(end + lE);
		}
		
		
		return ret;
	}


	private static String clearTags(String line) {
		// TODO Auto-generated method stub
		while(line.indexOf('<')!=-1) {
			int e = line.indexOf('>');
			int s = line.indexOf('<');
			if(e!=-1) {
				String a = line.substring(0, s);
				String b = line.substring(e+1);
				//TODO Consider!!!!
				line = a + " "+ b;
			}
		}
		line = line.replace("\n", " ");
		line = line.replace("\r", " ");
		return line;
	}
}
