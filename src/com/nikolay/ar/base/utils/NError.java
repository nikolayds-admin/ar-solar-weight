/*
 * MarsError.java
 *
 * Created on November 24, 2004, 12:32 PM
 */

package com.nikolay.ar.base.utils;

//import java.util.*;
//import java.lang.*;

/**
 *
 * @author  nikolays
 */
public class NError {
    
    public static final int EXCEPTION_TYPE = 0;
    public static final int SERVER_TYPE = 1;
    public static final int CONNECTION_TYPE = 2;
    
    private int errorType = EXCEPTION_TYPE;
    Exception ex = null;
    String text = "";
    boolean isException= false;
    
    /** Creates a new instance of MarsError */
    public NError(String text, int type) {
        this(text, type, null);
    }
    
    public NError(String text, int type, Exception ex) {
        errorType = type;
        this.text = text; 
        if(ex==null) {
            isException = false;
            ex = null;
        } else {
            isException = true;
            this.ex = ex;
        }
    }
    
    public int getType(){
        return errorType;
    }
    
    public String getText(){
        if(text!=null) {
            if(text.startsWith("Error:")){
                text = text.substring(6);
            }
            if( text.length()>2047) {
                text = text.substring(0, 2047);
            }
        }
        return text;
    }
    
    public boolean isException(){
        return this.isException;
    }
    
    public Exception getException(){
        return ex;
    }    
}
