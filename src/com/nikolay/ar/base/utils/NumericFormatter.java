/*
 * NumericFormatter.java
 *
 * Created on November 5, 2004, 2:01 PM
 */

package com.nikolay.ar.base.utils;

//import java.lang.*;

/**
 *
 * @author  nikolays
 */
public class NumericFormatter {
    
    /** Creates a new instance of NumericFormatter */
    public NumericFormatter() {
    }
    
    public static String unFormatNumberFromLocale(String num) {
        String rez = "";
        if(num.startsWith("$")) num = num.substring(1);
        for(int i=0; i<num.length(); i++){
            char a = num.charAt(i);
            if('a'>=0 && a<='9'){
                rez += a;
            }
        }
        
        
        return rez;
    }
    
    public static String formatNumberToLocale(String num, boolean showDollar) {
        return formatNumberToLocale(num, showDollar, false);
    }
    
    public static String formatNumberToLocale(String num, boolean showDollar, boolean showDec) {
        if(num == null || num.length()==0) return num;
        int inx = num.indexOf('.');
        boolean p = false;
        String dec = "";
        String w = num;
        
        boolean addZeroToEnd = false;
        
        
        if(inx!=-1) {
            p = true;
            dec = num.substring(inx+1);
            if(dec.length()==1) dec += "0";
            w = num.substring(0, inx);
        } else {
            addZeroToEnd = showDec;
        }
        
        boolean s = false;
        if(num.charAt(0)=='-'){
            s = true;
            w = w.substring(1);
        }        
        
        String r = "";
        for(int i = w.length()-3;i>=0; i-=3) {
            String t = w.substring(i, i+3);
            r = "," + t + r;            
        }
        
        int n = w.length();
        
        if(n%3 == 0) {
            r = r.substring(1);
        } else {
            r = w.substring(0, n%3) + r;
        }
        
        
        if(s) r="-"+r;
        if(p) r = r + "." + dec;
        
        if(showDollar) r = "$"+r;
        if(addZeroToEnd) r +=".00";
        
        return r;
    }
    
}
