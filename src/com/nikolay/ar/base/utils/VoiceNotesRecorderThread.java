/*
 * VoiceNotesRecorderThread.java
 *
 * � <your company here>, 2003-2005
 * Confidential and proprietary.
 */

package com.nikolay.ar.base.utils;

//import java.io.*;
//import net.rim.device.api.ui.*;
//import net.rim.device.api.ui.component.*;

//import javax.microedition.media.*;
//import javax.microedition.media.control.*;


public class VoiceNotesRecorderThread extends Thread
{
   //private Player _player;
   //private RecordControl _rcontrol;
   //private ByteArrayOutputStream _output;
   private byte _data[];

   public VoiceNotesRecorderThread() {}

/*
   private int getSize()
   {
       return (_output != null ? _output.size() : 0);
   } */

   public byte[] getVoiceNote()
   {
      return _data;
   }

   public void run() {
      try {
    	  /*
          // Create a Player that captures live audio.
          _player = javax.microedition.media.Manager.createPlayer("capture://audio");
          _player.realize();

          // Get the RecordControl, set the record stream,
          _rcontrol = (RecordControl)_player.getControl("RecordControl");

          //Create a ByteArrayOutputStream to capture the audio stream.
          _output = new ByteArrayOutputStream();
          _rcontrol.setRecordStream(_output);
          _rcontrol.startRecord();
          _player.start();
		*/
      } catch (final Exception e) {
      }
   }

   public void stopReal() {
      try {
           //Stop recording, capture data from the OutputStream,
           //close the OutputStream and player.
    	  /*
           _rcontrol.commit();
           _data = _output.toByteArray();
           _output.close();
           _player.close();
           */

      } catch (Exception e) {
      }
   }
}
