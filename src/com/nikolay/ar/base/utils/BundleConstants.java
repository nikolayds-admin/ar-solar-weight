package com.nikolay.ar.base.utils;

public interface BundleConstants {
	//When yo show rate us
	public static int ARTICLE_READ_CONSTANT = 4;
	
	
	public static String REP = "REP";
	public static String SCREENID = "SCREENID";
	public static String SCREEN_NAME = "SCREENNAME";
	public static String SCREEN_TYPE = "SCREENTYPE";
	
	public static final String USERNAME = "USERNAME";
	public static final String PASSWORD = "PASSWORD";
	public static final String URL = "URL";
	public static final String DOMAIN = "DOMAIN";
	public static final String USER_EMAIL="USER_EMAIL";
	public static final String SERVER_URL="SERVER_URL";
	
	//Default Values
	public static final String DEFAULT_SERVER = "http://callyourdoctor.eu:8880/GluServer/BaseServlet";//"http://192.168.2.62:8080/GluServer/BaseServlet";
	public static final String DEFAULT_USER = "GUEST";
	public static final String DEFAULT_PASSWORD = "Parolata@0123";
	public static final String DEFAULT_DOMAIN ="";
	public static final String DEFAULT_USER_EMAIL="guest@callyourdoctor.eu";
	
	//Shared Preferences
	public static final String PREFS_NAME = "NikolayPrefsFile";
	
	//Preferences Profile Constants
	//IF value does not need to go to the server put 'NS_' infront
	
	//public static final String USER_EMAIL="USER_EMAIL";
	public static final String USER_MEASUREMENT_UNITS="MEASUREMENT_UNITS";
	public static final String USER_WEIGHT="USER_WEIGHT";
	public static final String USER_HEGIHT="USER_HEIGHT";
	public static final String USER_SEX="USER_SEX";
	public static final String USER_AGE_GROUP="USER_AGE_GROUP";
	public static final String USER_MD_EMAIL="USER_MD_EMAIL";
	public static final String USER_MD_PHONE="USER_MD_PHONE";
	
	//Emergency
	public static final String HOTLINE_PHONE="HOTLINE_PHONE";
	public static final String POISON_CONTROL_PHONE="POISON_CONTROL_PHONE";
	public static final String EM_PERSON_NAME="EM_PERSON_NAME";
	public static final String EM_PERSON_PHONE="EM_PERSON_PHONE_PHONE";
	
	
	//Risks & Med Cond
	public static final String USER_HBP = "HIGH_BLOOD_PRESUARE";
	public static final String USER_DIABETIS = "DIABETIS";
	public static final String USER_HIGHCHOLESTEROL = "HIGH_CHOLESTEROL";
	public static final String USER_CARDIOVASCULAR = "CARDIO_VASCULAR_D";
	public static final String USER_MYOCARD = "MYOCARD_INFRACTION";
	public static final String USER_STROKE = "STROKE";
	public static final String USER_SMOKE = "SMOKE";
	public static final String USER_OVERWEIGHT = "OVERWEIGHT";
	public static final String USER_ENTERED_PROFILE = "PROFILE_ENTERED";
	
	//Evaluation
	public static final String EVAL_COMPLETED = "EVAL_COMPLETED";
	public static final String EVAL_CERTIFITED = "CERTIFITED_PHONE";
	public static final String EVAL_SLOW = "SLOW_INTERNET";
	
	//SMS Body
	public static final String EM_SMS_BODY = "EM_SMS_BODY";
	
	//Heredeti
	public static final String USER_HEREDITY_DIABETIS = "HEREDITY_DIABETIS";
	public static final String USER_HEREDITY_HBP = "HEREDETI_HBP";
	
	//Pref Constants
	public static final String LASTSYNCDATE = "LASTSYNCDATE";
	public static final String AGREEAT="AGREEAT";
	//End Constants;
	
	//Preferences
	
	public static final String ASKED_FOR_RATING = "NS_ASKED_FOR_RATING";
	public static final String ARTCILE_READ = "NS_ARTCILE_READ";
	public static final String ARTCILE_READ_COUNT = "ARTCILE_READ_COUNT";
	
	//NEW
	public static final String DENTIST_PHONE = "DENTIST_PHONE";
	public static final String PEDIATRITIAN_PHONE = "PED_PHONE";
	public static final String GYNECOLOGIST_PHONE = "GYNECOLOGIST_PHONE";
	public static final String INS_PROVIDER = "INS_PROVIDER";
	public static final String LOCAL_EMS_PHONE = "LOCAL_EMS_PHONE";
	
	public static final String LOCATION_TAGS = "NS_LOCATION_TAGS";
	public static final String IS_SOUTH_HEMISPHERE = "NS_IS_SOUTH_HEMISPHERE";
}
