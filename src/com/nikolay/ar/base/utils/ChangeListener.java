package com.nikolay.ar.base.utils;

public interface ChangeListener {
	public void changed(Object changed, Object value);
}
