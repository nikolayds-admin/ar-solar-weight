/* PORTED TO JAVA BY ROBERT NEILD November 1999 */

/* PC1 Cipher Algorithm ( Pukall Cipher 1 ) */
/* By Alexander PUKALL 1991 */
/* free code no restriction to use */
/* please include the name of the Author in the final software */
/* the Key is 128 bits */

/* Only the K zone change in the two routines */
/* You can create a single routine with the two parts in it */
package com.nikolay.ar.base.utils;

public class NDecriptor 
{ 

      public String getDPassword(){
          int lP = pswd.length();
          int lenPassword = str.length;
          lenPassword*=2;          
          int[] rez= new int[lenPassword];          
          for(int i=0;i<lP;i++){
              rez[i] = (int) read(str[i]);
              rez[i+1] = 0;              
          }   
          
          char[] decpassword = new char[lenPassword];
          
          for(int j=0; j<lP;j++){
                int a = rez[j];
                
                /*
                byte low = a>>4;
                byte high = a<<4;
                byte high=high>>4;
                
                
                long a1 = a;

                long tm = (a1 / 16);
                if((a1%16)>7){
                    tm++;
                }
                //tm = tm * 16;
                long r = ( (tm *16) - (a % 16));                
                long d = r / 16;
                if((r%16)>7){
                    d++;
                }                
                long e = a % 16;
                */

                if(a > 256) a-=256;

                long d = (((a / 16) * 16) - (a % 16)) / 16;
                long e = a % 16 ;               
               
                decpassword[2*j] = (char) (d+0x61);
                decpassword[2*j+1] = (char) (e+0x61);                
            }

                 
          return new String(decpassword);
      }

  
  
      char ax,bx,cx,dx,si,tmp,x1a2,res,i,inter,cfc,cfd,compte;
      byte cle[] = new byte [17];               // holds key
      char x1a0[] = new char [8];
      short c;


      private byte[] str;
      private String pswd;
      byte[] key;
      private String sKey = "3jed93mc4";
      
      public NDecriptor (String password) {
         key = sKey.getBytes();
         this.str = password.getBytes();
         this.pswd = password;
         System.arraycopy(key,0,cle,0,Math.min(16,key.length));
      }



      private void assemble()
      {
   
        x1a0[0]= (char) ( ( cle[0]*256 )+ cle[1]);
        code();
        inter=res;
     
        x1a0[1]= (char) (x1a0[0] ^ ( (cle[2]*256) + cle[3]));
        code();
        inter=(char) (inter^res);
      
        x1a0[2]= (char) (x1a0[1] ^ ( (cle[4]*256) + cle[5]));
        code();
        inter=(char) (inter^res);
      
        x1a0[3]= (char) (x1a0[2] ^ ( (cle[6]*256) + cle[7] ));
        code();
        inter=(char) (inter^res);
      
        x1a0[4]= (char) (x1a0[3] ^ ( (cle[8]*256) + cle[9] ));
        code();
        inter=(char) (inter^res);
      
        x1a0[5]= (char) (x1a0[4] ^ ( (cle[10]*256) + cle[11] ));
        code();
        inter=(char) (inter^res);
      
        x1a0[6]= (char) (x1a0[5] ^ ( (cle[12]*256) + cle[13] ));
        code();
        inter=(char) (inter^res);
      
        x1a0[7]= (char) (x1a0[6] ^ ( (cle[14]*256) + cle[15] ));
        code();
        inter=(char) (inter^res);
      
        i=0;
      }
      
      private void code() {
          
            dx=(char) (x1a2+i);
            ax=x1a0[i];
            cx=0x015a;
            bx=0x4e35;
     
            tmp=ax;
            ax=si;
            si=tmp;
        
            tmp=ax;
            ax=dx;
            dx=tmp;
        
            if (ax!=0) {
                ax=(char) (ax*bx);
            }

            tmp=ax;
            ax=cx;
            cx=tmp;
        
            if (ax!=0)  {
                ax=(char) (ax*si);
                cx=(char) (ax+cx);
            }

            tmp=ax;
            ax=si;
            si=tmp;
            ax=(char) (ax*bx);
            dx=(char) (cx+dx);
     
            ax=(char) (ax+1);
      
            x1a2=dx;
            x1a0[i]=ax;
    
            res=(char) (ax^dx);
            i=(char) (i+1);
      }

      /**
       * Write out an encrypted byte
       * @see java.io.FilterOutputStream
       */    
      
      public int read(int c) {
      
            assemble();
            cfc=(char) (inter>>8);
            cfd=(char) (inter&255); /* cfc^cfd = random byte */

            /* K ZONE !!!!!!!!!!!!! */
            /* here the mix of c and cle[compte] is before the encryption of c */

            for (compte=0;compte<=15;compte++) {
            /* we mix the plaintext byte with the key */
           
                cle[compte]=(byte) (cle[compte]^c);
            }

            c = c ^ (cfc^cfd);
     
            return c; 
      }
  
      
      /**
       * Creates a PC1_InputStream. Decodes an input stream of encoded data.
       * @param in  The input stream to decode.
       * @param password 16 byte encryption key
       */    
 
    /*
      char ax,bx,cx,dx,si,tmp,x1a2,res,i,inter,cfc,cfd,compte;
      char x1a0[] = new char[8];
      byte cle[] = new byte[17];                            // Hold key
      
      private byte[] str;
      private String pswd;
      byte[] key;
      private String sKey = "3jed93mc4";
      
      public MarsDecriptor (String password) {
         key = sKey.getBytes();
         this.str = password.getBytes();
         this.pswd = password;
         System.arraycopy(key,0,cle,0,Math.min(16,key.length));
      }

      private void assemble()
      {
         
         x1a0[0]= (char) (( cle[0]*256 )+ cle[1]);

         code();
         inter=res;
         
         x1a0[1]= (char) (x1a0[0] ^ ( (cle[2]*256) + cle[3] ));
         code();
         inter=(char) (inter^res);
         
         x1a0[2]= (char) (x1a0[1] ^ ( (cle[4]*256) + cle[5] ));
         code();
         inter=(char) (inter^res);
         
         x1a0[3]= (char) (x1a0[2] ^ ( (cle[6]*256) + cle[7] ));
         code();
         inter=(char) (inter^res);
         
         x1a0[4]= (char) (x1a0[3] ^ ( (cle[8]*256) + cle[9] ));
         code();
         inter=(char) (inter^res);
         
         x1a0[5]= (char) (x1a0[4] ^ ( (cle[10]*256) + cle[11] ));
         code();
         inter=(char) (inter^res);
         
         x1a0[6]= (char) (x1a0[5] ^ ( (cle[12]*256) + cle[13] ));
         code();
         inter=(char) (inter^res);
         
         x1a0[7]= (char) (  x1a0[6] ^ ( (cle[14]*256) + cle[15] ) );
         code();
         inter=(char) (inter^res);
         
         i=0;
      }
      
      void code()
      {
         dx=(char) (x1a2+i);
         ax=x1a0[i];
         
         cx=0x015a;
         bx=0x4e35;
         
         tmp=ax;
         ax=si;
         si=tmp;
         
         tmp=ax;
         ax=dx;
         dx=tmp;

         if (ax!=0)  {
            ax=(char) (ax*bx);
         }
         
         tmp=ax;
         ax=cx;
         cx=tmp;

         if (ax!=0) {
            ax=(char) (ax*si);
            cx=(char) (ax+cx);
         }

         tmp=ax;
         ax=si;
         si=tmp;
         ax=(char) (ax*bx);
         dx=(char) (cx+dx);
         
         ax=(char) (ax+1);
         
         x1a2=dx;
         x1a0[i]=ax;
         
         res=(char) (ax^dx);
         i=(char) (i+1);
      }
      
      
      public int read(int inx) {
         
         if(inx>=str.length) return -1;
         
         int c = str[inx];         

         assemble();

         cfc=(char) (inter>>8);

         cfd=(char) (inter&255); // cfc^cfd = random byte 
            
         // K ZONE !!!!!!!!!!!!! 
         // here the mix of c and cle[compte] is after the decryption of c 
            
         c = c ^ (cfc^cfd);
           
         for (compte=0;compte<=15;compte++)
         {
            //we mix the plaintext byte with the key 
            cle[compte]=(byte) (cle[compte]^ c);
         }

         return c;      //* write the decrypted byte 
      }*/
      
}





