package com.nikolay.ar.base.utils;

public class NServerException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2923709947598736511L;
	
	private int type = NError.SERVER_TYPE;
	public NServerException(String message) {
		this(message, NError.SERVER_TYPE);
	}
	public NServerException(String message, int type) {
		super(message);
		this.type = type; 
	}
	
	public int getType(){
		return this.type;
	}
}
