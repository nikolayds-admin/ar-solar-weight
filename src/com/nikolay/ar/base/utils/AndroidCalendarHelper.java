package com.nikolay.ar.base.utils;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;


public class AndroidCalendarHelper {
	
	//on Froyo the contentprovider uri 
	//has changed to "content://com.android.calendar/events"
	
	public static boolean insertMeeting(ContentResolver cr, Object meeting) {		
		
		Cursor cursor = cr.query(Uri.parse("content://calendar/calendars"), new String[]{ "_id", "displayname" }, null, null, null);
		cursor.moveToFirst();
		String[] CalNames = new String[cursor.getCount()];
		int[] CalIds = new int[cursor.getCount()];
		for (int i = 0; i < CalNames.length; i++) {
		    CalIds[i] = cursor.getInt(0);
		    CalNames[i] = cursor.getString(1);
		    cursor.moveToNext();
		}
		cursor.close();
		
		
		ContentValues cv = new ContentValues();
		cv.put("calendar_id", CalIds[0]);
		cv.put("title", "");
		cv.put("dtstart", "");
		cv.put("dtend", "");
		cv.put("eventLocation", "");
		
		Uri newevent = cr.insert(Uri.parse("content://calendar/events"), cv);
		
		String eventid = newevent.getPathSegments().get(newevent.getPathSegments().size()-1);
		
		cv.put("event_id",eventid);
		cv.put("minutes","1");
		
		cr.insert(Uri.parse("content://calendar/reminders"),cv);

		return false;
	}
}
