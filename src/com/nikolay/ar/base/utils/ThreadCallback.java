package com.nikolay.ar.base.utils;

public interface ThreadCallback {
	public void runMethod(int id, int secondID);
}
