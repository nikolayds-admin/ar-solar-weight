/*
 * MarsTheme.java
 *
 * � <your company here>, 2003-2005
 * Confidential and proprietary.
 */
 package com.nikolay.ar.base.utils;
 
import com.nikolay.ar.base.constants.*;


/**
 * 
 */
public class NTheme  {
    
    //Colors Theme //RGB (204, 255, 204);
    private int HEADER_COLOR = 0xFFFFFF;
    private int HEADER_BACK_COLOR = 0x888888;
    private int LIST_BACKGROUND_COLOR = 0xFFFFFF;
    private int LIST_HIGHLIGHT_BACKGROUND_COLOR = 0x00CCFF;
    private int ALT_LIST_BACKGROUND_COLOR = 0xEBDDE2;
    private boolean IS_LABEL_BOLD = true;
    private int HYPERLINK_COLOR = 0x0000FF;     
    private int ALT_REQ_BACK_COLOR = 0x00000000;
    private int REQ_BACK_COLOR = 0x00CCFFCC; 
    private int REQ_BACK_COLOR_HI = 0x0066CC33;
    
    //private int EDIT_LINE_COLOR = 0x00FF0000;
    //private int LABEL_COLOR = 0x00;  
    private int LIST_TYPE_COLOR =  0x0000FF;  
    //private int DIGITS_COLOR = 0x0000FF;
    private int REQUIRE_COLOR = 0x999999;
    
    private int BACK_COLOR_RIGHT_FIELDS = 0xAAAAAA;
      
    
    private String appFontFamily = NConstants.DEFAULT_FONT_NAME;//FontFamily.FAMILY_SYSTEM;
    private int appFontSize = NConstants.getDefaultFontSize();
    //private boolean appBold = false;
    //private boolean appItl = false;
    
    private String listFontFamily = NConstants.DEFAULT_FONT_NAME;//FontFamily.FAMILY_SYSTEM;
    private int listFontSize = NConstants.getDefaultFontSize();
    //private boolean listBold = false;
    //private boolean listItl = false;
    
    private int BB_CALENDAR_IDLE_TIME = 10; //In intervals;
    private int MAX_OPEN_THREADS = 20;
    
    private int MARS_DEAFULT_START_SCREEN = 0;
    
    //Color Theme index
    private int colorThemeIndex = 8;

    private boolean multiFirmView = false;
    private boolean multiRepView = false;
    private boolean multiOffView = false;
    
    private boolean isAccelNeeded = true;
    
    
    private boolean _isShowYesNo = true;
    
    //Events Settings
    public boolean IS_LOCAL_EVENTS = true;
    
    //Instant Meeting
    private boolean isForwardPopUp = false;
    private boolean isForwardDefault = false;
    private String instantMeetingDesc = "Meeting with <REP>";
    private String instantMeetingTaskSubject = "Follow-up with <REP>";
    
    private boolean _isDeleteCompletedMeetings = false;
    
    private boolean _isShowChartsDefault = true;
   
    
    public NTheme() {    
    }
    
    public boolean isDeletedCompletedMeetings() {
    	return this._isDeleteCompletedMeetings;
    }
    
    public void setDeletedCompletedMeetings(boolean val) {
    	this._isDeleteCompletedMeetings = val;
    }
    
    public boolean isShowYesNo() {
        return this._isShowYesNo;
    }
        
    public void setShowYesNo(boolean val) {
        this._isShowYesNo = val;
    }
    
    public boolean isLocalEvents() {
        return IS_LOCAL_EVENTS;
    }
    
    public void setLocalEvents(boolean val) {
        this.IS_LOCAL_EVENTS = val;
    }
    
    public boolean isMultiFirmView() {
        return this.multiFirmView;
    }
    
    public boolean isMultiRepView() {
        return this.multiRepView;
    }

    public boolean isMultiOfficeView() {
        return this.multiOffView;
    }
    
    public void setMultiFirmView(boolean val) {
        this.multiFirmView = val;
    }
    
    public void setMultiRepView(boolean val) {
        this.multiRepView = val;
    }

    public void setMultiOfficeView(boolean val) {
        this.multiOffView = val;
    }
        
    public void setDefaultStartScreen(int scr) {
        MARS_DEAFULT_START_SCREEN = scr;
    }
    
    public int getDefaultStartScreen() {
        return MARS_DEAFULT_START_SCREEN;
    }
    
    public void setMaxThreads(int val) {
        this.MAX_OPEN_THREADS = val;
    }
    
    public int getMaxThreads() {
        return this.MAX_OPEN_THREADS;
    }
    
    public String getFontFamilyStr() {
        return appFontFamily;
    }
    
    public void setFontFamilyStr(String family) {
        appFontFamily = family;
    }
    
    public String getListFontFamilyStr() {
        return listFontFamily;
    }
    
    public void setListFontFamilyStr(String family) {
        listFontFamily = family;
    }
    
    public int getHeaderColor() {
        return this.HEADER_COLOR;
    }

    /* --Commented as per Compiler not Invoke
    public void setHeaderColor(int val) {
        this.HEADER_COLOR = val;
    }*/
    
    public int getHeaderBackGroundColor() {
        return this.HEADER_BACK_COLOR;
    }
    
    public void setBBCalendarIdleTime(int val) {
        this.BB_CALENDAR_IDLE_TIME = val;
    }
    
     public int getBBCalendarIdleTime() {
        return this.BB_CALENDAR_IDLE_TIME;
    }

    /* --Commented as per Compiler not Invoke
    public void setHeaderBackGroundColor(int val) {
        this.HEADER_BACK_COLOR = val;
    }*/
    
    /*
    public Font getAppFont () {
        int style = Font.PLAIN;
        if(appBold) style = Font.BOLD;
        if(appItl) style = style | Font.ITALIC;
        try {
            return FontFamily.forName(appFontFamily).getFont(style, appFontSize);                
        } catch (Exception ex) {
            return Font.getDefault();
        }
    }*/
    
    /* --Commented as per Compiler not Invoke
    public void setAppFont (String name, int size, boolean isBold, boolean isItl) {
        appBold = isBold;
        appItl = isItl;
        appFontSize = size;
        appFontFamily = name;
    }*/
    
    public int getBackColorRightFields() {
        return this.BACK_COLOR_RIGHT_FIELDS;
    }

    /* --Commented as per Compiler not Invoke
    public void setBackColorRightFields(int val) {
        this.BACK_COLOR_RIGHT_FIELDS = val;
    } */
    
    
    /*
    public Font getListFont () {
        int style = Font.PLAIN;
        if(listBold) style = Font.BOLD;
        if(listItl) style = style | Font.ITALIC;
        try {
            return FontFamily.forName(listFontFamily).getFont(style, listFontSize);  
        } catch (Exception ex) {
            return Font.getDefault();
        }
              
    }*/
    
    public String getListFontSize() {
        return Integer.toString(listFontSize);    
    }
    
    public String getAppFontSize() {
        return Integer.toString(appFontSize);
    }
    
    public void setListFontSize(String val) {
        try {
            listFontSize = Integer.parseInt(val);
        } catch(Exception ex) {
        }
    }
    
    public void setAppFontSize(String val) {
        try {
            appFontSize = Integer.parseInt(val);
        } catch(Exception ex) {
        }
    }

    
    /* --Commented as per Compiler not Invoke
    public void setListFont (String name, int size, boolean isBold, boolean isItl) {
        listBold = isBold;
        listItl = isItl;
        listFontSize = size;
        listFontFamily = name;
    } */
    

    public int getListHighBackColor() {
        return this.LIST_HIGHLIGHT_BACKGROUND_COLOR;
    }
    
    public void setListHighBackColor(int value) {
        this.LIST_HIGHLIGHT_BACKGROUND_COLOR = value;
    }
    
    public int getListBackColor() {
        return this.LIST_BACKGROUND_COLOR;
    }
    
    public void setListBackColor(int value) {
        this.LIST_BACKGROUND_COLOR = value;
    }
    
    public int getAltListBackColor() {
        return this.ALT_LIST_BACKGROUND_COLOR;
    }
    
    public void setAltListBackColor(int value) {
        this.ALT_LIST_BACKGROUND_COLOR = value;
    }
    
    public int getHyperlinkColor() {
        return this.HYPERLINK_COLOR;
    }
    
    /* --Commented as per Compiler not Invoke
    public void setHyperlinkColor(int value) {
        this.HYPERLINK_COLOR = value;
    }*/
    
    public int getAltRequireBackColor() {
        return this.ALT_REQ_BACK_COLOR;
    }
    
    /* --Commented as per Compiler not Invoke
    public void setAltRequireBackColor(int value) {
        this.ALT_REQ_BACK_COLOR = value;
    }*/
    
    /* --Commented as per Compiler not Invoke
    public int getEditLineColor() {
        return this.EDIT_LINE_COLOR;
    } */
    
    /* --Commented as per Compiler not Invoke
    public void setEditLineColor(int value) {
        this.EDIT_LINE_COLOR = value;
    }*/
    
    public int getRequireBackColor() {
    	if(NConstants.isHighResolution()) {
    		return this.REQ_BACK_COLOR_HI;
    	} else {
    		return this.REQ_BACK_COLOR;
    	}
    }

    /* --Commented as per Compiler not Invoke    
    public void setRequireBackColor(int value) {
        this.REQ_BACK_COLOR = value;
    } */
    
    /* --Commented as per Compiler not Invoke
    public int getLabelColor() {
        return this.LABEL_COLOR;
    } */
    
    /* --Commented as per Compiler not Invoke
    public void setLabelColor(int value) {
        this.LABEL_COLOR = value;
    } */
    
    public int getListTypeColor() {
        return this.LIST_TYPE_COLOR;
    }
    
    /* --Commented as per Compiler not Invoke
    public void setListTypeColor(int value) {
        this.LIST_TYPE_COLOR = value;
    } */
    
    public boolean isLabelBold() {
        return this.IS_LABEL_BOLD;
    } 
    
    /* --Commented as per Compiler not Invoke
    public void setLabelBold(boolean val) {
        this.IS_LABEL_BOLD = val;
    } */
    
    /* --Commented as per Compiler not Invoke
    public int getDigitColor() {
        return this.DIGITS_COLOR;
    } */

    /* --Commented as per Compiler not Invoke    
    public void setDigitsColor(int value) {
        this.DIGITS_COLOR = value;
    } */
   
    public int getRequireColor() {
        return this.REQUIRE_COLOR;
    }
    
    /* --Commented as per Compiler not Invoke
    public void setRequireColor(int value) {
        this.REQUIRE_COLOR = value;
    }*/
    
    public int getColorThemeIndex() {
        return this.colorThemeIndex;
    }
   
    public void setColorTheme(NColorTheme mct, int inx) {
        this.colorThemeIndex = inx;
        this.setAltListBackColor(mct.ALT_LIST_BACKGROUND_COLOR);
        this.setListBackColor(mct.LIST_BACKGROUND_COLOR);
        this.setListHighBackColor(mct.LIST_HIGHLIGHT_BACKGROUND_COLOR);
    }
    
    public boolean isSlowDeviceFixNeeded() {
        if(NConstants.isFastDevice()) return false;
        return this.isAccelNeeded;
    }
    
    private String defaultMeetingCode = "";
    
    public String getDefaultInstantMeetingCode() {
        return this.defaultMeetingCode;
    }
    
    public void setDefaultInstantMeetingCode(String val) {
        this.defaultMeetingCode = val;
    }
    
  
    public boolean isForwardDefaultOn() {
        return isForwardDefault;
    }
    
    public void setForwardDefaultOn(boolean val) {
        this.isForwardDefault = val;
    }
    
    
   
    
    public boolean isForwardPopUpOn() {
        return isForwardPopUp;
    }
    
    public void setForwardPopUpOn(boolean val) {
        this.isForwardPopUp = val;
    }
    
    public String getInstantMeetingDesc() {
        return instantMeetingDesc;
    }
    
    public String getInstantMeetingTaskSubject() {
        return instantMeetingTaskSubject;
    }
    
    public String getInstantMeetingDesc(String repName, String firmName, String userName) {
        String ret = instantMeetingDesc;
        ret = NUtilities.replaceStr(ret, "<REP>", repName);
        ret = NUtilities.replaceStr(ret, "<FIRM>", firmName);
        ret = NUtilities.replaceStr(ret, "<USER>", userName);        
        return ret;
    }
    
    public String getInstantMeetingTaskSubject(String repName, String firmName, String userName) {
        String ret = instantMeetingTaskSubject;
        ret = NUtilities.replaceStr(ret, "<REP>", repName);
        ret = NUtilities.replaceStr(ret, "<FIRM>", firmName);
        ret = NUtilities.replaceStr(ret, "<USER>", userName);        
        return ret;
    }
    
    public void setInstantMeetingDesc(String val) {
        this.instantMeetingDesc = val;
    }
    
    public void setInstantMeetingTaskSubject(String val) {
        this.instantMeetingTaskSubject = val;
    }    
    
    public boolean isShowChartsDefault() {
    	return _isShowChartsDefault;
    }
    
    public void setShowChartsDefault(boolean val) {
    	this._isShowChartsDefault = val;
    }
} 
