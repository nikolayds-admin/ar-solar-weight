/*
 * MarsDateFormat.java
 *
 * Created on November 18, 2004, 12:15 PM
 */

package com.nikolay.ar.base.utils;

import java.text.SimpleDateFormat;
import java.util.*;


/**
 *
 * @author  nikolays
 */
public class NDateFormat {
    
    public static Date currentDateAsDate(){
        Calendar cal =  Calendar.getInstance(TimeZone.getDefault());
        return cal.getTime();            
    }
    
    public static Date currentDateAsDate1200AM(){
        Calendar cal =  Calendar.getInstance(TimeZone.getDefault());
        //int d = cal.get(Calendar.DATE);
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.AM_PM, Calendar.AM); 
        //Date dt = cal.getTime();
        //cal.set(Calendar.DATE, d);
        return cal.getTime();            
    }
    
    public static Date currentDateAsDateToTheHour(){
        Calendar cal =  Calendar.getInstance(TimeZone.getDefault());       
        cal.set(Calendar.MINUTE, 0);
        //cal.set(Calendar.AM_PM, Calendar.AM); 
        return cal.getTime();            
    }
    
        
    /* --Commented as per Compiler not Invoke
    public static String currentTimeAsString(){
        Calendar cal =  Calendar.getInstance(TimeZone.getDefault());
        Date ret = cal.getTime();
        return ret.toString();
    }*/
    
    public static String currentTimeAsString2(){
        Calendar cal =  Calendar.getInstance(TimeZone.getDefault());
        Date ret = cal.getTime();        
        return dateAsString2(ret);
    }
    
     public static String currentTimeAsStringShort2(){
        Date dt = currentDateAsDate();
        SimpleDateFormat df = new SimpleDateFormat("MM/dd hh:mm:ss");        
        return df.format(dt.getTime());//formatLocal(dt.getTime());
    }   
    
    public static String dateAsString2(Date dt){
        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss aa");        
        StringBuffer str = new StringBuffer();
        Calendar myCal = Calendar.getInstance();
        myCal.setTime(dt);
        String ret = df.format(myCal, str, null).toString();
        return ret;
    }    
    
    public static String dateAsString2(long dt){        
        return dateAsString2(new Date(dt));
    }     
    
    public static String dateAsString2(String dt){
        return dateAsString2(parseDBDate(dt));
    }   
    
    public static String justDateAsString(Date dt){
        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");        
        StringBuffer str = new StringBuffer();
        Calendar myCal = Calendar.getInstance();
        myCal.setTime(dt);
        String ret = df.format(myCal, str, null).toString();
        return ret;
    }          
    
    public static String justDateAsStringDB(String date) {
        return justDateAsString(parseDBDate(date));
    }
    
    public static Date parseDBDate(String date){
        
        
        //Temp solution 
        // assuming date is in mm/dd/yyyy hh:MM:s.sss AM/PM format
        Calendar cal =  Calendar.getInstance(TimeZone.getDefault());
        Date ret = cal.getTime();
        String d = date;
        //int hbase = 0;
        int inx = d.indexOf("PM");        
        if(inx!=-1) {
            cal.set(Calendar.AM_PM, Calendar.PM);
        } else {
            cal.set(Calendar.AM_PM, Calendar.AM);
        }        
        
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        
        inx = d.indexOf("/");        
        if(inx==-1) return ret;        
        String month = d.substring(0, inx);
        try {
            int v = Integer.parseInt(month, 10);
            v--;
            cal.set(Calendar.MONTH, v);
        } catch (Exception ex) {            
        }
        
        d = d.substring(inx+1);        
        inx = d.indexOf("/");
        if(inx==-1) return ret;
        String day = d.substring(0, inx);
        d = d.substring(inx+1);
        try {
            int v = Integer.parseInt(day, 10);            
            cal.set(Calendar.DAY_OF_MONTH, v);
        } catch (Exception ex) {            
        }  
              
        ret =  cal.getTime();
        inx = d.indexOf(" ");
        if(inx==-1 && d.length()==0) return ret;
        
        String year = d;
        if(inx!=-1) {
        	year = d.substring(0, inx);        
        	d = d.substring(inx+1);
        }
        
        try {
            int v = Integer.parseInt(year, 10);            
            cal.set(Calendar.YEAR, v);
        } catch (Exception ex) {            
        } 
        ret =  cal.getTime();       
        inx = d.indexOf(":");        
        if(inx==-1) return ret;
        String hour = d.substring(0, inx);
        d = d.substring(inx+1);
        try {
            int v = Integer.parseInt(hour, 10);            
            //temp fix
            if(v==12) v=0;
            //end temp fix
            cal.set(Calendar.HOUR, v);
        } catch (Exception ex) {            
        }         
        
        inx = d.indexOf(":");
        if(inx==-1) inx = d.indexOf(" ");
        String min = d;
        if(inx!=-1) {
            min = d.substring(0, inx);            
            d = d.substring(inx+1);
        }
        
        try {
            int v = Integer.parseInt(min, 10);            
            cal.set(Calendar.MINUTE, v);
        } catch (Exception ex) {            
        }    
        
        ret =  cal.getTime();   
        inx = d.indexOf(".");  
        if(inx==-1) return ret;        
        String sec = d.substring(0, inx);
        d = d.substring(inx+1);
        try {
            int v = Integer.parseInt(sec, 10);            
            cal.set(Calendar.SECOND, v);
        } catch (Exception ex) {            
        }        
        return cal.getTime();//.getTimeInMillis();        
    }    
}
