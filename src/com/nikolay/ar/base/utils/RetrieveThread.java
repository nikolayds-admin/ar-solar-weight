package com.nikolay.ar.base.utils;

public class RetrieveThread extends Thread {
	
	ThreadCallback callback = null;
	int id = 0;
	int secondID = 0;
	
	public RetrieveThread(ThreadCallback callBack) {
		this.callback = callBack;
	}
	
	public void setID(int id) {
		this.id = id;
	}
	
	public void setSecondID(int id) {
		this.secondID = id;
	}
	
	public void run() {
		callback.runMethod(id, secondID);
	}
}
