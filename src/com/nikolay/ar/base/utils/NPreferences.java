package com.nikolay.ar.base.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.nikolay.ar.base.activities.NBaseActivity;
import com.nikolay.solar.R;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;

public class NPreferences implements com.nikolay.ar.base.utils.BundleConstants {

	private NBaseActivity mActivity;
	
	public NPreferences(NBaseActivity nBaseActivity) {
		mActivity = nBaseActivity;
	}
	
	
	
	public void initPreferences() {	
		SharedPreferences settings = mActivity.getSharedPreferences(PREFS_NAME, 0);
		if(!settings.contains("URL")) {
		    SharedPreferences.Editor editor = settings.edit();
		    editor.putString("URL", DEFAULT_SERVER);
		    editor.commit();
		}	  
		    
		if(!settings.contains("USERNAME")) {
		    SharedPreferences.Editor editor = settings.edit();
		    editor.putString("USERNAME", DEFAULT_USER);
		    editor.commit();
		}		

		//TODO Implement properly
		if(!settings.contains("PASSWORD")) {
			SharedPreferences.Editor editor = settings.edit();
			editor.putString("PASSWORD", DEFAULT_PASSWORD);
			editor.commit();
		}
	
	    if(!settings.contains(LASTSYNCDATE)) {
		    SharedPreferences.Editor editor = settings.edit();
		    editor.putString(LASTSYNCDATE, "");
		    editor.commit();
	    }	
	}
	
	public List<NameValuePair> getPreferencesToStoreToServer() {
		List<NameValuePair> ret = new ArrayList<NameValuePair>();
		SharedPreferences settings = mActivity.getSharedPreferences(PREFS_NAME, 0);
		Set<String> keys = settings.getAll().keySet();
		Iterator<String> iKeys = keys.iterator(); 
		while(iKeys.hasNext()) {
			String key = iKeys.next();
			if(key.equalsIgnoreCase("URL") 
					|| key.equalsIgnoreCase("PASSWORD") 
					|| key.startsWith("NS_") ) continue;
			
			String value = "";
			boolean found = false;
			try {
				value = settings.getString(key, "N/A");
				found = true;
			} catch (Exception ex) {
				found = false;
			}
			
			if(!found) {
				try {
					value = Integer.toString(settings.getInt(key, 0));
					found = true;
				} catch (Exception ex) {
					found = false;
				}
			}
			
			if(found) {
				ret.add(new BasicNameValuePair("PROF_"+key, value));
			}
		}
		
		return ret;
	}
	

	//Preferences
	public String getPrefURL() {
		SharedPreferences settings = mActivity.getSharedPreferences(PREFS_NAME, 0);
		String server = settings.getString(SERVER_URL, DEFAULT_SERVER);
		return server;
	}
	
	public String getUserID(){
		SharedPreferences settings = mActivity.getSharedPreferences(PREFS_NAME, 0);
		return settings.getString(USERNAME, DEFAULT_USER);
	}
	
	public String getPassword(){
		SharedPreferences settings = mActivity.getSharedPreferences(PREFS_NAME, 0);
		return settings.getString(PASSWORD, DEFAULT_PASSWORD);
	}
	
	public String getDomain() {
		SharedPreferences settings = mActivity.getSharedPreferences(PREFS_NAME, 0);
	    return settings.getString(DOMAIN, DEFAULT_DOMAIN);	
	}
	
	public String getUserEmail() {
		SharedPreferences settings = mActivity.getSharedPreferences(PREFS_NAME, 0);
		String emailId = settings.getString(USER_EMAIL, DEFAULT_USER_EMAIL);
		if(emailId.equalsIgnoreCase(DEFAULT_USER_EMAIL)) {
			emailId = NUtilities.getUserEmailID(mActivity);
		}
		return emailId;
	}
	
	//Conditions
	public boolean isDiabeties() {
		SharedPreferences settings = mActivity.getSharedPreferences(PREFS_NAME, 0);
		if(settings.getInt(USER_DIABETIS, 0)==1)  return true;
		return false;
	}

	public boolean isHBP() {
		SharedPreferences settings = mActivity.getSharedPreferences(PREFS_NAME, 0);
    	if(settings.getInt(USER_HBP, 0)==1)  return true;
		return false;
	}

	public boolean isHighCholesterol() {
		SharedPreferences settings = mActivity.getSharedPreferences(PREFS_NAME, 0);
		if(settings.getInt(USER_HIGHCHOLESTEROL, 0)==1)  return true;
		return false;
	}

	public boolean isCardioVascular() {
		SharedPreferences settings = mActivity.getSharedPreferences(PREFS_NAME, 0);
		if(settings.getInt(USER_CARDIOVASCULAR, 0)==1)  return true;
		return false;
	}

	public boolean isMyocardialInfaction() {
		SharedPreferences settings = mActivity.getSharedPreferences(PREFS_NAME, 0);
	 	if(settings.getInt(USER_MYOCARD, 0)==1)  return true;
		return false;
	}
	

	public boolean isStroke() {
		SharedPreferences settings = mActivity.getSharedPreferences(PREFS_NAME, 0);
		if(settings.getInt(USER_STROKE, 0)==1)  return true;
		return false;
	}
	
	public boolean isSmoke() {
		SharedPreferences settings = mActivity.getSharedPreferences(PREFS_NAME, 0);
		if(settings.getInt(USER_SMOKE, 0)==1)  return true;
		return false;
	}

	public boolean isOverweight() {
		SharedPreferences settings = mActivity.getSharedPreferences(PREFS_NAME, 0);
		if(settings.getInt(USER_OVERWEIGHT, 0)==1)  return true;
		return false;
	}

	public boolean isMale() {
		SharedPreferences settings = mActivity.getSharedPreferences(PREFS_NAME, 0);
  	if(settings.getInt(USER_SEX, 2)==1)  return true;
		return false;
	}
	public boolean isFemale() {
		SharedPreferences settings = mActivity.getSharedPreferences(PREFS_NAME, 0);
	  	if(settings.getInt(USER_SEX, 2)==0)  return true;
		return false;
	}

	public boolean isElderly() {
		SharedPreferences settings = mActivity.getSharedPreferences(PREFS_NAME, 0);
	  	if(settings.getInt(USER_AGE_GROUP, 1)==2)  return true;
		return false;
	}
	
	public boolean isKid() {
		SharedPreferences settings = mActivity.getSharedPreferences(PREFS_NAME, 0);
    	if(settings.getInt(USER_AGE_GROUP, 1)==0)  return true;
		return false;
	}

	public boolean isHeredityDiabetes() {
		SharedPreferences settings = mActivity.getSharedPreferences(PREFS_NAME, 0);
		if(settings.getInt(USER_HEREDITY_DIABETIS, 1)==0)  return true;
		return false;
	}
	
	public boolean isHeredityHbp() {
		SharedPreferences settings = mActivity.getSharedPreferences(PREFS_NAME, 0);
		if(settings.getInt(USER_HEREDITY_HBP, 1)==0)  return true;
		return false;
	}
	
	public boolean isValidProfile() {
		SharedPreferences settings = mActivity.getSharedPreferences(PREFS_NAME, 0);
		String a = USER_ENTERED_PROFILE;
		boolean b = settings.getBoolean(a, false);
		//b = settings.getBoolean("PROFILE_ENTERED", false);
		if(b==true) {
			return true;
		} else {
			return false;
		}
	}
	
	public void setEvaluationCompleted(boolean b) {
		SharedPreferences settings = mActivity.getSharedPreferences(PREFS_NAME, 0);
	    SharedPreferences.Editor editor = settings.edit();
		editor.putBoolean(EVAL_COMPLETED, b);
	    editor.commit();
	}


	public void setCertifitedPhone(boolean b) {
		SharedPreferences settings = mActivity.getSharedPreferences(PREFS_NAME, 0);
	    SharedPreferences.Editor editor = settings.edit();
		editor.putBoolean(EVAL_CERTIFITED, b);
	    editor.commit();
	}


	public void setSlowInternet(boolean b) {
		SharedPreferences settings = mActivity.getSharedPreferences(PREFS_NAME, 0);
	    SharedPreferences.Editor editor = settings.edit();
		editor.putBoolean(EVAL_SLOW, b);
	    editor.commit();
	}


	public boolean isEvaluationCompleted() {
		SharedPreferences settings = mActivity.getSharedPreferences(PREFS_NAME, 0);
		String a = EVAL_COMPLETED;
		boolean b = settings.getBoolean(a, false);
		if(b==true) {
			return true;
		} else {
			return false;
		}
	}


	public boolean isCertifitedPhone() {
		SharedPreferences settings = mActivity.getSharedPreferences(PREFS_NAME, 0);
		String a = EVAL_CERTIFITED;
		boolean b = settings.getBoolean(a, false);
		if(b==true) {
			return true;
			
		} else {
			return false;
		}
	}


	public boolean isSlowInternet() {
		SharedPreferences settings = mActivity.getSharedPreferences(PREFS_NAME, 0);
		String a = EVAL_SLOW;
		boolean b = settings.getBoolean(a, false);
		if(b==true) {
			return true;
		} else {
			return false;
		}
	}


	public String getEmergencySMSBody() {
		SharedPreferences pref = mActivity.getSharedPreferences(PREFS_NAME, 0);
		String smsBody =  pref.getString(EM_SMS_BODY, mActivity.getString(R.string.default_sms_body));
		return smsBody;
	}
	
	public void setCertifitedPhone(String smsBody) {
		SharedPreferences settings = mActivity.getSharedPreferences(PREFS_NAME, 0);
	    SharedPreferences.Editor editor = settings.edit();
		editor.putString(EM_SMS_BODY, smsBody);
	    editor.commit();
	}
	
	public String getHotline() {
		SharedPreferences pref = mActivity.getSharedPreferences(PREFS_NAME, 0);
		String phone =  pref.getString(HOTLINE_PHONE, "");
		if(phone == null || phone.length()==0) {
			new AlertDialog.Builder(mActivity)
			.setCancelable(false)
	        .setIcon(android.R.drawable.ic_dialog_info)
	        .setTitle(mActivity.getString(R.string.update_profile))
	        .setMessage(mActivity.getString(R.string.hotline_phone_missing))
	        .setPositiveButton(R.string.OK, new DialogInterface.OnClickListener() {
	            @Override
	            public void onClick(DialogInterface dialog, int which) {
	        	}
	        })
	        .show();
		}
		
		return phone;
	}

	public void setEmergencyContactPhone(String phone) {
		SharedPreferences settings = mActivity.getSharedPreferences(PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(EM_PERSON_PHONE, phone);
		editor.commit();
	}
	
	public String getEmergencyContactPhone() {
		return getEmergencyContactPhone(true);
	}

	public String getEmergencyContactPhone(boolean isShow) {
		SharedPreferences pref = mActivity.getSharedPreferences(PREFS_NAME, 0);
		String phone =  pref.getString(EM_PERSON_PHONE, "");
		if(isShow) {
			if(phone == null || phone.length()==0) {
				new AlertDialog.Builder(mActivity)
		        .setIcon(android.R.drawable.ic_dialog_info)
		        .setTitle(mActivity.getString(R.string.update_profile))
		        .setMessage(mActivity.getString(R.string.person_phone_missing))
		        .setPositiveButton(R.string.OK, new DialogInterface.OnClickListener() {
		            @Override
		            public void onClick(DialogInterface dialog, int which) {
		        	}
		        })
		        .show();
			}
		}
		return phone;
	}
	
	public void setPosionControl(String phone) {
		SharedPreferences settings =  mActivity.getSharedPreferences(PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(POISON_CONTROL_PHONE, phone);
		editor.commit();
	}
	

	public String getPoisonControlPhone() {
		return getPoisonControlPhone(true);
	}

	public String getPoisonControlPhone(boolean isShow) {
		SharedPreferences pref = mActivity.getSharedPreferences(PREFS_NAME, 0);
		String phone =  pref.getString(POISON_CONTROL_PHONE, mActivity.getString(R.string.default_poison_control));
		if(isShow) 
			if(phone == null || phone.length()==0) {
				new AlertDialog.Builder(mActivity)
		        .setIcon(android.R.drawable.ic_dialog_info)
		        .setTitle(mActivity.getString(R.string.update_profile))
		        .setMessage(mActivity.getString(R.string.poison_control_phone))
		        .setPositiveButton(R.string.OK, new DialogInterface.OnClickListener() {
		            @Override
		            public void onClick(DialogInterface dialog, int which) {
		        	}
		        })
		        .show();
			}
		
		return phone;
	}
	
	public String getAgreeDate() {	
		SharedPreferences pref = mActivity.getSharedPreferences(PREFS_NAME, 0);
		return pref.getString(AGREEAT, "");
	}
	
	public void setLastSyncDate(String date) {
		SharedPreferences settings = mActivity.getSharedPreferences(PREFS_NAME, 0);
	    SharedPreferences.Editor editor = settings.edit();
		editor.putString(LASTSYNCDATE, date);
		// Commit the edits!
	    editor.commit();		
	}
	
	public String getLastSyncDateAsServerTZ() {	
		SharedPreferences pref = mActivity.getSharedPreferences(PREFS_NAME, 0);
		return pref.getString(LASTSYNCDATE, "");
	}
	
	public String getMDEmail() {	
		SharedPreferences pref = mActivity.getSharedPreferences(PREFS_NAME, 0);
		String phone =  pref.getString(USER_MD_EMAIL, "");
		if(phone == null || phone.length()==0) {
			new AlertDialog.Builder(mActivity)
	        .setIcon(android.R.drawable.ic_dialog_info)
	        .setTitle(mActivity.getString(R.string.update_profile))
	        .setMessage(mActivity.getString(R.string.enter_md_info))
	        .setPositiveButton(R.string.OK, new DialogInterface.OnClickListener() {
	            @Override
	            public void onClick(DialogInterface dialog, int which) {
	        	}
	        })
	        .show();
		}
		
		return phone;
	}

	public String getMDPhone() {	
		SharedPreferences pref = mActivity.getSharedPreferences(PREFS_NAME, 0);
		String phone =  pref.getString(USER_MD_PHONE, "");
		if(phone == null || phone.length()==0) {
			new AlertDialog.Builder(mActivity)
	        .setIcon(android.R.drawable.ic_dialog_info)
	        .setTitle(mActivity.getString(R.string.update_profile))
	        .setMessage(mActivity.getString(R.string.enter_md_info))
	        .setPositiveButton(R.string.OK, new DialogInterface.OnClickListener() {
	            @Override
	            public void onClick(DialogInterface dialog, int which) {
	        	}
	        })
	        .show();
		}
		
		return phone;
	}

	public void setAskedForRating(boolean b) {
		SharedPreferences settings = mActivity.getSharedPreferences(PREFS_NAME, 0);
	    SharedPreferences.Editor editor = settings.edit();
		editor.putBoolean(ASKED_FOR_RATING, b);
	    editor.commit();
	}


	public boolean isAskedForRating() {
		SharedPreferences settings = mActivity.getSharedPreferences(PREFS_NAME, 0);
		return settings.getBoolean(ASKED_FOR_RATING, false);
		//return false;
	}
	
	public void setArtivleEverRead(boolean b) {
		SharedPreferences settings = mActivity.getSharedPreferences(PREFS_NAME, 0);
		int count = settings.getInt(ARTCILE_READ_COUNT, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putBoolean(ARTCILE_READ, b);
		count++;
		editor.putInt(ARTCILE_READ_COUNT, count);
	    editor.commit();
	}


	public boolean isArticleEverRead() {
		SharedPreferences settings = mActivity.getSharedPreferences(PREFS_NAME, 0);
		boolean ret = settings.getBoolean(ARTCILE_READ, false);
		if(ret) {
			int count = settings.getInt(ARTCILE_READ_COUNT, 0);
			if(count>=ARTICLE_READ_CONSTANT) {
				return true;
			} else {
				return false;
			}
		} else {
			return false; 
		}
	}



	public String getLocalEMSPhone() {
		// TODO Auto-generated method stub
		SharedPreferences settings = mActivity.getSharedPreferences(PREFS_NAME, 0);
		return settings.getString(LOCAL_EMS_PHONE, NUtilities.getEmergencyPhoneByCountry(mActivity.getCountry()));
		
	}

	public void setLocalEMSPhone(String phone) {
		SharedPreferences settings = mActivity.getSharedPreferences(PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(LOCAL_EMS_PHONE, phone);
		editor.commit();
	}


	public String getLocationTags() {
		// TODO Auto-generated method stub
		SharedPreferences settings = mActivity.getSharedPreferences(PREFS_NAME, 0);
		return settings.getString(LOCATION_TAGS,"");
	}
	
	public void setLocationTags(String tags) {
		SharedPreferences settings = mActivity.getSharedPreferences(PREFS_NAME, 0);
	    SharedPreferences.Editor editor = settings.edit();
		editor.putString(LOCATION_TAGS, tags);
		// Commit the edits!
	    editor.commit();	
	}



	public boolean isSouthHemi() {
		SharedPreferences settings = mActivity.getSharedPreferences(PREFS_NAME, 0);
		return settings.getBoolean(IS_SOUTH_HEMISPHERE, false);
	}
	
	public void setSouthHemispheare(boolean b) {
		SharedPreferences settings = mActivity.getSharedPreferences(PREFS_NAME, 0);
	    SharedPreferences.Editor editor = settings.edit();
		editor.putBoolean(IS_SOUTH_HEMISPHERE, b);
	    editor.commit();
	}
}

