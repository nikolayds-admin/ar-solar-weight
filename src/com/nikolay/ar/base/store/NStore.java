package com.nikolay.ar.base.store;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import android.content.Context;
import android.util.Log;

import com.nikolay.ar.base.activities.NBaseActivity;
import com.nikolay.ar.base.services.SaveElement;
import com.nikolay.ar.base.services.SaveService;
import com.nikolay.ar.base.trans.ArUser;
import com.nikolay.ar.base.trans.ArUserInfo;
import com.nikolay.ar.base.trans.NCode;
import com.nikolay.ar.base.trans.NState;
import com.nikolay.ar.base.utils.NDateFormat;
import com.nikolay.ar.base.utils.NGPSThread;
import com.nikolay.ar.base.utils.NTheme;



public class NStore {
		
	public static int MAX_LOG_SIZE = 5000;
	public static int STEP_TO_REMOVE_LOG = 1000;
	private static NStore CURRENT_STORE=null;
		
	private static Vector<String> loc_events = new Vector<String>();
	private static ArUser _user = null;
	@SuppressWarnings({ "rawtypes" })
	private static Vector<Vector> staticData = null;
	private static Vector<SaveElement> save_query = new Vector<SaveElement>();
		
	private static NTheme _tempTheme = null;
	private static Vector<Object> _calList;
	
	private NStore() {
		//CURRENT_STORE = this;
	}
	
	//Services
	private NGPSThread mgt; 
	SaveService ss;
	
	/*
	 * 
	 */
	public void initGPSService(Context mContext, int minTime, int distance, int accuracy) {
		mgt = NGPSThread.createMGT(mContext, minTime, distance, accuracy);
	}
	
	public NGPSThread getGPSServiceThread(){
		return mgt;
	}
		
	public static NStore getStoreInstance() {
		if(CURRENT_STORE==null)
			CURRENT_STORE = new NStore();
		return CURRENT_STORE;
	}
	
	 
	public Vector<String> getEvents() {
		//TODO Please Implement the method
		return loc_events;
	}

	public void setEvents(Vector<String> events) {
		//TODO Please Implement the method
		loc_events = events;
	}

	

	@SuppressWarnings("rawtypes")
	public Vector<Vector> getStaticData() {
		//TODO Please Implement the method
		return staticData;
	}

	public NTheme getMarsTheme() {
		//TODO Please Implement the method
		if(_tempTheme ==null) 
			_tempTheme = new com.nikolay.ar.base.utils.NTheme();	
		return _tempTheme;
	}

	public void setMarsTheme(NTheme vec) {
		//TODO Please Implement the method
		_tempTheme = vec;
	}

	public void cleanStore() {
		//TODO Please Implement the method
		staticData = null;
	}


	public void setStaticData(@SuppressWarnings("rawtypes") Vector<Vector> data) {
		//TODO Please Implement the method
		staticData = data;
	}

	
	@Deprecated
	public Vector<Object> getCalendarActivities() {
		//TODO Please Implement the method
		return _calList;
	}

	@Deprecated
	public void setCalendarActivities(Vector<Object> list) {
		//TODO Please Implement the method			
		_calList = list;
	}
		
	public Vector<SaveElement> getUpdateQuery() {
		//TODO Please Implement the method
		return save_query;
	}

	public void setUpdateQuery(Vector<SaveElement> con) {
		//TODO Please Implement the method
		save_query = con;
	}

	public ArUser getUser() {			
		//TODO Please Implement the method
		return _user;
	}
	
	public void setUser(ArUser user) {
		//TODO Please Implement the method
		_user = user;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Vector<NCode> getCodes(boolean includeInActive) {
		Vector<Vector> rez =getStaticData();
		if(rez!=null) {
			if(includeInActive) 
				return (Vector<NCode>) rez.elementAt(1);
				
			Vector<NCode> ret = new Vector<NCode>();
			Enumeration<NCode> en = ((Vector<NCode>) rez.elementAt(1)).elements();
			while(en.hasMoreElements()){
				NCode cd = (NCode) en.nextElement();
				if(cd.isActive()) 
					ret.addElement(cd);
			}
			return ret;
		}
		return null;
	}
		    
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Vector<NState> getStates() {
		Vector rez = getStaticData();
		if(rez!=null) 
			return (Vector<NState>) rez.elementAt(2);
		return null;
	}    
		    
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Vector<ArUserInfo> getUsers() {
		Vector rez = getStaticData();
		if(rez!=null) 
			return (Vector) rez.elementAt(3);
		return null;
	}    
		    
	public String getNextSeq(ArUser user){
		String ret = user.getNextSeq();
		setUser(user);
		return ret;
	}	
		
	public Vector<SaveElement> getUpdateContainer(){
		Vector<SaveElement> con =  getUpdateQuery();
	
		if(con==null) {
			con = new Vector<SaveElement>();
			setUpdateQuery(con);
		}
		return con;
	}
		
	public void addElementToUpdate(String upStr, Hashtable<String, String> params, boolean isEnc){
		Vector<SaveElement> container = this.getUpdateContainer();
		container.addElement(new SaveElement(upStr, params, isEnc));
		setUpdateQuery(container);
	}
		    
	public void addElementToUpdate(SaveElement el){
		Vector<SaveElement> container = this.getUpdateContainer();
		container.addElement(el);
		setUpdateQuery(container);
	}    
		    
	public void removeUpdateElement(int inx) {
		Vector<SaveElement> container = this.getUpdateContainer();
		if((inx>-1) && (inx<container.size())) {
			container.removeElementAt(inx);
			setUpdateQuery(container);
		}		    
	}
		

	
	
	public synchronized void logEvent(String method, String text, boolean isErr){
		if(text.indexOf("password=")!=-1) {
			String temp = text.substring(0, text.indexOf("password="));
			String rest = text.substring(text.indexOf("password="));
			if(rest.indexOf("&")!=-1) {
				rest = rest.substring(rest.indexOf("&"));
				text = temp + "pass" + rest;
	    	} else {
	    		text = temp;
	    	}
	    }
		    	
	    StringBuffer elToLog = new StringBuffer(NDateFormat.currentTimeAsStringShort2());
	    elToLog.append("|");
	    elToLog.append(method);
	    elToLog.append("|");
	    elToLog.append(text); 
	    elToLog.append("|");
	    if(isErr) elToLog.append("E");    
	    else elToLog.append("S");
	    elToLog.append("|");
	        
	    
	    Log.d("MARS::LOG", elToLog.toString());
	    
	    Vector<String> events = getEvents();
		        
	    if(!getMarsTheme().isLocalEvents()){
	    	if(events.size()>MAX_LOG_SIZE){
	    		for(int i=0; i<STEP_TO_REMOVE_LOG; i++) {
	    			events.removeElementAt(i);
		        }
		    }               
		}
		        
		events.addElement(elToLog.toString());
		this.setEvents(events);
	}

	HashMap<String, String> allTags = new HashMap<String, String>();
	HashMap<String, String> notFoundTags = new HashMap<String, String>();
	
	public void saveUnfoundTags(NBaseActivity activity) {
		if(notFoundTags==null || notFoundTags.isEmpty()) return;
		
		StringBuffer sb = new StringBuffer();
		Iterator<String> is =  notFoundTags.values().iterator();
		while(is.hasNext()) {
			sb.append(is.next());
			sb.append(", ");
		}
		
		//activity.storeZeroResult(sb.toString());
	}

	public void addTagsToAllTags(String[] tags) {
		for(int i=0; i<tags.length; i++) {
			if(tags[i] == null || tags[i].trim().length()==0) continue;
			
			if(!allTags.containsKey(tags[i])) {
				allTags.put(tags[i], tags[i]);
			}
		}
	}
	
	/*
	public boolean isDropWord(String word, List<DropWord> dropWords) {
		if(dropWords == null) return false;
		
		for(int i = 0; i<dropWords.size(); i++) {
			if(dropWords.get(i)!=null) {
				if(dropWords.get(i).getWord().compareToIgnoreCase(word)==0) return true;
			}
		}
		
		return false;
	}*/
	/*
	public List<String> filterUnknownTags(String[] fs, List<DropWord> dropWords, boolean filterGrammar) {
		List<String> rez = new ArrayList<String>();
		for(int i=0; i<fs.length; i++) {
			String tag = fs[i];
			String orgTag = fs[i];
			if(filterGrammar)
				tag = filterGrammarTags(tag);
			if(isDropWord(tag, dropWords)) continue;
			//Do not include single letters Spider Bug
			if(tag.length()<2) continue;
			
			Collection<String> values = allTags.values();
			boolean found = false;
			Iterator<String> it = values.iterator();
			while(it.hasNext()) {
				String str = it.next();
				if(str.startsWith(tag)) {
					rez.add(tag);
					found = true;	
					break;
				} 
			}
			
			if(!found){
				if(!notFoundTags.containsKey(orgTag)) {
						notFoundTags.put(orgTag, orgTag);					
				}
			}
		}
		return rez;
	}*/

	private String filterGrammarTags(String tag) {
		
			try {
				if(tag.endsWith("s")) {
					tag = tag.substring(0, tag.length()-1);
				}
				if(tag.endsWith("ing")) {
					tag = tag.substring(0, tag.length()-3);
				}
				/*if(tag.endsWith("in")) {
					tag = tag.substring(0, tag.length()-2);
				}*/
				if(tag.endsWith("i")) {
					tag = tag.substring(0, tag.length()-1);
				}
				if(tag.equalsIgnoreCase("Difficulty")) {
					tag = "difficult";
				}
			} catch (Exception ex) {
				Log.e("filterGrammarTags", ex.getMessage());
			}
			
		return tag;
	}


}