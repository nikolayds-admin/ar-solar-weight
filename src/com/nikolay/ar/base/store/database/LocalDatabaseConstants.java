package com.nikolay.ar.base.store.database;

import android.app.SearchManager;

public interface LocalDatabaseConstants {
	 public static String ARFA_TAG = "ARFADatabase";
	 //The columns we'll include in the dictionary table
	 public static final String ARFA_DATABASE_NAME = "ARFA";
	 public static final String ARFA_QUESTIONS_VIRTUAL_TABLE = "ArfaQuestions";
	 public static final int ARFA_DATABASE_VERSION = 1;
	 
	 public static String TAG = "LocalDatabase";
	 //The columns we'll include in the dictionary table
	 public static final String KEY_WORD = SearchManager.SUGGEST_COLUMN_TEXT_1;
	 public static final String KEY_DEFINITION = SearchManager.SUGGEST_COLUMN_TEXT_2;
	 public static final String DATABASE_NAME = "LOCAL";
	 public static final String LOCAL_VIRTUAL_TABLE = "Local";
	 public static final int DATABASE_VERSION = 1;
}
