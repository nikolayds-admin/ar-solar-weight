package com.nikolay.ar.weatherforecast.weather;

import android.util.Log;

/**
 * Holds the information between the <current_conditions>-tag of what the Google
 * Weather API returned.
 */
public class WeatherCurrentCondition {

	// ===========================================================
	// Fields
	// ===========================================================

	private String dayofWeek = null;
	private Integer tempCelcius = null;
	private Integer tempFahrenheit = null;
	private String iconURL = null;
	private String condition = null;
	private String windCondition = null;
	private String humidity = null;

	// ===========================================================
	// Constructors
	// ===========================================================

	public WeatherCurrentCondition() {

	}

	// ===========================================================
	// Getter & Setter
	// ===========================================================

	public String getDayofWeek() {
		return this.dayofWeek;
	}

	public void setDayofWeek(String dayofWeek) {
		this.dayofWeek = dayofWeek;
	}

	public Integer getTempCelcius() {
		return this.tempCelcius;
	}

	public void setTempCelcius(Integer temp) {
		this.tempCelcius = temp;
	}

	public Integer getTempFahrenheit() {
		return this.tempFahrenheit;
	}

	public void setTempFahrenheit(Integer temp) {
		this.tempFahrenheit = temp;
	}

	public String getIconURL() {
		return this.iconURL;
	}

	public void setIconURL(String iconURL) {
		this.iconURL = iconURL;
	}

	public String getCondition() {
		return this.condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public String getWindCondition() {
		return this.windCondition;
	}
	
	public int getWindSpeed() {
		int ret = 0;
		
		if(this.windCondition==null || this.windCondition.length()==0) 
			return ret;
		int inx = this.windCondition.indexOf(" at ");
		if(inx !=-1 ) {
			String wind = this.windCondition.substring(
						inx + 4);
			if(wind.indexOf(" ")!=-1) {
				wind = wind.substring(0, wind.indexOf(" "));
			}
			
			try {
				ret = Integer.parseInt(wind);
			} catch (Exception ex) {
				Log.e("getWindSpeed", ex.getMessage());
				ret = 0;
			}
			
		}
		
		return ret;
	}

	public void setWindCondition(String windCondition) {
		this.windCondition = windCondition;
	}

	public String getHumidity() {
		return this.humidity;
	}
	
	public int getHumidityPerc() {
		int ret = 40;
		
		if(this.humidity==null || this.humidity.length()==0) {
			return ret;
		}
		
		
		if(humidity.indexOf("%")!=-1 &&
				humidity.indexOf(" ")!=-1
					) {
			String v = humidity.substring(humidity.indexOf(" ")+1, humidity.indexOf("%"));
		

			try {
				ret = Integer.parseInt(v);
			} catch (Exception ex) {
				Log.e("getHumidityPerc", ex.getMessage());
			}
		}
		return ret;
	}

	public void setHumidity(String humidity) {
		this.humidity = humidity;
	}
	
	public String getTagsCommaSeparated() {
		String tags = "";
		if(getWindSpeed()>15) {
			tags+="high_wind";
		}
		
		if(this.tempCelcius>30) {
			tags+=",hot_t";
		}
		
		if(this.tempCelcius<5) {
			tags+=",cold_t";
		}
	
		if(this.getHumidityPerc()<21) {
			tags+=",low_h";
		}
		
		if(this.getHumidityPerc()>70) {
			tags+=",high_h";
		}
		if(tags.startsWith(",")) {
			tags = tags.substring(1);
		}
		return tags;
	}
}
