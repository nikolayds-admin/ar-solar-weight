package com.nikolay.solar;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

import com.nikolay.solar.R;
import com.nikolay.ar.base.activities.NBaseActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.speech.tts.TextToSpeech.OnUtteranceCompletedListener;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ARWeightSolarActivity extends NBaseActivity implements ARSolarConstants, OnInitListener
 {
 
	boolean isMetric = false;
	int weight = 100;
	ARSolarPlanet planet = null;
	
	/** Called when the activity is first created. */
    @Override
    public void onNCreate(Bundle savedInstanceState) {
    	MarkableImageView ivSolar = (MarkableImageView) findViewById(R.id.ivSolarSystem);
    	
    	tts = new TextToSpeech(this, this);       
        //don't do speak until initing
        waitForInitLock.lock();

    	
    	ivSolar.setOnTouchListener(new android.view.View.OnTouchListener(){
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				
				if(event.getAction() != MotionEvent.ACTION_DOWN/*0*/) return false;
				
				float x = event.getX();
				float y = event.getY();
				//x*=event.getXPrecision();
				//y*=event.getXPrecision();
				
				//recalculate
				Display display = getWindowManager().getDefaultDisplay(); 
				int sw = display.getWidth();
				int sh = display.getHeight();

				float qx = sw/854f;
				float qy = sh/480f;
				
				x /=qx;
				y /=qy;
				
				
				EditText etWeigth = (EditText) findViewById(R.id.etWeight);
				
				if(etWeigth==null || etWeigth.getText()==null) {
					Toast.makeText(ARWeightSolarActivity.this, "Error", Toast.LENGTH_LONG).show();
					return true;
				}
				
				String strWeight = etWeigth.getText().toString();
				if(strWeight.length()==0) {
					Toast.makeText(ARWeightSolarActivity.this, "Enter your weight on earth.", Toast.LENGTH_LONG).show();
					return true;
				} else {
					try {
						weight = Integer.parseInt(strWeight);
					} catch (Exception ex) {
						Toast.makeText(ARWeightSolarActivity.this, "Enter proper weight.", Toast.LENGTH_LONG).show();
						return true;
					}
				}
				 
				//Button btn = (Button) findViewById(R.id.floating_button);
				//btn.setVisibility(View.VISIBLE);
				//btn.bringToFront();
				//btn.setLayoutParams()
				
				TableLayout ll = (TableLayout) findViewById(R.id.floating_layout);
				TextView tvDetails = (TextView) findViewById(R.id.tvWeightOn);
				
				ARSolarPlanet planet = ARSolarUtilities.resolvePlanet(x, y);
				if(planet!=null) {
					ll.setVisibility(View.VISIBLE);
					ll.bringToFront();
					ARWeightSolarActivity.this.planet = planet;
					String measurement = "pounds";
					if(isMetric) measurement = "kilograms";
					
					measurement = "units";
					
					String text = String.format("Your weight on %s is %d %s\n", planet.getName(), planet.calculateWeight(weight), measurement);
					
					text+=String.format("\n Distance from Sun is %d mil km", planet.getDistanceFormSun());
					text+=String.format("\n Distance from Earth is %d - %d mil km", planet.getDistanceFormEarthMin(), planet.getDistanceFormEarthMax());
					text+=String.format("\n Day is %d hours", planet.getDayLength());
					text+=String.format("\n Year is %d days", planet.getYearLength());
					text+=String.format("\n %s has %d moons", planet.getName(), planet.getMoonsCount());
					
					//Toast.makeText(ARWeightSolarActivity.this, text, Toast.LENGTH_LONG).show();
					tvDetails.setText(text);
					
					 say(planet.getDesc());


				} else {
					Toast.makeText(ARWeightSolarActivity.this, "Click on a planet or star", Toast.LENGTH_LONG).show();
				}
				
				//+++++++++++++ 
				//Check buttons
				/*if(ivSolar.onButtonSay(x, y)) {
					onVoiceQuestionClick(ivHuman);
					return true;
				}
				if(ivSolar.onButtonRotate(x, y)) {
					onRotate(ivHuman);
					return true;
				}
				if(ivSolar.onButtonMax(x, y)) {
					onMaximize(ivHuman);
					return true;
				}
				if(ivSolar.onButtonSearch(x, y)) {
					onClickSearch(ivHuman);
					return true;
				}*/
				//+++++++++++
				
				//+++++++++++++++++++++++++++
				//Add Marker
				//ivSolar.addMarker(x, y);
				//++++++++++++++
				
					
			
				return true;
			}
		});
    }

	@Override
	public boolean showAbout() {
		// TODO Auto-generated method stub
		Toast.makeText(this, "Solar fast facts by AR", Toast.LENGTH_SHORT).show();
		return false;
	}

	@Override
	protected boolean callPreferences() {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	protected int getLayoutID() {
		return R.layout.dashboard_layout;
	}
	
	public void closeLayout(final View view) {
		LinearLayout ll = (LinearLayout) findViewById(R.id.floating_layout);
		ll.setVisibility(View.GONE);
		
		MarkableImageView ivSolar = (MarkableImageView) findViewById(R.id.ivSolarSystem);
	    ivSolar.bringToFront();
		
	}
	
	public void openUrl(final View button) {
	
		if(planet!=null) {
			Intent i = new Intent(Intent.ACTION_VIEW, 
				       Uri.parse(planet.getUrl()));
				startActivity(i);

		}
		
		closeLayout(button);
		
	}

	private TextToSpeech tts;
	private static HashMap DUMMY_PARAMS = new HashMap();
    static 
    {
        DUMMY_PARAMS.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "theUtId");
    }
    private ReentrantLock waitForInitLock = new ReentrantLock();

	 
	@Override
	public void onInit(int arg0) {
		waitForInitLock.unlock();
    }  


    public void say(String say)
    {
        tts.speak(say, TextToSpeech.QUEUE_FLUSH, null);
    }

    public void say(String say, OnUtteranceCompletedListener whenTextDone)
    {
        if (waitForInitLock.isLocked())
        {
            try
            {
                waitForInitLock.tryLock(180, TimeUnit.SECONDS);
            }
            catch (InterruptedException e)
            {
                Log.e("speaker", "interruped");
            }
            //unlock it here so that it is never locked again
            waitForInitLock.unlock();
        }

        int result = tts.setOnUtteranceCompletedListener(whenTextDone);
        if (result == TextToSpeech.ERROR)
        {
            Log.e("speaker", "failed to add utterance listener");
        }
        //note: here pass in the dummy params so onUtteranceCompleted gets called
        tts.speak(say, TextToSpeech.QUEUE_FLUSH, DUMMY_PARAMS);
    }

    /**
     * make sure to call this at the end
     */
    public void done()
    {
        tts.shutdown();
    }

}