package com.nikolay.solar;

public interface ARSolarConstants {
	
	public static final int SUN = 0;
	public static final int MERCURY = 1;
	public static final int VENUS = 2;
	public static final int EARTH = 3;
	public static final int MARS = 4;
	public static final int JUPITER = 5;
	public static final int SATURN = 6;
	public static final int URAN = 7;
	public static final int NEPTUN = 8;
	public static final int PLUTO = 9;
	
	public static final int daysLength[]= new int[] {
		0, 59*24, 243*24, 24, 25, 10, 11, 17, 16, 153 
	};
	
	public static final int yearsLength[]= new int[] {
		0, 88, 225, 365, 687, 4332, 10753, 30660, 60152,90410
	};
	
	public static final int sunDistances[]= new int[] {
		0, 58, 108, 149,228, 778, 1427, 2871, 4498, 5900 
	};
	
	
	public static final int earthMinDistances[]= new int[] {
		149, 77, 38, 0, 54, 629, 1200, 2570, 4301, 4200 
	};
	
	public static final int earthMaxDistances[]= new int[] {
		149, 138, 223, 0, 401, 928, 1670, 3150, 4553, 7500
	};
	
	public static final int solarMoonsCount[]= new int[] {
		9, 0, 0, 1, 2, 64, 62, 27, 13, 4
	};
	
	
	
	public static final double koeficients[] = new double[] {
		27.072d, 0.378d, 0.907d, 1d, 0.37d, 2.364d, 1.064d, 0.889d, 1.125d, 0.067d 
	};
	/*
	  form.outputmrc.value = int_zero( 10 * weight * .378 ) / 10;
      form.outputvn.value = int_zero( 10 * weight * .907 ) / 10;
      form.outputmoon.value = int_zero( 10 * weight * .166 ) / 10;
      form.outputmars.value = int_zero( 10 * weight * .377 ) / 10;
      form.outputjp.value = int_zero( 10 * weight * 2.364 ) / 10;
      form.outputsat.value = int_zero( 10 * weight * 1.064 ) / 10;
      form.outputur.value = int_zero( 10 * weight * .889 ) / 10;
      form.outputnpt.value = int_zero( 10 * weight * 1.125 ) / 10;
      form.outputplt.value = int_zero( 10 * weight * .067 ) / 10;
      form.outputj2.value = int_zero( 100 * weight * .1264 ) / 100;
      form.outputj3.value = int_zero( 100 * weight * .13358 ) / 100;
      form.outputj4.value = int_zero( 100 * weight * .1448 ) / 100;
      form.outputj5.value = int_zero( 100 * weight * .18355 ) / 100;
      form.outputsun.value = int_zero( 10 * weight * 27.072 ) / 10;
      form.outputwd.value = int_zero( 10 * weight * 1.3E+6 ) / 10;
      form.outputns.value = weight * 1.4E+11;
	*/
	
	public static final String names[] = new String[] {
		"Sun",
		"Mercury",
		"Venus",
		"Earth",
		"Mars",
		"Jupiter",
		"Saturn",
		"Uranius",
		"Neptune",
		"Pluto"
	};
	
	public static final String[] urls = new String[] {
		"http://nineplanets.org/sol.html",
		"http://nineplanets.org/mercury.html",
		"http://nineplanets.org/venus.html",
		"http://nineplanets.org/earth.html",
		"http://nineplanets.org/mars.html",
		"http://nineplanets.org/jupiter.html",
		"http://nineplanets.org/saturn.html",
		"http://nineplanets.org/uranus.html",
		"http://nineplanets.org/neptune.html",
		"http://nineplanets.org/pluto.html"
	};
	
	public static ARSolarPlanet[] planets = null;
	
	
	public static final String[] descs = new String[] {
		"The Sun is not a planet. Our Star is actually a drawf.",
		"Mercury is the smallest planet, easy to get from freezer to the grill.",
		"Venus, Earth sister",
		"Earth, our home planet",
		"Mars is like home but colder",
		"Jupiter the giant. The eye is actually 300's year old storm. This is what we call extreme weather.",
		"Saturn is my son's favorite planet",
		"Uranus the blue planet, made up from methane and has a aquamarine color.",
		"Neptune God of sea",
		"Pluto is a planet, planet I said. Dont believe if you hear otherwise."
	};
}
