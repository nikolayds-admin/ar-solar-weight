package com.nikolay.solar;

public class ARSolarPlanet implements ARSolarConstants {
	String name;
	String desc;
	String url;
	double koeficient;
	
	//mercury
	
	int distanceSun = 58;
	int distanceEarthMin = 77;
	int distanceEarthMax = 138;
	int dayLength = 59*24;
	int yearLength = 88;
	int moonsCount = 0;
	
	int id = 0;
	
	public ARSolarPlanet(int id) {
		this.id = id;
		this.name = names[id];
		this.desc = descs[id];
		this.url = urls[id];
		this.koeficient = koeficients[id];
		this.distanceSun = sunDistances[id];
		this.distanceEarthMin = earthMinDistances[id];
		this.distanceEarthMax =  earthMaxDistances[id];
		this.dayLength = daysLength[id];
		this.yearLength = yearsLength[id];
		this.moonsCount = solarMoonsCount[id];
	}
	
	public int calculateWeight(int weight) {
		double w = weight;
		w*=koeficient;
		return new Double(w).intValue();
	}

	public Object getName() {
		return name;
	}

	public String getUrl() {
		return url;
	}

	public int getDistanceFormSun() {
		return distanceSun;
	}
	
	public int getDistanceFormEarthMin() {
		return distanceEarthMin;
	}
	
	public int getDistanceFormEarthMax() {
		return distanceEarthMax;
	}

	public int getDayLength() {
		return dayLength;
	}

	public int getYearLength() {
		return yearLength;
	}

	public int getMoonsCount() {
		// TODO Auto-generated method stub
		return moonsCount;
	}

	public String getDesc() {
		return desc;
	}
}
