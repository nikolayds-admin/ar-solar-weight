package com.nikolay.solar;

import java.util.ArrayList;

import com.nikolay.solar.R;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;


public class MarkableImageView extends android.widget.ImageView {

	boolean hasRotateButton = false;
	boolean hasSayButton = false;
	boolean hasMaxButton = false;
	boolean hasSearchButton = false;
	
	private static final int x_offest = 2;
	private static final int y_offset = 5;
	
	public MarkableImageView(Context context) {
		super(context);
	}
	
	public MarkableImageView(android.content.Context context, android.util.AttributeSet attrs) {
		super(context, attrs);
	}

	public MarkableImageView(android.content.Context context, android.util.AttributeSet attrs, int defStyle){
		super(context, attrs, defStyle);
	}
	
	public void addSayButton() {
		hasSayButton = true;
	}
	
	public void addRotateButton() {
		hasRotateButton = true;
	}
	
	ArrayList<Marker> mMarkers;


    // ...

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if(mMarkers!=null) {
        	Marker m = mMarkers.get(0);
        	Paint paint = new Paint();
        	paint.setColor(Color.RED);
            paint.setAntiAlias(true);
            
        	canvas.drawCircle(m.x, m.y, 10, paint);
        }
        /*for(Marker m : mMarkers) {
            // draw the marker
        }*/
        
        if(hasSearchButton) {
        	Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.search_72);
        	Paint paint = new Paint();
        	canvas.drawBitmap(bitmap, x_offest, y_offset, paint);
        }
        
        if(hasSayButton) {
        	if(hasSearchButton) {
	        	Bitmap s_bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.search_72);
	        	Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.mic_72);
	        	int x = x_offest*2 + s_bitmap.getWidth();
	        	Paint paint = new Paint();
	        	canvas.drawBitmap(bitmap, x, y_offset, paint);
        	} else {
        		Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.mic_72);
	        	int x = x_offest;
	        	Paint paint = new Paint();
	        	canvas.drawBitmap(bitmap, x, y_offset, paint);
        	}
        }
        
        if(hasRotateButton) {
        	if(hasMaxButton) {
	        	Bitmap full_bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.replacement);
	        	Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.rotate_72);
	        	Paint paint = new Paint();
	        	int x = this.getWidth()-bitmap.getWidth() - 2*x_offest - full_bitmap.getWidth();
	        	canvas.drawBitmap(bitmap, x , y_offset, paint);
        	} else {
        	 	Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.rotate_72);
	        	Paint paint = new Paint();
	        	int x = this.getWidth()-bitmap.getWidth() - x_offest;
	        	canvas.drawBitmap(bitmap, x , y_offset, paint);
        	}
        }
        
        if(hasMaxButton) {
        	Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.replacement);
        	Paint paint = new Paint();
        	canvas.drawBitmap(bitmap, this.getWidth()-bitmap.getWidth() - x_offest, y_offset, paint);
        }
        
    }
    
    public void addMarker(float x, float y) {
    	
    	if(!onButtonSay(x, y) && !onButtonRotate(x, y)) {
	    	mMarkers = new ArrayList<Marker>();
	    	mMarkers.add(new Marker(x, y));
	    	invalidate();
    	}
    }

    public boolean onButtonSay(float x, float y) {
    	if(!hasSayButton) return false;
    	if(hasSearchButton) {
	    	Bitmap s_bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.search_72);
	    	Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.mic_72);
	    	int x_s = x_offest + s_bitmap.getWidth();
	    	int x_e = x_s + (bitmap.getWidth()+x_offest);
	    	
	    	if(x>x_s
	    		&&	x< x_e
	    		&& y<(bitmap.getHeight()+y_offset)){
	    				return true;
	    	}
    	} else {
    		Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.mic_72);
	    	int x_s = 0;
	    	int x_e = x_s + (bitmap.getWidth()+x_offest);
	    	
	    	if(x>x_s
	    		&&	x< x_e
	    		&& y<(bitmap.getHeight()+y_offset)){
	    				return true;
	    	}
    	}
		return false;
	}

	public boolean onButtonRotate(float x, float y) {
		if(!hasRotateButton) return false;
		if(hasMaxButton) {
			Bitmap full_bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.replacement);
			Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.rotate_72);
			
			int x_s = this.getWidth()-bitmap.getWidth() - 2*x_offest - full_bitmap.getWidth();
			int x_e = this.getWidth() - bitmap.getWidth() -x_offest;
			if(x>(x_s) 
					&& x<x_e
		    		&& y<(bitmap.getHeight()+y_offset)){
		    				return true;
		    }
		} else {
			Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.rotate_72);
			int x_s = this.getWidth()-bitmap.getWidth() - 2*x_offest;
			//int x_e = this.getWidth() - x_offest;
			if(x>(x_s) 
		    		&& y<(bitmap.getHeight()+y_offset)){
		    				return true;
		    }
		}
		return false;
	}
	
	public boolean onButtonSearch(float x, float y) {
	    	if(!hasSearchButton) return false;
	    	
	    	Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.replacement);
	    	if(x<(bitmap.getWidth()+x_offest) 
	    		&& y<(bitmap.getHeight()+y_offset)){
	    				return true;
	    	}
			return false;
		}

		public boolean onButtonMax(float x, float y) {
			if(!hasMaxButton) return false;
			
			Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.replacement);
			if(x>(this.getWidth() - bitmap.getWidth() -x_offest) 
		    		&& y<(bitmap.getHeight()+y_offset)){
		    				return true;
		    }
			return false;
		}

	/*@Override
    public boolean onTouchEvent(MotionEvent e) {
        if (e.getAction() == MotionEvent.ACTION_DOWN) {
            mMarkers.add(new Marker(e.getX(), e.getY()));
            invalidate();
            return true;
        }
        return false;
    }*/

    public void reset() {
        mMarkers.clear();
        invalidate();
    }

    // this class will be visible only inside MarkableImageView
    private class Marker {
        public float x;
        public float y;
        // you might want to add other properties, for example
        // if you need to have different types of markers

        public Marker(float f, float g) {
            this.x = f;
            this.y = g;
        }
    }

	public void addMaximizeButton() {
		hasMaxButton = true;
	}

	public void addSearchButton() {
		hasSearchButton = true;
	}

}
