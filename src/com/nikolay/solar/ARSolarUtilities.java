package com.nikolay.solar;

public class ARSolarUtilities implements ARSolarConstants {

	static int s_mercury = 150;
	static int s_venus = 180;
	static int s_earth = 240;
	static int s_mars = 300;
	static int s_jupiter = 390;
	static int s_saturn = 480;
	static int s_uran = 570;
	static int s_neptune = 660;
	static int s_pluto = 760;
	
	public static ARSolarPlanet resolvePlanet(float x, float y) {
		// TODO Fix it for normal devices
		int planet = MARS;
		
		//recalculate as per device screen size 
		int xi = new Float(x).intValue();
		int yi = new Float(y).intValue();
		
		if(xi<s_mercury) {
			planet = SUN;
		} else if(xi<s_venus ){
			planet = MERCURY;
		} else if(xi<s_earth ){
			planet = VENUS;
		} else if(xi<s_mars ){
			planet = EARTH;
		} else if(xi<s_jupiter ){
			planet = MARS;
		} else if(xi<s_saturn ){
			planet = JUPITER;
		}else if(xi<s_uran ){
			planet = SATURN;
		}else if(xi<s_neptune ){
			planet = URAN;
		}else if(xi<s_pluto ){
			planet = NEPTUN;
		}else {
			planet = PLUTO;
		}
		return new ARSolarPlanet(planet);
	}

}
